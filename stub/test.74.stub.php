<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\JsonSchema\Php74CompoundMetadata;
use PhpExtended\JsonSchema\Php74TestMetadata;

/**
 * @var Php74TestMetadata $this
 */

echo "<?php"; ?> declare(strict_types=1);

/*
 * This file is part of the <?php echo $this->getComposerPackageName(); ?> library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace <?php echo $this->getTestNamespace(); ?>;

<?php foreach($this->getUses() as $use) { ?>
use <?php echo $use; ?>;
<?php } ?>

/**
 * <?php echo $this->getTestClassname(); ?> test file.
 * 
<?php if(!empty($this->getTitle())) { ?>
 * <?php echo $this->getTitle()."\n"; ?>
 * 
<?php } ?>
<?php if(!empty($this->getDescription())) { ?>
<?php 	foreach(explode("\n", $this->getDescription()) as $descriptionLine) { ?>
 * <?php echo wordwrap($descriptionLine, 76, "\n * ", false)."\n"; ?>
<?php 	} ?>
 * 
<?php } ?>
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator <?php echo get_class($this)."\n"; ?>
 * 
 * @author Anastaszor
 * @covers \<?php echo trim($this->getClassNamespace()."\\".$this->getClassName(), '\\')."\n"; ?>
 * @internal
 * @small
 */
class <?php echo $this->getTestClassname(); ?> extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var <?php echo $this->getClassName()."\n"; ?>
	 */
	protected <?php echo $this->getClassName(); ?> $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
<?php foreach($this->getCompounds() as $compound) { ?>
	public function test<?php echo ucfirst($compound->getGetterName()); ?>() : void
	{
<?php 	if($compound->isArray() && $compound->isNullable()) { ?>
		$this->assertEquals([], $this->_object-><?php echo $compound->getGetterName(); ?>());
<?php 	} elseif($compound->isNullable()) { ?>
		$this->assertNull($this->_object-><?php echo $compound->getGetterName(); ?>());
<?php 	} elseif($compound->getDefaultValue() === 'false') { ?>
		$this->assertFalse($this->_object-><?php echo $compound->getGetterName(); ?>());
<?php 	} elseif($compound->getDefaultValue() === 'true') { ?>
		$this->assertTrue($this->_object-><?php echo $compound->getGetterName(); ?>());
<?php 	} else { ?>
		$this->assertEquals(<?php echo $compound->getDefaultValue(); ?>, $this->_object-><?php echo $compound->getGetterName(); ?>());
<?php 	} ?>
		$expected = <?php echo $compound->getCalculatedValue(); ?>;
		$this->_object-><?php echo $compound->getSetterName(); ?>($expected);
<?php 	if($compound->getCalculatedvalue() === 'false') { ?>
		$this->assertFalse($this->_object-><?php echo $compound->getGetterName(); ?>());
<?php 	} elseif($compound->getCalculatedValue() === 'true') { ?>
		$this->assertTrue($this->_object-><?php echo $compound->getGetterName(); ?>());
<?php	} else { ?>
		$this->assertEquals($expected, $this->_object-><?php echo $compound->getGetterName(); ?>());
<?php 	} ?>
	}
	
<?php } ?>
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new <?php echo $this->getClassName(); ?>(<?php echo implode(', ', array_filter(array_map(function(Php74CompoundMetadata $compoundData) : ?string
		{
			if($compoundData->isPublic())
			{
				return null;
			}
			
			if($compoundData->isNullable())
			{
				return null;
			}
			
			return $compoundData->getDefaultValue();
		}, $this->getCompounds()))); ?>);
	}
	
}
