<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\JsonSchema\Php74InterfaceMetadata;
use PhpExtended\JsonSchema\Php74MethodMetadata;
use PhpExtended\JsonSchema\Php74ParameterMetadata;

/**
 * @var Php74InterfaceMetadata $this
 */

echo "<?php"; ?> declare(strict_types=1);

/*
 * This file is part of the <?php echo $this->getComposerPackageName(); ?> library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace <?php echo $this->getNamespace(); ?>;

<?php foreach($this->getUses() as $use) { ?>
use <?php echo $use; ?>;
<?php } ?>

/**
 * <?php echo $this->getClassname(); ?> interface file.
 * 
<?php if(!empty($this->getTitle())) { ?>
 * <?php echo $this->getTitle()."\n"; ?>
 * 
<?php } ?>
<?php if(!empty($this->getDescription())) { ?>
<?php 	foreach(explode("\n", $this->getDescription()) as $descriptionLine) { ?>
 * <?php echo wordwrap($descriptionLine, 76, "\n * ", false)."\n"; ?>
<?php 	} ?>
 * 
<?php } ?>
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator <?php echo get_class($this)."\n"; ?>
 * 
 * @author Anastaszor
<?php if(strlen($this->getClassName()) >= 40) { ?>
 * @SuppressWarnings(PHPMD.LongClassName)
<?php } ?>
<?php if($this->getMethodCount() >= 45) { ?>
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
<?php } ?>
 */
interface <?php echo $this->getClassName(); ?> extends Stringable
{
	
<?php /** @var $methodData Php74MethodMetadata */ ?>
<?php foreach($this->getMethods() as $methodData) { ?>
	/**
<?php if(!empty($methodData->getTitle())) { ?>
	 * <?php echo $methodData->getTitle()."\n"; ?>
	 * 
<?php } ?>
<?php if(!empty($methodData->getDescription())) { ?>
<?php 	foreach(explode("\n", $methodData->getDescription()) as $descriptionLine) { ?>
	 * <?php echo wordwrap($descriptionLine, 72, "\n\t * ", false)."\n"; ?>
<?php 	} ?>
	 * 
<?php } ?>
<?php /** @var $parameter Php74ParameterMetadata */ ?>
<?php foreach($methodData->getParameters() as $parameter) { ?>
	 * @param <?php echo ($parameter->isNullable() && !$parameter->isArray() ? '?' : '').$parameter->getDocType(); ?> $<?php echo $parameter->getName(); ?> <?php echo $parameter->getTitle()."\n"; ?>
<?php } ?>
	 * @return <?php echo ($methodData->isReturnNullable() && !$methodData->isReturnArray() ? '?' : '').$methodData->getReturnDocType()."\n"; ?>
	 */
	public function <?php echo $methodData->getName(); ?>(<?php echo implode(', ', array_filter(array_map(function(Php74ParameterMetadata $parameter) : ?string
	{
		return ($parameter->isNullable() && !$parameter->isArray() ? '?' : '').$parameter->getPhpType().' $'.$parameter->getName();
	}, $methodData->getParameters()))); ?>) : <?php echo ($methodData->isReturnNullable() && !$methodData->isReturnArray() ? '?' : '').$methodData->getReturnPhpType(); ?>;
	
<?php } ?>
}
