<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\JsonSchema\Php74ClassMetadata;
use PhpExtended\JsonSchema\Php74FieldMetadata;
use PhpExtended\JsonSchema\Php74MethodMetadata;
use PhpExtended\JsonSchema\Php74ParameterMetadata;

/**
 * @var Php74ClassMetadata $this
 */

echo "<?php"; ?> declare(strict_types=1);

/*
 * This file is part of the <?php echo $this->getComposerPackageName(); ?> library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace <?php echo $this->getNamespace(); ?>;

<?php if(!empty($this->getUses())) { ?>
<?php foreach($this->getUses() as $use) { ?>
use <?php echo $use; ?>;
<?php } ?>

<?php } ?>
/**
 * <?php echo $this->getClassname(); ?> class file.
 * 
<?php if(!empty($this->getTitle())) { ?>
 * <?php echo $this->getTitle()."\n"; ?>
 * 
<?php } ?>
<?php if(!empty($this->getDescription())) { ?>
<?php 	foreach(explode("\n", $this->getDescription()) as $descriptionLine) { ?>
 * <?php echo wordwrap($descriptionLine, 76, "\n * ", false)."\n"; ?>
<?php 	} ?>
 * 
<?php } ?>
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator <?php echo get_class($this)."\n"; ?>
 * 
 * @author Anastaszor
<?php if($this->getComplexity() >= 50) { ?>
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
<?php } ?>
<?php if($this->getLineCount() >= 1000) { ?>
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
<?php } ?>
<?php if($this->getPublicCount() >= 45) { ?>
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
<?php } ?>
<?php if(strlen($this->getClassName()) >= 40) { ?>
 * @SuppressWarnings(PHPMD.LongClassName)
<?php } ?>
<?php if($this->hasLongFieldNames()) { ?>
 * @SuppressWarnings(PHPMD.LongVariable)
<?php } ?>
<?php if($this->hasShortFieldNames()) { ?>
 * @SuppressWarnings(PHPMD.ShortVariable)
<?php } ?>
<?php if($this->getFieldCount() >= 15) { ?>
 * @SuppressWarnings(PHPMD.TooManyFields)
<?php } ?>
<?php if($this->getMethodCount() >= 45) { ?>
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
<?php } ?>
 */
class <?php echo $this->getClassName(); ?> implements <?php echo $this->getInterfaceName()."\n"; ?>
{
	
<?php /** @var $fieldData Php74FieldMetadata */ ?>
<?php foreach($this->getFields() as $fieldData) { ?>
	/**
<?php if(!empty($fieldData->getTitle())) { ?>
	 * <?php echo $fieldData->getTitle()."\n"; ?>
	 * 
<?php } ?>
<?php if(!empty($fieldData->getDescription())) { ?>
<?php 	foreach(explode("\n", $fieldData->getDescription()) as $descriptionLine) { ?>
	 * <?php echo wordwrap($descriptionLine, 72, "\n\t * ", false)."\n"; ?>
<?php 	} ?>
	 * 
<?php } ?>
	 * @var <?php echo ($fieldData->isNullable() && !$fieldData->isArray() ? '?' : '').$fieldData->getDocType().(empty($fieldData->getTitle() ? '' : ' ').$fieldData->getTitle())."\n"; ?>
	 */
	<?php echo $fieldData->isPublic() ? 'public' : 'protected'; ?> <?php echo ($fieldData->isNullable() && !$fieldData->isArray() ? '?' : '').$fieldData->getPhpType(); ?> $_<?php echo $fieldData->getName().($fieldData->isArray() ? ' = []' : ($fieldData->isNullable() ? ' = null' : '')); ?>;
	
<?php } ?>
<?php if($this->needsConstructor()) { ?>
	/**
	 * Constructor for <?php echo $this->getClassName(); ?> with private members.
	 * 
<?php /** @var $fieldData Php74FieldMetadata */ ?>
<?php foreach($this->getFields() as $fieldData) { ?>
<?php 	if(!($fieldData->isPublic() || $fieldData->isNullable())) { ?>
	 * @param <?php echo ($fieldData->isNullable() && !$fieldData->isArray() ? '?' : '').$fieldData->getDocType().' $'.$fieldData->getName()."\n"; ?>
<?php 	} ?>
<?php } ?>
<?php if(count($this->getFields()) >= 10) { ?>
	 * @SuppressWarnings(PHPMD.ExcessiveParameterList)
<?php } ?>
	 */
	public function __construct(<?php echo implode(', ', array_filter(array_map(function(Php74FieldMetadata $fieldData) : ?string
	{
		if($fieldData->isPublic())
		{
			return null;
		}
		
		if($fieldData->isNullable())
		{
			return null;
		}
		
		return ($fieldData->isNullable() && !$fieldData->isArray() ? '?' : '').$fieldData->getPhpType().' $'.$fieldData->getName();
	}, $this->getFields()))); ?>)
	{
<?php /** @var $methodData Php74MethodMetadata */ ?>
<?php foreach($this->getMethods() as $methodData) { ?>
<?php 	if($methodData->isSetter() && !($methodData->getRelatedField()->isPublic() || $methodData->getRelatedField()->isNullable())) { ?>
		$this-><?php echo $methodData->getName(); ?>(<?php echo implode(', ', array_filter(array_map(function(Php74ParameterMetadata $parameter) : ?string
		{
			return '$'.$parameter->getName();
		}, $methodData->getParameters()))); ?>);
<?php 	} ?>
<?php } ?>
	}
	
<?php } ?>
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
<?php /** @var $methodData Php74MethodMetadata */ ?>
<?php foreach($this->getMethods() as $methodData) { ?>
	/**
<?php 	if(!empty($methodData->getTitle())) { ?>
	 * <?php echo $methodData->getTitle()."\n"; ?>
	 * 
<?php } ?>
<?php 	if(!empty($methodData->getDescription())) { ?>
<?php 		foreach(explode("\n", $methodData->getDescription()) as $descriptionLine) { ?>
	 * <?php echo wordwrap($descriptionLine, 72, "\n\t * ", false)."\n"; ?>
<?php 		} ?>
	 * 
<?php 	} ?>
<?php /** @var $parameter Php74ParameterMetadata */ ?>
<?php 	foreach($methodData->getParameters() as $parameter) { ?>
	 * @param <?php echo ($parameter->isNullable() && !$parameter->isArray() ? '?' : '').$parameter->getDocType(); ?> $<?php echo $parameter->getName(); ?><?php echo (empty($parameter->getTitle()) ? '' : ' '.$parameter->getTitle())."\n"; ?>
<?php 	} ?>
	 * @return <?php echo ($methodData->isReturnNullable() && !$methodData->isReturnArray() ? '?' : '').$methodData->getReturnDocType()."\n"; ?>
	 */
	public function <?php echo $methodData->getName(); ?>(<?php echo implode(', ', array_filter(array_map(function(Php74ParameterMetadata $parameter) : ?string
	{
		return ($parameter->isNullable() && !$parameter->isArray() ? '?' : '').$parameter->getPhpType().' $'.$parameter->getName();
	}, $methodData->getParameters()))); ?>) : <?php echo ($methodData->isReturnNullable() && !$methodData->isReturnArray() ? '?' : '').$methodData->getReturnPhpType()."\n"; ?>
	{
<?php 	if($methodData->isGetter()) { ?>
		return $this->_<?php echo $methodData->getRelatedField()->getName(); ?>;
<?php 	} ?>
<?php 	if($methodData->isSetter()) { ?>
		$this->_<?php echo $methodData->getRelatedField()->getName(); ?> = $<?php echo $methodData->getRelatedField()->getName(); ?>;
		
		return $this;
<?php 	} ?>
	}
	
<?php } ?>
}
