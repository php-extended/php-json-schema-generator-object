<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\JsonSchema\JsonSchemaFileGeneratorPhp74;
use PhpExtended\JsonSchema\JsonSchemaProvider;
use PhpExtended\JsonSchema\NamespaceHolder;
use PHPUnit\Framework\TestCase;

/**
 * JsonSchemaFileGeneratorPhp74Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\JsonSchemaFileGeneratorPhp74
 * @internal
 * @small
 * @internal
 */
class JsonSchemaFileGeneratorPhp74FloatSchemaTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var JsonSchemaFileGeneratorPhp74
	 */
	protected JsonSchemaFileGeneratorPhp74 $_object;
	
	public function testGenerateFloat() : void
	{
		$provider = new JsonSchemaProvider();
		$schema = $provider->provideFromString('{
	"$id" : "'.__FILE__.'",
	"type": "null",
	"$defs": {
		"ClassName": {
			"type": "object",
			"description": "This is a description",
			"properties": {
				"field_float": {
					"type": "number",
					"description": "The description of the field"
				}
			}
		}
	}
}');
		
		$collection = $this->_object->generate($schema);
		
		$interface = '<?php declare(strict_types=1);

/*
 * This file is part of the interfaces/package-name library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Interfaces\\Namespace;

use Stringable;

/**
 * ClassNameInterface interface file.
 * 
 * This is a description
 * 
 * /!\\ This file was generated automatically from the json-schema.json file.
 * /!\\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\\JsonSchema\\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ClassNameInterface extends Stringable
{
	
	/**
	 * Gets the description of the field
	 * 
	 * @return ?float
	 */
	public function getFieldFloat() : ?float;
	
}
';
		
		$class = '<?php declare(strict_types=1);

/*
 * This file is part of the classes/package-name library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Classes\\Namespace;

use Interfaces\\Namespace\\ClassNameInterface;

/**
 * ClassName class file.
 * 
 * This is a simple implementation of the ClassNameInterface.
 * 
 * /!\\ This file was generated automatically from the json-schema.json file.
 * /!\\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\\JsonSchema\\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ClassName implements ClassNameInterface
{
	
	/**
	 * The description of the field
	 * 
	 * @var ?float
	 */
	protected ?float $_fieldFloat = null;
	
	/**
	 * {@inheritDoc}
	 * @see \\Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.\'@\'.\\spl_object_hash($this);
	}
	
	/**
	 * Sets the description of the field
	 * 
	 * @param ?float $fieldFloat
	 * @return ClassNameInterface
	 */
	public function setFieldFloat(?float $fieldFloat) : ClassNameInterface
	{
		$this->_fieldFloat = $fieldFloat;
		
		return $this;
	}
	
	/**
	 * Gets the description of the field
	 * 
	 * @return ?float
	 */
	public function getFieldFloat() : ?float
	{
		return $this->_fieldFloat;
	}
	
}
';
		
		$test = '<?php declare(strict_types=1);

/*
 * This file is part of the tests/package-name library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Tests\\Namespace;

use Classes\\Namespace\\ClassName;
use PHPUnit\\Framework\\TestCase;

/**
 * ClassNameTest test file.
 * 
 * /!\\ This file was generated automatically from the json-schema.json file.
 * /!\\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\\JsonSchema\\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \\Classes\\Namespace\\ClassName
 * @internal
 * @small
 */
class ClassNameTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ClassName
	 */
	protected ClassName $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\\get_class($this->_object).\'@\'.\\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetFieldFloat() : void
	{
		$this->assertNull($this->_object->getFieldFloat());
		$expected = 15.2;
		$this->_object->setFieldFloat($expected);
		$this->assertEquals($expected, $this->_object->getFieldFloat());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \\PHPUnit\\Framework\\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ClassName();
	}
	
}
';
		
		$this->assertCount(1, $collection->getInterfaceFiles());
		$this->assertEquals($interface, \array_values($collection->getInterfaceFiles())[0], 'interface');
		$this->assertCount(1, $collection->getClassFiles());
		$this->assertEquals($class, \array_values($collection->getClassFiles())[0], 'class');
		$this->assertCount(1, $collection->getTestFiles());
		$this->assertEquals($test, \array_values($collection->getTestFiles())[0], 'test');
	}
	
	public function testGenerateFloatMandatory() : void
	{
		$provider = new JsonSchemaProvider();
		$schema = $provider->provideFromString('{
	"$id" : "'.__FILE__.'",
	"type": "null",
	"$defs": {
		"ClassName": {
			"type": "object",
			"description": "This is a description",
			"properties": {
				"field_float": {
					"type": "number",
					"description": "The description of the field"
				}
			},
			"required" : ["field_float"]
		}
	}
}');
		
		$collection = $this->_object->generate($schema);
		
		$interface = '<?php declare(strict_types=1);

/*
 * This file is part of the interfaces/package-name library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Interfaces\\Namespace;

use Stringable;

/**
 * ClassNameInterface interface file.
 * 
 * This is a description
 * 
 * /!\\ This file was generated automatically from the json-schema.json file.
 * /!\\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\\JsonSchema\\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ClassNameInterface extends Stringable
{
	
	/**
	 * Gets the description of the field
	 * 
	 * @return float
	 */
	public function getFieldFloat() : float;
	
}
';
		
		$class = '<?php declare(strict_types=1);

/*
 * This file is part of the classes/package-name library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Classes\\Namespace;

use Interfaces\\Namespace\\ClassNameInterface;

/**
 * ClassName class file.
 * 
 * This is a simple implementation of the ClassNameInterface.
 * 
 * /!\\ This file was generated automatically from the json-schema.json file.
 * /!\\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\\JsonSchema\\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ClassName implements ClassNameInterface
{
	
	/**
	 * The description of the field
	 * 
	 * @var float
	 */
	protected float $_fieldFloat;
	
	/**
	 * Constructor for ClassName with private members.
	 * 
	 * @param float $fieldFloat
	 */
	public function __construct(float $fieldFloat)
	{
		$this->setFieldFloat($fieldFloat);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \\Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.\'@\'.\\spl_object_hash($this);
	}
	
	/**
	 * Sets the description of the field
	 * 
	 * @param float $fieldFloat
	 * @return ClassNameInterface
	 */
	public function setFieldFloat(float $fieldFloat) : ClassNameInterface
	{
		$this->_fieldFloat = $fieldFloat;
		
		return $this;
	}
	
	/**
	 * Gets the description of the field
	 * 
	 * @return float
	 */
	public function getFieldFloat() : float
	{
		return $this->_fieldFloat;
	}
	
}
';
		
		$test = '<?php declare(strict_types=1);

/*
 * This file is part of the tests/package-name library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Tests\\Namespace;

use Classes\\Namespace\\ClassName;
use PHPUnit\\Framework\\TestCase;

/**
 * ClassNameTest test file.
 * 
 * /!\\ This file was generated automatically from the json-schema.json file.
 * /!\\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\\JsonSchema\\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \\Classes\\Namespace\\ClassName
 * @internal
 * @small
 */
class ClassNameTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ClassName
	 */
	protected ClassName $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\\get_class($this->_object).\'@\'.\\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetFieldFloat() : void
	{
		$this->assertEquals(14.1, $this->_object->getFieldFloat());
		$expected = 15.2;
		$this->_object->setFieldFloat($expected);
		$this->assertEquals($expected, $this->_object->getFieldFloat());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \\PHPUnit\\Framework\\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ClassName(14.1);
	}
	
}
';
		
		$this->assertCount(1, $collection->getInterfaceFiles());
		$this->assertEquals($interface, \array_values($collection->getInterfaceFiles())[0], 'interface');
		$this->assertCount(1, $collection->getClassFiles());
		$this->assertEquals($class, \array_values($collection->getClassFiles())[0], 'class');
		$this->assertCount(1, $collection->getTestFiles());
		$this->assertEquals($test, \array_values($collection->getTestFiles())[0], 'test');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$namespaces = new NamespaceHolder('Interfaces\\Namespace', 'Classes\\Namespace', 'Tests\\Namespace');
		$this->_object = new JsonSchemaFileGeneratorPhp74(
			$namespaces,
			'interfaces/package-name',
			'classes/package-name',
			'tests/package-name',
		);
	}
	
}
