<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\JsonSchema\NamespaceHolder;
use PhpExtended\JsonSchema\Php74InterfaceMetadata;
use PhpExtended\JsonSchema\Php74MethodMetadata;
use PhpExtended\JsonSchema\PhpSingleType;
use PHPUnit\Framework\TestCase;

/**
 * Php74InterfaceMetadataTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\Php74InterfaceMetadata
 * @internal
 * @small
 */
class Php74InterfaceMetadataTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Php74InterfaceMetadata
	 */
	protected Php74InterfaceMetadata $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetComposerPackageName() : void
	{
		$this->assertEquals('composerPackageName', $this->_object->getComposerPackageName());
	}
	
	public function testGetNamespace() : void
	{
		$this->assertEquals('Interfaces\\Namespace', $this->_object->getNamespace());
	}
	
	public function testGetUse() : void
	{
		$this->assertEquals(['Stringable'], $this->_object->getUses());
		$this->_object->addUses(['New\\Used\\To\\Be\\Class']);
		$this->assertEquals(['Stringable', 'New\\Used\\To\\Be\\Class'], $this->_object->getUses());
	}
	
	public function testDoesNotAcceptUse() : void
	{
		$this->assertEquals(['Stringable'], $this->_object->getUses());
		$this->_object->addUse('Interfaces\\Namespace\\OtherInterface');
		$this->assertEquals(['Stringable'], $this->_object->getUses());
	}
	
	public function testGetClassname() : void
	{
		$this->assertEquals('className', $this->_object->getClassname());
	}
	
	public function testGetNamespacedClassName() : void
	{
		$this->assertEquals('Interfaces\\Namespace\\className', $this->_object->getNamespacedClassName());
	}
	
	public function testGetTitle() : void
	{
		$this->assertEquals('title', $this->_object->getTitle());
	}
	
	public function testGetDescription() : void
	{
		$this->assertEquals('description', $this->_object->getDescription());
	}
	
	public function testGetMethods() : void
	{
		$this->assertEmpty($this->_object->getMethods());
		$method = new Php74MethodMetadata('title', 'description', 'name', new PhpSingleType('Classes\\Namespace\\ReturnType', 'Classes\\Namespace\\ReturnType'));
		$this->_object->addMethod($method);
		$this->assertEquals([$method], $this->_object->getMethods());
		$this->assertEquals(['Stringable', 'Classes\\Namespace\\ReturnType'], $this->_object->getUses());
	}
	
	public function testRender() : void
	{
		$this->assertNotEmpty($this->_object->render());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$namespaces = new NamespaceHolder('Interfaces\\Namespace', 'Classes\\Namespace', 'Tests\\Namespace');
		$this->_object = new Php74InterfaceMetadata($namespaces, 'composerPackageName', 'className', 'title', 'description');
	}
	
}
