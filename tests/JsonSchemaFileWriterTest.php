<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\JsonSchema\JsonSchemaFileCollection;
use PhpExtended\JsonSchema\JsonSchemaFileWriter;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;

/**
 * JsonSchemaFileWriterTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\JsonSchemaFileWriter
 * @internal
 * @small
 */
class JsonSchemaFileWriterTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var JsonSchemaFileWriter
	 */
	protected JsonSchemaFileWriter $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testCheckWriteableRealpath() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_object->checkWriteable('type', __DIR__.'/nonexistant.file');
	}
	
	public function testCheckWriteableNonDirectory() : void
	{
		$this->expectException(InvalidArgumentException::class);
		
		$this->_object->checkWriteable('type', __FILE__);
	}
	
	public function testFailedWriting() : void
	{
		$this->expectException(RuntimeException::class);
		
		$this->_object->writeAt('class', __DIR__, '<<contents>>');
	}
	
	public function testWriteCollection() : void
	{
		$collection = new JsonSchemaFileCollection();
		$collection->addInterfaceFile('ifqcn', '<<<data>>>');
		$collection->addClassFile('cfqcn', '<<<data>>>');
		$collection->addTestFile('tfqcn', '<<<data>>>');
		
		$res = $this->_object->write($collection);
		
		$this->assertEquals(3, $res);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		\mkdir(__DIR__.'/temp');
		$this->_object = new JsonSchemaFileWriter(new NullLogger(), __DIR__.'/temp', __DIR__.'/temp', __DIR__.'/temp');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::tearDown()
	 */
	protected function tearDown() : void
	{
		if(\is_dir(__DIR__.'/temp'))
		{
			$files = \scandir(__DIR__.'/temp');
			
			foreach($files as $file)
			{
				$path = __DIR__.'/temp/'.$file;
				if(\is_file($path))
				{
					\unlink($path);
				}
			}
			
			\rmdir(__DIR__.'/temp');
		}
	}
	
}
