<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\JsonSchema\JsonSchemaFileCollection;
use PHPUnit\Framework\TestCase;

/**
 * JsonSchemaFileCollectionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\JsonSchemaFileCollection
 * @internal
 * @small
 */
class JsonSchemaFileCollectionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var JsonSchemaFileCollection
	 */
	protected JsonSchemaFileCollection $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetInterfaces() : void
	{
		$this->assertEquals([], $this->_object->getInterfaceFiles());
		$this->_object->addInterfaceFile('fqcn', '<<<data>>>');
		$this->assertEquals(['fqcn' => '<<<data>>>'], $this->_object->getInterfaceFiles());
	}
	
	public function testGetClasses() : void
	{
		$this->assertEquals([], $this->_object->getClassFiles());
		$this->_object->addClassFile('fqcn', '<<<data>>>');
		$this->assertEquals(['fqcn' => '<<<data>>>'], $this->_object->getClassFiles());
	}
	
	public function testGetTests() : void
	{
		$this->assertEquals([], $this->_object->getTestFiles());
		$this->_object->addTestFile('fqcn', '<<<data>>>');
		$this->assertEquals(['fqcn' => '<<<data>>>'], $this->_object->getTestFiles());
	}
	
	public function testMergeWith() : void
	{
		$new = new JsonSchemaFileCollection();
		$new->addInterfaceFile('fqcn', '<<<data>>>');
		$new->addClassFile('fqcn', '<<<data>>>');
		$new->addTestFile('fqcn', '<<<data>>>');
		
		$this->assertEquals($new, $this->_object->mergeWith($new));
		$this->assertEquals($new, $new->mergeWith($this->_object));
	}
	
	public function testMergeWithNull() : void
	{
		$this->assertEquals($this->_object, $this->_object->mergeWith(null));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new JsonSchemaFileCollection();
	}
	
}
