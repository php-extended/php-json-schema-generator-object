<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Email\EmailAddressInterface;
use PhpExtended\Email\MailboxListInterface;
use PhpExtended\HttpClient\AcceptLanguageChainInterface;
use PhpExtended\JsonSchema\JsonSchema;
use PhpExtended\JsonSchema\JsonSchemaArray;
use PhpExtended\JsonSchema\JsonSchemaBoolean;
use PhpExtended\JsonSchema\JsonSchemaFloat;
use PhpExtended\JsonSchema\JsonSchemaInteger;
use PhpExtended\JsonSchema\JsonSchemaObject;
use PhpExtended\JsonSchema\JsonSchemaResolver;
use PhpExtended\JsonSchema\JsonSchemaString;
use PhpExtended\JsonSchema\JsonSchemaTypeVisitor;
use PhpExtended\JsonSchema\NamespaceHolder;
use PhpExtended\JsonSchema\NamespaceHolderInterface;
use PhpExtended\JsonSchema\PhpArrayType;
use PhpExtended\JsonSchema\PhpSingleType;
use PhpExtended\Uuid\UuidInterface;
use PhpExtended\Version\VersionInterface;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\UriInterface;

/**
 * JsonSchemaTypeVisitorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\JsonSchemaTypeVisitor
 * @internal
 * @small
 */
class JsonSchemaTypeVisitorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var JsonSchemaTypeVisitor
	 */
	protected JsonSchemaTypeVisitor $_object;
	
	/**
	 * The namespace.
	 * 
	 * @var NamespaceHolderInterface
	 */
	protected NamespaceHolderInterface $_namespace;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testVisitSchemaRawBoolean() : void
	{
		$schema = new JsonSchemaBoolean();
		
		$this->assertEquals(new PhpSingleType('bool', 'bool'), $this->_object->handle('key', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaRawNonBoolean() : void
	{
		$this->expectException(LogicException::class);
		
		$this->_object->handle('key', new JsonSchema(), $this->_namespace);
	}
	
	public function testVisitSchemaArray() : void
	{
		$schema = new JsonSchemaArray();
		$schema->setType('array');
		$items = new JsonSchemaInteger();
		$items->setType('integer');
		$schema->setItems($items);
		
		$this->assertEquals(new PhpArrayType(PhpArrayType::KEY_TYPE_INT_ONLY, new PhpSingleType('int', 'int')), $this->_object->handle('key', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaArrayNotAList() : void
	{
		$this->expectException(LogicException::class);
		
		$this->_object->handle('key', new JsonSchemaArray(), $this->_namespace);
	}
	
	public function testVisitSchemaArrayOutOfOrder() : void
	{
		$this->expectException(LogicException::class);
		
		$schema = new JsonSchemaArray();
		$schema->setItems(new JsonSchema());
		
		$this->_object->visitSchemaArray($schema);
	}
	
	public function testVisitSchemaFloat() : void
	{
		$this->assertEquals(new PhpSingleType('float', 'float'), $this->_object->handle('key', new JsonSchemaFloat(), $this->_namespace));
	}
	
	public function testVisitSchemaInteger() : void
	{
		$this->assertEquals(new PhpSingleType('int', 'int'), $this->_object->handle('key', new JsonSchemaInteger(), $this->_namespace));
	}
	
	public function testVisitSchemaObject() : void
	{
		$this->assertEquals(new PhpSingleType('Interface\\Namespace\\ObjectInterface', 'Interface\\Namespace\\ObjectInterface'), $this->_object->handle('Object', new JsonSchemaObject(), $this->_namespace));
	}
	
	public function testVisitSchemaMap() : void
	{
		$schema = new JsonSchemaObject();
		$schema->setAdditionalProperties(new JsonSchemaString());
		
		$this->assertEquals(new PhpArrayType(PhpArrayType::KEY_TYPE_STR_ONLY, new PhpSingleType('string', 'string')), $this->_object->handle('Object', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaObjectOutOfOrder() : void
	{
		$this->expectException(LogicException::class);
		
		$this->_object->visitSchemaObject(new JsonSchemaObject());
	}
	
	public function testVisitSchemaString() : void
	{
		$this->assertEquals(new PhpSingleType('string', 'string'), $this->_object->handle('key', new JsonSchemaString(), $this->_namespace));
	}
	
	public function testVisitSchemaAcceptLanguage() : void
	{
		$schema = new JsonSchemaString();
		$schema->setFormat('accept-language-chain');
		
		$this->assertEquals(new PhpSingleType(AcceptLanguageChainInterface::class, AcceptLanguageChainInterface::class), $this->_object->handle('key', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaDate() : void
	{
		$schema = new JsonSchemaString();
		$schema->setFormat('date');
		
		$this->assertEquals(new PhpSingleType(DateTimeInterface::class, DateTimeInterface::class), $this->_object->handle('key', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaDateTime() : void
	{
		$schema = new JsonSchemaString();
		$schema->setFormat('date-time');
		
		$this->assertEquals(new PhpSingleType(DateTimeInterface::class, DateTimeInterface::class), $this->_object->handle('key', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaEmail() : void
	{
		$schema = new JsonSchemaString();
		$schema->setFormat('email');
		
		$this->assertEquals(new PhpSingleType(EmailAddressInterface::class, EmailAddressInterface::class), $this->_object->handle('key', $schema, $this->_namespace));
	}
	
	public function testVisitMailboxList() : void
	{
		$schema = new JsonSchemaString();
		$schema->setFormat('mailbox-list');
		
		$this->assertEquals(new PhpSingleType(MailboxListInterface::class, MailboxListInterface::class), $this->_object->handle('key', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaTime() : void
	{
		$schema = new JsonSchemaString();
		$schema->setFormat('time');
		
		$this->assertEquals(new PhpSingleType(DateTimeInterface::class, DateTimeInterface::class), $this->_object->handle('key', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaUri() : void
	{
		$schema = new JsonSchemaString();
		$schema->setFormat('uri');
		
		$this->assertEquals(new PhpSingleType(UriInterface::class, UriInterface::class), $this->_object->handle('key', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaUuid() : void
	{
		$schema = new JsonSchemaString();
		$schema->setFormat('uuid');
		
		$this->assertEquals(new PhpSingleType(UuidInterface::class, UuidInterface::class), $this->_object->handle('key', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaVersion() : void
	{
		$schema = new JsonSchemaString();
		$schema->setFormat('version');
		
		$this->assertEquals(new PhpSingleType(VersionInterface::class, VersionInterface::class), $this->_object->handle('key', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaStringUnavailable() : void
	{
		$this->expectException(LogicException::class);
		
		$schema = new JsonSchemaString();
		$schema->setFormat('unavailable-format');
		
		$this->_object->handle('key', $schema, $this->_namespace);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_namespace = new NamespaceHolder('Interface\\Namespace', 'Class\\Namespace', 'Tests\\Namespace');
		$resolverMock = $this->createMock(JsonSchemaResolver::class);
		$resolverMock->expects($this->any())->method('resolveNamespace')->willReturn($this->_namespace);
		$resolverMock->expects($this->any())->method('resolveSchema')->willReturnArgument(0);
		$this->_object = new JsonSchemaTypeVisitor($resolverMock);
	}
	
}
