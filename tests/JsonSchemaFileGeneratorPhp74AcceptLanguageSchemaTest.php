<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\JsonSchema\JsonSchemaFileGeneratorPhp74;
use PhpExtended\JsonSchema\JsonSchemaProvider;
use PhpExtended\JsonSchema\NamespaceHolder;
use PHPUnit\Framework\TestCase;

/**
 * JsonSchemaFileGeneratorPhp74Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\JsonSchemaFileGeneratorPhp74
 * @internal
 * @small
 */
class JsonSchemaFileGeneratorPhp74AcceptLanguageSchemaTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var JsonSchemaFileGeneratorPhp74
	 */
	protected JsonSchemaFileGeneratorPhp74 $_object;
	
	public function testGenerateAcceptLanguage() : void
	{
		$provider = new JsonSchemaProvider();
		$schema = $provider->provideFromString('{
	"$id" : "'.__FILE__.'",
	"type": "null",
	"$defs": {
		"ClassName": {
			"type": "object",
			"description": "This is a description",
			"properties": {
				"field_accept_language": {
					"type": "string",
					"description": "The description of the field",
					"format": "accept-language-chain"
				}
			}
		}
	}
}');
		
		$collection = $this->_object->generate($schema);
		
		$interface = '<?php declare(strict_types=1);

/*
 * This file is part of the interfaces/package-name library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Interfaces\\Namespace;

use PhpExtended\\HttpClient\\AcceptLanguageChainInterface;
use Stringable;

/**
 * ClassNameInterface interface file.
 * 
 * This is a description
 * 
 * /!\\ This file was generated automatically from the json-schema.json file.
 * /!\\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\\JsonSchema\\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ClassNameInterface extends Stringable
{
	
	/**
	 * Gets the description of the field
	 * 
	 * @return ?AcceptLanguageChainInterface
	 */
	public function getFieldAcceptLanguage() : ?AcceptLanguageChainInterface;
	
}
';
		
		$class = '<?php declare(strict_types=1);

/*
 * This file is part of the classes/package-name library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Classes\\Namespace;

use Interfaces\\Namespace\\ClassNameInterface;
use PhpExtended\\HttpClient\\AcceptLanguageChainInterface;

/**
 * ClassName class file.
 * 
 * This is a simple implementation of the ClassNameInterface.
 * 
 * /!\\ This file was generated automatically from the json-schema.json file.
 * /!\\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\\JsonSchema\\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ClassName implements ClassNameInterface
{
	
	/**
	 * The description of the field
	 * 
	 * @var ?AcceptLanguageChainInterface
	 */
	protected ?AcceptLanguageChainInterface $_fieldAcceptLanguage = null;
	
	/**
	 * {@inheritDoc}
	 * @see \\Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.\'@\'.\\spl_object_hash($this);
	}
	
	/**
	 * Sets the description of the field
	 * 
	 * @param ?AcceptLanguageChainInterface $fieldAcceptLanguage
	 * @return ClassNameInterface
	 */
	public function setFieldAcceptLanguage(?AcceptLanguageChainInterface $fieldAcceptLanguage) : ClassNameInterface
	{
		$this->_fieldAcceptLanguage = $fieldAcceptLanguage;
		
		return $this;
	}
	
	/**
	 * Gets the description of the field
	 * 
	 * @return ?AcceptLanguageChainInterface
	 */
	public function getFieldAcceptLanguage() : ?AcceptLanguageChainInterface
	{
		return $this->_fieldAcceptLanguage;
	}
	
}
';
		
		$test = '<?php declare(strict_types=1);

/*
 * This file is part of the tests/package-name library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Tests\\Namespace;

use Classes\\Namespace\\ClassName;
use PhpExtended\\HttpClient\\AcceptLanguageChainParser;
use PHPUnit\\Framework\\TestCase;

/**
 * ClassNameTest test file.
 * 
 * /!\\ This file was generated automatically from the json-schema.json file.
 * /!\\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\\JsonSchema\\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \\Classes\\Namespace\\ClassName
 * @internal
 * @small
 */
class ClassNameTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ClassName
	 */
	protected ClassName $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\\get_class($this->_object).\'@\'.\\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetFieldAcceptLanguage() : void
	{
		$this->assertNull($this->_object->getFieldAcceptLanguage());
		$expected = (new AcceptLanguageChainParser())->parse(\'fr-FR,en-US;q=0.8,en;q=0.5\');
		$this->_object->setFieldAcceptLanguage($expected);
		$this->assertEquals($expected, $this->_object->getFieldAcceptLanguage());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \\PHPUnit\\Framework\\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ClassName();
	}
	
}
';
		
		$this->assertCount(1, $collection->getInterfaceFiles());
		$this->assertEquals($interface, \array_values($collection->getInterfaceFiles())[0], 'interface');
		$this->assertCount(1, $collection->getClassFiles());
		$this->assertEquals($class, \array_values($collection->getClassFiles())[0], 'class');
		$this->assertCount(1, $collection->getTestFiles());
		$this->assertEquals($test, \array_values($collection->getTestFiles())[0], 'test');
	}
	
	public function testGenerateAcceptLanguageMandatory() : void
	{
		$provider = new JsonSchemaProvider();
		$schema = $provider->provideFromString('{
	"$id" : "'.__FILE__.'",
	"type": "null",
	"$defs": {
		"ClassName": {
			"type": "object",
			"description": "This is a description",
			"properties": {
				"field_accept_language": {
					"type": "string",
					"description": "The description of the field",
					"format": "accept-language-chain"
				}
			},
			"required" : ["field_accept_language"]
		}
	}
}');
		
		$collection = $this->_object->generate($schema);
		
		$interface = '<?php declare(strict_types=1);

/*
 * This file is part of the interfaces/package-name library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Interfaces\\Namespace;

use PhpExtended\\HttpClient\\AcceptLanguageChainInterface;
use Stringable;

/**
 * ClassNameInterface interface file.
 * 
 * This is a description
 * 
 * /!\\ This file was generated automatically from the json-schema.json file.
 * /!\\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\\JsonSchema\\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ClassNameInterface extends Stringable
{
	
	/**
	 * Gets the description of the field
	 * 
	 * @return AcceptLanguageChainInterface
	 */
	public function getFieldAcceptLanguage() : AcceptLanguageChainInterface;
	
}
';
		
		$class = '<?php declare(strict_types=1);

/*
 * This file is part of the classes/package-name library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Classes\\Namespace;

use Interfaces\\Namespace\\ClassNameInterface;
use PhpExtended\\HttpClient\\AcceptLanguageChainInterface;

/**
 * ClassName class file.
 * 
 * This is a simple implementation of the ClassNameInterface.
 * 
 * /!\\ This file was generated automatically from the json-schema.json file.
 * /!\\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\\JsonSchema\\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ClassName implements ClassNameInterface
{
	
	/**
	 * The description of the field
	 * 
	 * @var AcceptLanguageChainInterface
	 */
	protected AcceptLanguageChainInterface $_fieldAcceptLanguage;
	
	/**
	 * Constructor for ClassName with private members.
	 * 
	 * @param AcceptLanguageChainInterface $fieldAcceptLanguage
	 */
	public function __construct(AcceptLanguageChainInterface $fieldAcceptLanguage)
	{
		$this->setFieldAcceptLanguage($fieldAcceptLanguage);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \\Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.\'@\'.\\spl_object_hash($this);
	}
	
	/**
	 * Sets the description of the field
	 * 
	 * @param AcceptLanguageChainInterface $fieldAcceptLanguage
	 * @return ClassNameInterface
	 */
	public function setFieldAcceptLanguage(AcceptLanguageChainInterface $fieldAcceptLanguage) : ClassNameInterface
	{
		$this->_fieldAcceptLanguage = $fieldAcceptLanguage;
		
		return $this;
	}
	
	/**
	 * Gets the description of the field
	 * 
	 * @return AcceptLanguageChainInterface
	 */
	public function getFieldAcceptLanguage() : AcceptLanguageChainInterface
	{
		return $this->_fieldAcceptLanguage;
	}
	
}
';
		
		$test = '<?php declare(strict_types=1);

/*
 * This file is part of the tests/package-name library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Tests\\Namespace;

use Classes\\Namespace\\ClassName;
use PhpExtended\\HttpClient\\AcceptLanguageChainParser;
use PHPUnit\\Framework\\TestCase;

/**
 * ClassNameTest test file.
 * 
 * /!\\ This file was generated automatically from the json-schema.json file.
 * /!\\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\\JsonSchema\\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \\Classes\\Namespace\\ClassName
 * @internal
 * @small
 */
class ClassNameTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ClassName
	 */
	protected ClassName $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\\get_class($this->_object).\'@\'.\\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetFieldAcceptLanguage() : void
	{
		$this->assertEquals((new AcceptLanguageChainParser())->parse(\'en-US\'), $this->_object->getFieldAcceptLanguage());
		$expected = (new AcceptLanguageChainParser())->parse(\'fr-FR,en-US;q=0.8,en;q=0.5\');
		$this->_object->setFieldAcceptLanguage($expected);
		$this->assertEquals($expected, $this->_object->getFieldAcceptLanguage());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \\PHPUnit\\Framework\\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ClassName((new AcceptLanguageChainParser())->parse(\'en-US\'));
	}
	
}
';
		
		$this->assertCount(1, $collection->getInterfaceFiles());
		$this->assertEquals($interface, \array_values($collection->getInterfaceFiles())[0], 'interface');
		$this->assertCount(1, $collection->getClassFiles());
		$this->assertEquals($class, \array_values($collection->getClassFiles())[0], 'class');
		$this->assertCount(1, $collection->getTestFiles());
		$this->assertEquals($test, \array_values($collection->getTestFiles())[0], 'test');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$namespaces = new NamespaceHolder('Interfaces\\Namespace', 'Classes\\Namespace', 'Tests\\Namespace');
		$this->_object = new JsonSchemaFileGeneratorPhp74(
			$namespaces,
			'interfaces/package-name',
			'classes/package-name',
			'tests/package-name',
		);
	}
	
}
