<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Email\EmailAddressParser;
use PhpExtended\Email\MailboxListParser;
use PhpExtended\HttpClient\AcceptLanguageChainParser;
use PhpExtended\JsonSchema\JsonSchema;
use PhpExtended\JsonSchema\JsonSchemaArray;
use PhpExtended\JsonSchema\JsonSchemaFloat;
use PhpExtended\JsonSchema\JsonSchemaInteger;
use PhpExtended\JsonSchema\JsonSchemaObject;
use PhpExtended\JsonSchema\JsonSchemaResolver;
use PhpExtended\JsonSchema\JsonSchemaString;
use PhpExtended\JsonSchema\JsonSchemaTestUsesVisitor;
use PhpExtended\JsonSchema\NamespaceHolder;
use PhpExtended\JsonSchema\NamespaceHolderInterface;
use PhpExtended\Uri\UriParser;
use PhpExtended\Uuid\UuidParser;
use PhpExtended\Version\VersionParser;
use PHPUnit\Framework\TestCase;

/**
 * JsonSchemaTestUsesVisitorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\JsonSchemaTestUsesVisitor
 * @internal
 * @small
 */
class JsonSchemaTestUsesVisitorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var JsonSchemaTestUsesVisitor
	 */
	protected JsonSchemaTestUsesVisitor $_object;
	
	/**
	 * The namespace.
	 *
	 * @var NamespaceHolderInterface
	 */
	protected NamespaceHolderInterface $_namespace;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testVisitSchemaRawBoolean() : void
	{
		$this->assertEquals([], $this->_object->handle('key', new JsonSchema(), $this->_namespace));
	}
	
	public function testVisitSchemaArray() : void
	{
		$schema = new JsonSchemaArray();
		$schema->setType('array');
		$items = new JsonSchemaInteger();
		$items->setType('integer');
		$schema->setItems($items);
		
		$this->assertEquals([], $this->_object->handle('key', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaArrayOfObjects() : void
	{
		$schema = new JsonSchemaArray();
		$schema->setType('array');
		$items = new JsonSchemaObject();
		$items->setType('object');
		$schema->setItems($items);
		
		$this->assertEquals(['Class\\Namespace\\TargetClass'], $this->_object->handle('targetClass', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaArrayNotList() : void
	{
		$this->expectException(LogicException::class);
		
		$this->_object->handle('key', new JsonSchemaArray(), $this->_namespace);
	}
	
	public function testVisitSchemaArrayOutOfOrder() : void
	{
		$this->expectException(LogicException::class);
		
		$schema = new JsonSchemaArray();
		$schema->setItems(new JsonSchema());
		
		$this->_object->visit($schema);
	}
	
	public function testVisitSchemaFloat() : void
	{
		$this->assertEquals([], $this->_object->handle('key', new JsonSchemaFloat(), $this->_namespace));
	}
	
	public function testVisitSchemaInteger() : void
	{
		$this->assertEquals([], $this->_object->handle('key', new JsonSchemaInteger(), $this->_namespace));
	}
	
	public function testVisitSchemaObject() : void
	{
		$this->assertEquals(['Class\\Namespace\\ClassName'], $this->_object->handle('ClassName', new JsonSchemaObject(), $this->_namespace));
	}
	
	public function testVisitSchemaMap() : void
	{
		$schema = new JsonSchemaObject();
		$schema->setAdditionalProperties(new JsonSchemaString());
		
		$this->assertEquals([], $this->_object->handle('ClassName', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaObjectOutOfOrder() : void
	{
		$this->expectException(LogicException::class);
		
		$this->_object->visitSchemaObject(new JsonSchemaObject());
	}
	
	public function testVisitSchemaString() : void
	{
		$this->assertEquals([], $this->_object->handle('key', new JsonSchemaString(), $this->_namespace));
	}
	
	public function testVisitSchemaAcceptLanguage() : void
	{
		$schema = new JsonSchemaString();
		$schema->setFormat('accept-language-chain');
		
		$this->assertEquals([AcceptLanguageChainParser::class], $this->_object->handle('key', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaDate() : void
	{
		$schema = new JsonSchemaString();
		$schema->setFormat('date');
		
		$this->assertEquals([DateTimeImmutable::class], $this->_object->handle('key', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaDateTime() : void
	{
		$schema = new JsonSchemaString();
		$schema->setFormat('date-time');
		
		$this->assertEquals([DateTimeImmutable::class], $this->_object->handle('key', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaEmail() : void
	{
		$schema = new JsonSchemaString();
		$schema->setFormat('email');
		
		$this->assertEquals([EmailAddressParser::class], $this->_object->handle('key', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaMailboxList() : void
	{
		$schema = new JsonSchemaString();
		$schema->setFormat('mailbox-list');
		
		$this->assertEquals([MailboxListParser::class], $this->_object->handle('key', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaTime() : void
	{
		$schema = new JsonSchemaString();
		$schema->setFormat('time');
		
		$this->assertEquals([DateTimeImmutable::class], $this->_object->handle('key', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaUri() : void
	{
		$schema = new JsonSchemaString();
		$schema->setFormat('uri');
		
		$this->assertEquals([UriParser::class], $this->_object->handle('key', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaUuid() : void
	{
		$schema = new JsonSchemaString();
		$schema->setFormat('uuid');
		
		$this->assertEquals([UuidParser::class], $this->_object->handle('key', $schema, $this->_namespace));
	}
	
	public function testVersionSchemaVersion() : void
	{
		$schema = new JsonSchemaString();
		$schema->setFormat('version');
		
		$this->assertEquals([VersionParser::class], $this->_object->handle('key', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaStringUnavailable() : void
	{
		$this->expectException(LogicException::class);
		
		$schema = new JsonSchemaString();
		$schema->setFormat('unavailable-format');
		
		$this->_object->handle('key', $schema, $this->_namespace);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_namespace = new NamespaceHolder('Interface\\Namespace', 'Class\\Namespace', 'Tests\\Namespace');
		$resolverMock = $this->createMock(JsonSchemaResolver::class);
		$resolverMock->expects($this->any())->method('resolveNamespace')->willReturn($this->_namespace);
		$resolverMock->expects($this->any())->method('resolveSchema')->willReturnArgument(0);
		$this->_object = new JsonSchemaTestUsesVisitor($resolverMock);
	}
	
}
