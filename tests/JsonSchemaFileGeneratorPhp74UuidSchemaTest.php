<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\JsonSchema\JsonSchemaFileGeneratorPhp74;
use PhpExtended\JsonSchema\JsonSchemaProvider;
use PhpExtended\JsonSchema\NamespaceHolder;
use PHPUnit\Framework\TestCase;

/**
 * JsonSchemaFileGeneratorPhp74Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\JsonSchemaFileGeneratorPhp74
 * @internal
 * @small
 */
class JsonSchemaFileGeneratorPhp74UuidSchemaTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var JsonSchemaFileGeneratorPhp74
	 */
	protected JsonSchemaFileGeneratorPhp74 $_object;
	
	public function testGenerateUri() : void
	{
		$provider = new JsonSchemaProvider();
		$schema = $provider->provideFromString('{
	"$id" : "'.__FILE__.'",
	"type": "null",
	"$defs": {
		"ClassName": {
			"type": "object",
			"description": "This is a description",
			"properties": {
				"field_uuid": {
					"type": "string",
					"description": "The description of the field",
					"format": "uuid"
				}
			}
		}
	}
}');
		
		$collection = $this->_object->generate($schema);
		
		$interface = '<?php declare(strict_types=1);

/*
 * This file is part of the interfaces/package-name library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Interfaces\\Namespace;

use PhpExtended\\Uuid\\UuidInterface;
use Stringable;

/**
 * ClassNameInterface interface file.
 * 
 * This is a description
 * 
 * /!\\ This file was generated automatically from the json-schema.json file.
 * /!\\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\\JsonSchema\\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ClassNameInterface extends Stringable
{
	
	/**
	 * Gets the description of the field
	 * 
	 * @return ?UuidInterface
	 */
	public function getFieldUuid() : ?UuidInterface;
	
}
';
		
		$class = '<?php declare(strict_types=1);

/*
 * This file is part of the classes/package-name library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Classes\\Namespace;

use Interfaces\\Namespace\\ClassNameInterface;
use PhpExtended\\Uuid\\UuidInterface;

/**
 * ClassName class file.
 * 
 * This is a simple implementation of the ClassNameInterface.
 * 
 * /!\\ This file was generated automatically from the json-schema.json file.
 * /!\\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\\JsonSchema\\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ClassName implements ClassNameInterface
{
	
	/**
	 * The description of the field
	 * 
	 * @var ?UuidInterface
	 */
	protected ?UuidInterface $_fieldUuid = null;
	
	/**
	 * {@inheritDoc}
	 * @see \\Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.\'@\'.\\spl_object_hash($this);
	}
	
	/**
	 * Sets the description of the field
	 * 
	 * @param ?UuidInterface $fieldUuid
	 * @return ClassNameInterface
	 */
	public function setFieldUuid(?UuidInterface $fieldUuid) : ClassNameInterface
	{
		$this->_fieldUuid = $fieldUuid;
		
		return $this;
	}
	
	/**
	 * Gets the description of the field
	 * 
	 * @return ?UuidInterface
	 */
	public function getFieldUuid() : ?UuidInterface
	{
		return $this->_fieldUuid;
	}
	
}
';
		
		$test = '<?php declare(strict_types=1);

/*
 * This file is part of the tests/package-name library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Tests\\Namespace;

use Classes\\Namespace\\ClassName;
use PhpExtended\\Uuid\\UuidParser;
use PHPUnit\\Framework\\TestCase;

/**
 * ClassNameTest test file.
 * 
 * /!\\ This file was generated automatically from the json-schema.json file.
 * /!\\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\\JsonSchema\\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \\Classes\\Namespace\\ClassName
 * @internal
 * @small
 */
class ClassNameTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ClassName
	 */
	protected ClassName $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\\get_class($this->_object).\'@\'.\\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetFieldUuid() : void
	{
		$this->assertNull($this->_object->getFieldUuid());
		$expected = (new UuidParser())->parse(\'b592ccab-7e43-4de9-8460-34592395addb\');
		$this->_object->setFieldUuid($expected);
		$this->assertEquals($expected, $this->_object->getFieldUuid());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \\PHPUnit\\Framework\\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ClassName();
	}
	
}
';
		
		$this->assertCount(1, $collection->getInterfaceFiles());
		$this->assertEquals($interface, \array_values($collection->getInterfaceFiles())[0], 'interface');
		$this->assertCount(1, $collection->getClassFiles());
		$this->assertEquals($class, \array_values($collection->getClassFiles())[0], 'class');
		$this->assertCount(1, $collection->getTestFiles());
		$this->assertEquals($test, \array_values($collection->getTestFiles())[0], 'test');
	}
	
	public function testGenerateUriMandatory() : void
	{
		$provider = new JsonSchemaProvider();
		$schema = $provider->provideFromString('{
	"$id" : "'.__FILE__.'",
	"type": "null",
	"$defs": {
		"ClassName": {
			"type": "object",
			"description": "This is a description",
			"properties": {
				"field_uuid": {
					"type": "string",
					"description": "The description of the field",
					"format": "uuid"
				}
			},
			"required" : ["field_uuid"]
		}
	}
}');
		
		$collection = $this->_object->generate($schema);
		
		$interface = '<?php declare(strict_types=1);

/*
 * This file is part of the interfaces/package-name library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Interfaces\\Namespace;

use PhpExtended\\Uuid\\UuidInterface;
use Stringable;

/**
 * ClassNameInterface interface file.
 * 
 * This is a description
 * 
 * /!\\ This file was generated automatically from the json-schema.json file.
 * /!\\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\\JsonSchema\\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ClassNameInterface extends Stringable
{
	
	/**
	 * Gets the description of the field
	 * 
	 * @return UuidInterface
	 */
	public function getFieldUuid() : UuidInterface;
	
}
';
		
		$class = '<?php declare(strict_types=1);

/*
 * This file is part of the classes/package-name library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Classes\\Namespace;

use Interfaces\\Namespace\\ClassNameInterface;
use PhpExtended\\Uuid\\UuidInterface;

/**
 * ClassName class file.
 * 
 * This is a simple implementation of the ClassNameInterface.
 * 
 * /!\\ This file was generated automatically from the json-schema.json file.
 * /!\\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\\JsonSchema\\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ClassName implements ClassNameInterface
{
	
	/**
	 * The description of the field
	 * 
	 * @var UuidInterface
	 */
	protected UuidInterface $_fieldUuid;
	
	/**
	 * Constructor for ClassName with private members.
	 * 
	 * @param UuidInterface $fieldUuid
	 */
	public function __construct(UuidInterface $fieldUuid)
	{
		$this->setFieldUuid($fieldUuid);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \\Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.\'@\'.\\spl_object_hash($this);
	}
	
	/**
	 * Sets the description of the field
	 * 
	 * @param UuidInterface $fieldUuid
	 * @return ClassNameInterface
	 */
	public function setFieldUuid(UuidInterface $fieldUuid) : ClassNameInterface
	{
		$this->_fieldUuid = $fieldUuid;
		
		return $this;
	}
	
	/**
	 * Gets the description of the field
	 * 
	 * @return UuidInterface
	 */
	public function getFieldUuid() : UuidInterface
	{
		return $this->_fieldUuid;
	}
	
}
';
		
		$test = '<?php declare(strict_types=1);

/*
 * This file is part of the tests/package-name library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Tests\\Namespace;

use Classes\\Namespace\\ClassName;
use PhpExtended\\Uuid\\UuidParser;
use PHPUnit\\Framework\\TestCase;

/**
 * ClassNameTest test file.
 * 
 * /!\\ This file was generated automatically from the json-schema.json file.
 * /!\\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\\JsonSchema\\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \\Classes\\Namespace\\ClassName
 * @internal
 * @small
 */
class ClassNameTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ClassName
	 */
	protected ClassName $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\\get_class($this->_object).\'@\'.\\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetFieldUuid() : void
	{
		$this->assertEquals((new UuidParser())->parse(\'9e2cfea1-afc9-4ecb-8a90-d477a5a90cfa\'), $this->_object->getFieldUuid());
		$expected = (new UuidParser())->parse(\'b592ccab-7e43-4de9-8460-34592395addb\');
		$this->_object->setFieldUuid($expected);
		$this->assertEquals($expected, $this->_object->getFieldUuid());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \\PHPUnit\\Framework\\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ClassName((new UuidParser())->parse(\'9e2cfea1-afc9-4ecb-8a90-d477a5a90cfa\'));
	}
	
}
';
		
		$this->assertCount(1, $collection->getInterfaceFiles());
		$this->assertEquals($interface, \array_values($collection->getInterfaceFiles())[0], 'interface');
		$this->assertCount(1, $collection->getClassFiles());
		$this->assertEquals($class, \array_values($collection->getClassFiles())[0], 'class');
		$this->assertCount(1, $collection->getTestFiles());
		$this->assertEquals($test, \array_values($collection->getTestFiles())[0], 'test');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$namespaces = new NamespaceHolder('Interfaces\\Namespace', 'Classes\\Namespace', 'Tests\\Namespace');
		$this->_object = new JsonSchemaFileGeneratorPhp74(
			$namespaces,
			'interfaces/package-name',
			'classes/package-name',
			'tests/package-name',
		);
	}
	
}
