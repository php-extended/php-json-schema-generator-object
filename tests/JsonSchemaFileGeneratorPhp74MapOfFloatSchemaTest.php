<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\JsonSchema\JsonSchemaFileGeneratorPhp74;
use PhpExtended\JsonSchema\JsonSchemaProvider;
use PhpExtended\JsonSchema\NamespaceHolder;
use PHPUnit\Framework\TestCase;

/**
 * JsonSchemaFileGeneratorPhp74Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\JsonSchemaFileGeneratorPhp74
 * @internal
 * @small
 * @internal
 */
class JsonSchemaFileGeneratorPhp74MapOfFloatSchemaTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var JsonSchemaFileGeneratorPhp74
	 */
	protected JsonSchemaFileGeneratorPhp74 $_object;
	
	public function testGenerateFloat() : void
	{
		$provider = new JsonSchemaProvider();
		$schema = $provider->provideFromString('{
	"$id" : "'.__FILE__.'",
	"type": "null",
	"$defs": {
		"ClassName": {
			"type": "object",
			"description": "This is a description",
			"properties": {
				"field_map": {
					"type" : "object",
					"description": "The description of the field",
					"additionalProperties" : {
						"type": "number"
					}
				}
			}
		}
	}
}');
		
		$collection = $this->_object->generate($schema);
		
		$interface = '<?php declare(strict_types=1);

/*
 * This file is part of the interfaces/package-name library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Interfaces\\Namespace;

use Stringable;

/**
 * ClassNameInterface interface file.
 * 
 * This is a description
 * 
 * /!\\ This file was generated automatically from the json-schema.json file.
 * /!\\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\\JsonSchema\\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ClassNameInterface extends Stringable
{
	
	/**
	 * Gets the description of the field
	 * 
	 * @return array<string, float>
	 */
	public function getFieldMap() : array;
	
}
';
		
		$class = '<?php declare(strict_types=1);

/*
 * This file is part of the classes/package-name library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Classes\\Namespace;

use Interfaces\\Namespace\\ClassNameInterface;

/**
 * ClassName class file.
 * 
 * This is a simple implementation of the ClassNameInterface.
 * 
 * /!\\ This file was generated automatically from the json-schema.json file.
 * /!\\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\\JsonSchema\\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ClassName implements ClassNameInterface
{
	
	/**
	 * The description of the field
	 * 
	 * @var array<string, float>
	 */
	protected array $_fieldMap = [];
	
	/**
	 * {@inheritDoc}
	 * @see \\Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.\'@\'.\\spl_object_hash($this);
	}
	
	/**
	 * Sets the description of the field
	 * 
	 * @param array<string, float> $fieldMap
	 * @return ClassNameInterface
	 */
	public function setFieldMap(array $fieldMap) : ClassNameInterface
	{
		$this->_fieldMap = $fieldMap;
		
		return $this;
	}
	
	/**
	 * Gets the description of the field
	 * 
	 * @return array<string, float>
	 */
	public function getFieldMap() : array
	{
		return $this->_fieldMap;
	}
	
}
';
		
		$test = '<?php declare(strict_types=1);

/*
 * This file is part of the tests/package-name library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Tests\\Namespace;

use Classes\\Namespace\\ClassName;
use PHPUnit\\Framework\\TestCase;

/**
 * ClassNameTest test file.
 * 
 * /!\\ This file was generated automatically from the json-schema.json file.
 * /!\\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\\JsonSchema\\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \\Classes\\Namespace\\ClassName
 * @internal
 * @small
 */
class ClassNameTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ClassName
	 */
	protected ClassName $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\\get_class($this->_object).\'@\'.\\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetFieldMap() : void
	{
		$this->assertEquals([], $this->_object->getFieldMap());
		$expected = [\'key1\' => 15.2, \'key2\' => 15.2];
		$this->_object->setFieldMap($expected);
		$this->assertEquals($expected, $this->_object->getFieldMap());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \\PHPUnit\\Framework\\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ClassName();
	}
	
}
';
		
		$this->assertCount(1, $collection->getInterfaceFiles());
		$this->assertEquals($interface, \array_values($collection->getInterfaceFiles())[0], 'interface');
		$this->assertCount(1, $collection->getClassFiles());
		$this->assertEquals($class, \array_values($collection->getClassFiles())[0], 'class');
		$this->assertCount(1, $collection->getTestFiles());
		$this->assertEquals($test, \array_values($collection->getTestFiles())[0], 'test');
	}
	
	public function testGenerateFloatMandatory() : void
	{
		$provider = new JsonSchemaProvider();
		$schema = $provider->provideFromString('{
	"$id" : "'.__FILE__.'",
	"type": "null",
	"$defs": {
		"ClassName": {
			"type": "object",
			"description": "This is a description",
			"properties": {
				"field_map": {
					"type" : "object",
					"description": "The description of the field",
					"additionalProperties" : {
						"type": "number"
					}
				}
			},
			"required" : ["field_map"]
		}
	}
}');
		
		$collection = $this->_object->generate($schema);
		
		$interface = '<?php declare(strict_types=1);

/*
 * This file is part of the interfaces/package-name library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Interfaces\\Namespace;

use Stringable;

/**
 * ClassNameInterface interface file.
 * 
 * This is a description
 * 
 * /!\\ This file was generated automatically from the json-schema.json file.
 * /!\\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\\JsonSchema\\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ClassNameInterface extends Stringable
{
	
	/**
	 * Gets the description of the field
	 * 
	 * @return array<string, float>
	 */
	public function getFieldMap() : array;
	
}
';
		
		$class = '<?php declare(strict_types=1);

/*
 * This file is part of the classes/package-name library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Classes\\Namespace;

use Interfaces\\Namespace\\ClassNameInterface;

/**
 * ClassName class file.
 * 
 * This is a simple implementation of the ClassNameInterface.
 * 
 * /!\\ This file was generated automatically from the json-schema.json file.
 * /!\\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\\JsonSchema\\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ClassName implements ClassNameInterface
{
	
	/**
	 * The description of the field
	 * 
	 * @var array<string, float>
	 */
	protected array $_fieldMap = [];
	
	/**
	 * Constructor for ClassName with private members.
	 * 
	 * @param array<string, float> $fieldMap
	 */
	public function __construct(array $fieldMap)
	{
		$this->setFieldMap($fieldMap);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \\Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.\'@\'.\\spl_object_hash($this);
	}
	
	/**
	 * Sets the description of the field
	 * 
	 * @param array<string, float> $fieldMap
	 * @return ClassNameInterface
	 */
	public function setFieldMap(array $fieldMap) : ClassNameInterface
	{
		$this->_fieldMap = $fieldMap;
		
		return $this;
	}
	
	/**
	 * Gets the description of the field
	 * 
	 * @return array<string, float>
	 */
	public function getFieldMap() : array
	{
		return $this->_fieldMap;
	}
	
}
';
		
		$test = '<?php declare(strict_types=1);

/*
 * This file is part of the tests/package-name library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Tests\\Namespace;

use Classes\\Namespace\\ClassName;
use PHPUnit\\Framework\\TestCase;

/**
 * ClassNameTest test file.
 * 
 * /!\\ This file was generated automatically from the json-schema.json file.
 * /!\\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\\JsonSchema\\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \\Classes\\Namespace\\ClassName
 * @internal
 * @small
 */
class ClassNameTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ClassName
	 */
	protected ClassName $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\\get_class($this->_object).\'@\'.\\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetFieldMap() : void
	{
		$this->assertEquals([\'key\' => 14.1], $this->_object->getFieldMap());
		$expected = [\'key1\' => 15.2, \'key2\' => 15.2];
		$this->_object->setFieldMap($expected);
		$this->assertEquals($expected, $this->_object->getFieldMap());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \\PHPUnit\\Framework\\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ClassName([\'key\' => 14.1]);
	}
	
}
';
		
		$this->assertCount(1, $collection->getInterfaceFiles());
		$this->assertEquals($interface, \array_values($collection->getInterfaceFiles())[0], 'interface');
		$this->assertCount(1, $collection->getClassFiles());
		$this->assertEquals($class, \array_values($collection->getClassFiles())[0], 'class');
		$this->assertCount(1, $collection->getTestFiles());
		$this->assertEquals($test, \array_values($collection->getTestFiles())[0], 'test');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$namespaces = new NamespaceHolder('Interfaces\\Namespace', 'Classes\\Namespace', 'Tests\\Namespace');
		$this->_object = new JsonSchemaFileGeneratorPhp74(
			$namespaces,
			'interfaces/package-name',
			'classes/package-name',
			'tests/package-name',
		);
	}
	
}
