<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\JsonSchema\PhpAbstractType;
use PHPUnit\Framework\TestCase;

/**
 * PhpAbstractTypeTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\PhpAbstractType
 * @internal
 * @small
 */
class PhpAbstractTypeTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var PhpAbstractType
	 */
	protected PhpAbstractType $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('Namespace\\ClassName<T>', $this->_object->__toString());
	}
	
	public function testGetNamespacedDocType() : void
	{
		$this->assertEquals('Namespace\\ClassName<T>', $this->_object->getNamespacedDocType());
	}
	
	public function testGetNamespacedClassName() : void
	{
		$this->assertEquals('Namespace\\ClassName', $this->_object->getNamespacedClassName());
	}
	
	public function testGetNullable() : void
	{
		$this->assertFalse($this->_object->isNullable());
		$this->_object->setNullable(true);
		$this->assertTrue($this->_object->isNullable());
	}
	
	public function testIsArray() : void
	{
		$this->assertFalse($this->_object->isArray());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$mock = $this->getMockForAbstractClass(PhpAbstractType::class);
		$mock->expects($this->any())->method('getDocType')->willReturn('ClassName<T>');
		$mock->expects($this->any())->method('getNamespace')->willReturn('Namespace');
		$mock->expects($this->any())->method('getName')->willReturn('ClassName');
		$mock->expects($this->any())->method('collectNamespacedClasses')->willReturn(['Namespace\\ClassName']);
		$this->_object = $mock;
	}
	
}
