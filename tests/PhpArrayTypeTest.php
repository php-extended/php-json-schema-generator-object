<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\JsonSchema\PhpArrayType;
use PhpExtended\JsonSchema\PhpSingleType;
use PHPUnit\Framework\TestCase;

/**
 * PhpArrayTypeTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\PhpArrayType
 * @internal
 * @small
 */
class PhpArrayTypeTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var PhpArrayType
	 */
	protected PhpArrayType $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('array<int, array<int, float>>', $this->_object->__toString());
	}
	
	public function testGetDocType() : void
	{
		$this->assertEquals('array<int, array<int, float>>', $this->_object->getDocType());
	}
	
	public function testGetNamespace() : void
	{
		$this->assertEquals('', $this->_object->getNamespace());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('array', $this->_object->getName());
	}
	
	public function testCollectNamespacedClasses() : void
	{
		$this->assertEquals([], $this->_object->collectNamespacedClasses());
	}
	
	public function testIsNullable() : void
	{
		$this->assertFalse($this->_object->isNullable());
	}
	
	public function testIsArray() : void
	{
		$this->assertTrue($this->_object->isArray());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new PhpArrayType(PhpArrayType::KEY_TYPE_INT_ONLY, new PhpArrayType(PhpArrayType::KEY_TYPE_INT_ONLY, new PhpSingleType('float', 'float')));
	}
	
}
