<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\JsonSchema\JsonSchema;
use PhpExtended\JsonSchema\JsonSchemaBoolean;
use PhpExtended\JsonSchema\JsonSchemaResolver;
use PhpExtended\JsonSchema\NamespaceHolder;
use PhpExtended\JsonSchema\Php74CompoundMetadata;
use PhpExtended\JsonSchema\Php74FieldMetadata;
use PhpExtended\JsonSchema\Php74MetadataProvider;
use PhpExtended\JsonSchema\Php74MethodMetadata;
use PhpExtended\JsonSchema\PhpSingleType;
use PHPUnit\Framework\TestCase;

/**
 * Php74MetadataProviderTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\Php74MetadataProvider
 * @internal
 * @small
 */
class Php74MetadataProviderTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Php74MetadataProvider
	 */
	protected Php74MetadataProvider $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetField() : void
	{
		$schema = new JsonSchemaBoolean();
		
		$this->assertInstanceOf(Php74FieldMetadata::class, $this->_object->getFieldFrom('key', $schema, new NamespaceHolder('I', 'C', 'T'), true));
	}
	
	public function testGetGetter() : void
	{
		$schema = new JsonSchemaBoolean();
		
		$this->assertInstanceOf(Php74MethodMetadata::class, $this->_object->getGetterFrom('key', $schema, new NamespaceHolder('I', 'C', 'T'), true));
	}
	
	public function testGetSetter() : void
	{
		$schema = new JsonSchemaBoolean();
		
		$this->assertInstanceOf(Php74MethodMetadata::class, $this->_object->getSetterFrom('key', $schema, new NamespaceHolder('I', 'C', 'T'), true, new PhpSingleType('int', 'int')));
	}
	
	public function testGetCompound() : void
	{
		$schema = new JsonSchemaBoolean();
		
		$this->assertInstanceOf(Php74CompoundMetadata::class, $this->_object->getCompoundFor('key', $this->createMock(Php74FieldMetadata::class), $this->createMock(Php74MethodMetadata::class), $this->createMock(Php74MethodMetadata::class), $schema, new NamespaceHolder('I', 'C', 'T')));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$resolverMock = $this->createMock(JsonSchemaResolver::class);
		$resolverMock->expects($this->any())->method('resolveSchema')->willReturn(new JsonSchema());
		$resolverMock->expects($this->any())->method('getDefaultSchema')->willReturn(new JsonSchema());
		$resolverMock->expects($this->any())->method('resolveNamespace')->willReturn(new NamespaceHolder('Interfaces\\Namespace', 'Classes\\Namespace', 'Tests\\Namespaces'));
		$resolverMock->expects($this->any())->method('getDefaultNamespace')->willReturn(new NamespaceHolder('Interfaces\\Namespace', 'Classes\\Namespace', 'Tests\\Namespaces'));
		$this->_object = new Php74MetadataProvider($resolverMock);
	}
	
}
