<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\JsonSchema\NamespaceHolder;
use PHPUnit\Framework\TestCase;

/**
 * NamespaceHolderTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\NamespaceHolder
 * @internal
 * @small
 */
class NamespaceHolderTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var NamespaceHolder
	 */
	protected NamespaceHolder $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetInterfaceNamespace() : void
	{
		$this->assertEquals('Interfaces\\Namespace', $this->_object->getInterfaceNamespace());
	}
	
	public function testGetClassNamespace() : void
	{
		$this->assertEquals('Classes\\Namespace', $this->_object->getClassNamespace());
	}
	
	public function testGetTestsNamespace() : void
	{
		$this->assertEquals('Tests\\Namespace', $this->_object->getTestNamespace());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new NamespaceHolder('Interfaces\\Namespace', 'Classes\\Namespace', 'Tests\\Namespace');
	}
	
}
