<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\JsonSchema\JsonSchema;
use PhpExtended\JsonSchema\JsonSchemaArray;
use PhpExtended\JsonSchema\JsonSchemaBoolean;
use PhpExtended\JsonSchema\JsonSchemaDefaultVisitor;
use PhpExtended\JsonSchema\JsonSchemaFloat;
use PhpExtended\JsonSchema\JsonSchemaInteger;
use PhpExtended\JsonSchema\JsonSchemaObject;
use PhpExtended\JsonSchema\JsonSchemaResolver;
use PhpExtended\JsonSchema\JsonSchemaString;
use PhpExtended\JsonSchema\NamespaceHolder;
use PhpExtended\JsonSchema\NamespaceHolderInterface;
use PHPUnit\Framework\TestCase;

/**
 * JsonSchemaDefaultVisitorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\JsonSchemaDefaultVisitor
 * @internal
 * @small
 */
class JsonSchemaDefaultVisitorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var JsonSchemaDefaultVisitor
	 */
	protected JsonSchemaDefaultVisitor $_object;
	
	/**
	 * The namespace.
	 *
	 * @var NamespaceHolderInterface
	 */
	protected NamespaceHolderInterface $_namespace;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testVisitSchemaRawBoolean() : void
	{
		$schema = new JsonSchemaBoolean();
		
		$this->assertEquals('false', $this->_object->handle('key', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaRawNonBoolean() : void
	{
		$this->expectException(LogicException::class);
		
		$this->_object->handle('key', new JsonSchema(), $this->_namespace);
	}
	
	public function testVisitSchemaArray() : void
	{
		$schema = new JsonSchemaArray();
		$schema->setType('array');
		$items = new JsonSchemaInteger();
		$items->setType('integer');
		$schema->setItems($items);
		
		$this->assertEquals('[12]', $this->_object->handle('key', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaArrayNotAList() : void
	{
		$this->expectException(LogicException::class);
		
		$this->_object->handle('key', new JsonSchemaArray(), $this->_namespace);
	}
	
	public function testVisitSchemaArrayOutOfOrder() : void
	{
		$this->expectException(LogicException::class);
		
		$schema = new JsonSchemaArray();
		$schema->setItems(new JsonSchema());
		
		$this->_object->visitSchemaArray($schema);
	}
	
	public function testVisitSchemaFloat() : void
	{
		$this->assertEquals('14.1', $this->_object->handle('key', new JsonSchemaFloat(), $this->_namespace));
	}
	
	public function testVisitSchemaInteger() : void
	{
		$this->assertEquals('12', $this->_object->handle('key', new JsonSchemaInteger(), $this->_namespace));
	}
	
	public function testVisitSchemaObject() : void
	{
		$this->assertEquals('$this->getMockBuilder(ClassName::class)->disableOriginalConstructor()->getMock()', $this->_object->handle('className', new JsonSchemaObject(), $this->_namespace));
	}
	
	public function testVisitSchemaMap() : void
	{
		$schema = new JsonSchemaObject();
		$schema->setAdditionalProperties(new JsonSchemaString());
		
		$this->assertEquals('[\'key\' => \'azertyuiop\']', $this->_object->handle('ClassName', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaObjectOutOfOrder() : void
	{
		$this->expectException(LogicException::class);
		
		$this->_object->visitSchemaObject(new JsonSchemaObject());
	}
	
	public function testVisitSchemaString() : void
	{
		$this->assertEquals("'azertyuiop'", $this->_object->handle('key', new JsonSchemaString(), $this->_namespace));
	}
	
	public function testVisitSchemaAcceptLanguage() : void
	{
		$schema = new JsonSchemaString();
		$schema->setFormat('accept-language-chain');
		
		$this->assertEquals('(new AcceptLanguageChainParser())->parse(\'en-US\')', $this->_object->handle('key', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaDate() : void
	{
		$schema = new JsonSchemaString();
		$schema->setFormat('date');
		
		$this->assertEquals('DateTimeImmutable::createFromFormat(\'!Y-m-d\', \'2000-01-01\')', $this->_object->handle('key', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaDateTime() : void
	{
		$schema = new JsonSchemaString();
		$schema->setFormat('date-time');
		
		$this->assertEquals('DateTimeImmutable::createFromFormat(\'!Y-m-d H:i:s\', \'2000-01-01 00:00:01\')', $this->_object->handle('key', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaEmail() : void
	{
		$schema = new JsonSchemaString();
		$schema->setFormat('email');
		
		$this->assertEquals('(new EmailAddressParser())->parse(\'test@example.com\')', $this->_object->handle('key', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaMailboxList() : void
	{
		$schema = new JsonSchemaString();
		$schema->setFormat('mailbox-list');
		
		$this->assertEquals('(new MailboxListParser())->parse(\'Test <test@example.com>\')', $this->_object->handle('key', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaTime() : void
	{
		$schema = new JsonSchemaString();
		$schema->setFormat('time');
		
		$this->assertEquals('DateTimeImmutable::createFromFormat(\'!H:i:s\', \'00:00:01\')', $this->_object->handle('key', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaUri() : void
	{
		$schema = new JsonSchemaString();
		$schema->setFormat('uri');
		
		$this->assertEquals('(new UriParser())->parse(\'https://test.example.com\')', $this->_object->handle('key', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaUuid() : void
	{
		$schema = new JsonSchemaString();
		$schema->setFormat('uuid');
		
		$this->assertEquals('(new UuidParser())->parse(\'9e2cfea1-afc9-4ecb-8a90-d477a5a90cfa\')', $this->_object->handle('key', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaVersion() : void
	{
		$schema = new JsonSchemaString();
		$schema->setFormat('version');
		
		$this->assertEquals('(new VersionParser())->parse(\'1.0.0\')', $this->_object->handle('key', $schema, $this->_namespace));
	}
	
	public function testVisitSchemaStringUnavailable() : void
	{
		$this->expectException(LogicException::class);
		
		$schema = new JsonSchemaString();
		$schema->setFormat('unavailable-format');
		
		$this->_object->handle('key', $schema, $this->_namespace);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_namespace = new NamespaceHolder('Interface\\Namespace', 'Class\\Namespace', 'Tests\\Namespace');
		$resolverMock = $this->createMock(JsonSchemaResolver::class);
		$resolverMock->expects($this->any())->method('resolveNamespace')->willReturn($this->_namespace);
		$resolverMock->expects($this->any())->method('resolveSchema')->willReturnArgument(0);
		$this->_object = new JsonSchemaDefaultVisitor($resolverMock);
	}
	
}
