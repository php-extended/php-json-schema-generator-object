<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\File\FileSystem;
use PhpExtended\HttpClient\ClientFactory;
use PhpExtended\HttpMessage\Uri;
use PhpExtended\JsonSchema\JsonSchema;
use PhpExtended\JsonSchema\JsonSchemaProvider;
use PhpExtended\JsonSchema\JsonSchemaRepository;
use PhpExtended\Logger\BasicConsoleLogger;
use PhpExtended\SimpleCache\SimpleCacheFilesystem;
use PhpExtended\SimpleCache\SimpleCacheLogger;
use PhpExtended\Uri\UriParser;
use PHPUnit\Framework\TestCase;
use Psr\SimpleCache\CacheInterface;

/**
 * JsonSchemaRepositoryTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\JsonSchemaRepository
 * @internal
 * @small
 */
class JsonSchemaRepositoryTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var JsonSchemaRepository
	 */
	protected JsonSchemaRepository $_object;
	
	/**
	 * The cache.
	 *
	 * @var CacheInterface
	 */
	protected CacheInterface $_cache;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	public function testItWorks() : void
	{
		$uriParser = new UriParser();
		$id = $uriParser->parse('https://gitlab.com/php-extended/php-api-fr-insee-catjur-interface/json-schema.json');
		
		$this->assertInstanceOf(JsonSchema::class, $this->_object->findJsonSchema($id));
		$this->assertInstanceOf(JsonSchema::class, $this->_object->findJsonSchema($id)); // yes, twice to trigger local cache
	}
	
	public function testInexistantRepository() : void
	{
		$this->expectException(RuntimeException::class);
		
		$uriParser = new UriParser();
		$base = $uriParser->parse('https://gitlab.com/test-inexistant-user/test-inexistant-repository/json-schema.json');
		
		$this->_object->findJsonSchema($base);
	}
	
	public function testInexistantNonGitlabRepository() : void
	{
		$this->expectException(RuntimeException::class);
		
		$uriParser = new UriParser();
		$base = $uriParser->parse('https://example.com/test-inexistant-user/test-inexistant-repository/json-shema.json');
		
		$this->_object->findJsonSchema($base);
	}
	
	public function testFailedToDecodeJson() : void
	{
		$this->expectException(RuntimeException::class);
		
		$uriParser = new UriParser();
		$base = $uriParser->parse('https://example.com');
		
		$this->_object->findJsonSchema($base);
	}
	
	public function testSave() : void
	{
		$this->assertTrue($this->_object->save(new Uri(), new JsonSchema()));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$logger = new BasicConsoleLogger(/* 2 */);
		
		$filesystem = new FileSystem(__DIR__.'/../cache');
		$this->_cache = new SimpleCacheLogger(new SimpleCacheFilesystem($filesystem), $logger);
		$this->_cache->clear();
		if(!\is_file(__DIR__.'/../cache/.gitignore'))
		{
			\file_put_contents(__DIR__.'/../cache/.gitignore', "*\n!.gitignore\n");
		}
		
		$clientFactory = new ClientFactory();
		$clientFactory->setLogger($logger);
		$jsonSchemaProvider = new JsonSchemaProvider();
		
		$this->_object = new JsonSchemaRepository($logger, $this->_cache, $clientFactory->createClient(), $jsonSchemaProvider);
	}
	
}
