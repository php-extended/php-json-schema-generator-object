<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\JsonSchema\Php74FieldMetadata;
use PhpExtended\JsonSchema\Php74MethodMetadata;
use PhpExtended\JsonSchema\Php74ParameterMetadata;
use PhpExtended\JsonSchema\PhpSingleType;
use PHPUnit\Framework\TestCase;

/**
 * Php74MethodMetadataTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\Php74MethodMetadata
 * @internal
 * @small
 */
class Php74MethodMetadataTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Php74MethodMetadata
	 */
	protected Php74MethodMetadata $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testIsSetter() : void
	{
		$this->assertFalse($this->_object->isSetter());
	}
	
	public function testIsGetter() : void
	{
		$this->assertFalse($this->_object->isGetter());
	}
	
	public function testGetTitle() : void
	{
		$this->assertEquals('title', $this->_object->getTitle());
	}
	
	public function testGetDescription() : void
	{
		$this->assertEquals('description', $this->_object->getDescription());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('name', $this->_object->getName());
	}
	
	public function testGetParameters() : void
	{
		$this->assertEmpty($this->_object->getParameters());
		$parameter = new Php74ParameterMetadata('name', 'title', new PhpSingleType('int', 'int'));
		$this->_object->addParameter($parameter);
		$this->assertEquals([$parameter], $this->_object->getParameters());
	}
	
	public function testGetReturnDocType() : void
	{
		$this->assertEquals('int', $this->_object->getReturnDocType());
	}
	
	public function testGetReturnPhpType() : void
	{
		$this->assertEquals('int', $this->_object->getReturnPhpType());
	}
	
	public function testIsReturnNullable() : void
	{
		$this->assertFalse($this->_object->isReturnNullable());
	}
	
	public function testIsReturnArray() : void
	{
		$this->assertFalse($this->_object->isReturnArray());
	}
	
	public function testEmptyRelatedField() : void
	{
		$this->expectException(LogicException::class);
		
		$this->_object->getRelatedField();
	}
	
	public function testGetRelatedField() : void
	{
		$relatedField = new Php74FieldMetadata(true, 'title', 'desc', 'name', new PhpSingleType('int', 'int'));
		$this->_object->setRelatedField($relatedField);
		$this->assertEquals($relatedField, $this->_object->getRelatedField());
	}
	
	public function testGetLineCount() : void
	{
		$this->assertEquals(12, $this->_object->getLineCount());
	}
	
	public function testCollectNamespacedClasses() : void
	{
		$this->_object->addParameter(new Php74ParameterMetadata('name', 'title', new PhpSingleType('int', 'int')));
		
		$this->assertEquals([], $this->_object->collectNamespacedClasses());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Php74MethodMetadata('title', 'description', 'name', new PhpSingleType('int', 'int'));
	}
	
}
