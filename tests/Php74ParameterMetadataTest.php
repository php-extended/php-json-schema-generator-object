<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\JsonSchema\Php74ParameterMetadata;
use PhpExtended\JsonSchema\PhpSingleType;
use PHPUnit\Framework\TestCase;

/**
 * Php74ParameterMetadataTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\Php74ParameterMetadata
 * @internal
 * @small
 */
class Php74ParameterMetadataTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Php74ParameterMetadata
	 */
	protected Php74ParameterMetadata $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('name', $this->_object->getName());
	}
	
	public function testGetTitle() : void
	{
		$this->assertEquals('title', $this->_object->getTitle());
	}
	
	public function testIsNullable() : void
	{
		$this->assertFalse($this->_object->isNullable());
	}
	
	public function testIsArray() : void
	{
		$this->assertFalse($this->_object->isArray());
	}
	
	public function testGetDocType() : void
	{
		$this->assertEquals('int', $this->_object->getDocType());
	}
	
	public function testGetPhpType() : void
	{
		$this->assertEquals('int', $this->_object->getPhpType());
	}
	
	public function testCollectNamespacedClasses() : void
	{
		$this->assertEquals([], $this->_object->collectNamespacedClasses());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Php74ParameterMetadata('name', 'title', new PhpSingleType('int', 'int'));
	}
	
}
