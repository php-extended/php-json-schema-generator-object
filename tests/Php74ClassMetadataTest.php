<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\JsonSchema\NamespaceHolder;
use PhpExtended\JsonSchema\Php74ClassMetadata;
use PhpExtended\JsonSchema\Php74FieldMetadata;
use PhpExtended\JsonSchema\Php74MethodMetadata;
use PhpExtended\JsonSchema\PhpSingleType;
use PHPUnit\Framework\TestCase;

/**
 * Php74ClassMetadataTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\Php74ClassMetadata
 * @internal
 * @small
 */
class Php74ClassMetadataTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Php74ClassMetadata
	 */
	protected Php74ClassMetadata $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetComposerPackageName() : void
	{
		$this->assertEquals('composerPackageName', $this->_object->getComposerPackageName());
	}
	
	public function testGetNamespace() : void
	{
		$this->assertEquals('Classes\\Namespace', $this->_object->getNamespace());
	}
	
	public function testGetUses() : void
	{
		$this->assertEquals([], $this->_object->getUses());
		$this->_object->addUses(['Stringable']);
		$this->assertEquals(['Stringable'], $this->_object->getUses());
	}
	
	public function testDoesNotAcceptUse() : void
	{
		$this->assertEquals([], $this->_object->getUses());
		$this->_object->addUse('Classes\\Namespace\\OtherClass');
		$this->assertEquals([], $this->_object->getUses());
	}
	
	public function testGetClassName() : void
	{
		$this->assertEquals('className', $this->_object->getClassName());
	}
	
	public function testGetInterfaceName() : void
	{
		$this->assertEquals('interfaceName', $this->_object->getInterfaceName());
	}
	
	public function testGetTitle() : void
	{
		$this->assertEquals('title', $this->_object->getTitle());
	}
	
	public function testGetDescription() : void
	{
		$this->assertEquals('description', $this->_object->getDescription());
	}
	
	public function testGetField() : void
	{
		$this->assertEquals([], $this->_object->getFields());
		$field = new Php74FieldMetadata(false, 'title', 'description', 'name', new PhpSingleType('Interfaces\\Namespace\\ClassName', 'Interfaces\\Namespace\\ClassName'));
		$this->_object->addField($field);
		$this->assertEquals([$field], $this->_object->getFields());
		$this->assertEquals(['Interfaces\\Namespace\\ClassName'], $this->_object->getUses());
	}
	
	public function testGetSortedFields() : void
	{
		$field1 = new Php74FieldMetadata(true, 'title', 'desc', 'name1', new PhpSingleType('int', 'int'));
		$this->_object->addField($field1);
		$field2 = new Php74FieldMetadata(false, 'title', 'desc', 'name2', new PhpSingleType('int', 'int'));
		$this->_object->addField($field2);
		$field3 = new Php74FieldMetadata(false, 'title', 'desc', 'aaaa', new PhpSingleType('int', 'int'));
		$this->_object->addField($field3);
		$field4 = new Php74FieldMetadata(true, 'title', 'desc', 'aa', new PhpSingleType('int', 'int'));
		$this->_object->addField($field4);
		$this->assertEquals([$field1, $field4, $field2, $field3], $this->_object->getFields());
	}
	
	public function testGetNonPublicFields() : void
	{
		$field = new Php74FieldMetadata(false, 'title', 'description', 'n', new PhpSingleType('int', 'int'));
		$this->_object->addField($field);
		
		$this->assertEquals([$field], $this->_object->getNonPublicFields());
	}
	
	public function testNeedsConstructor() : void
	{
		$this->assertFalse($this->_object->needsConstructor());
		$this->_object->addField(new Php74FieldMetadata(false, 'title', 'description', 'name', new PhpSingleType('int', 'int')));
		$this->assertTrue($this->_object->needsConstructor());
	}
	
	public function testGetHasNotNonPublicFields() : void
	{
		$this->_object->addField(new Php74FieldMetadata(true, 'title', 'description', 'name', new PhpSingleType('int', 'int')));
		$this->assertFalse($this->_object->hasNonPublicFields());
	}
	
	public function testGetHasNonPublicFields() : void
	{
		$this->_object->addField(new Php74FieldMetadata(false, 'title', 'description', 'name', new PhpSingleType('int', 'int')));
		$this->assertTrue($this->_object->hasNonPublicFields());
	}
	
	public function testGetMethods() : void
	{
		$this->assertEquals([], $this->_object->getMethods());
		$method = new Php74MethodMetadata('title', 'description', 'name', new PhpSingleType('Interfaces\\Namespace\\ReturnType', 'Interfaces\\Namespace\\ReturnType'));
		$this->_object->addMethod($method);
		$this->assertEquals([$method], $this->_object->getMethods());
		$this->assertEquals(['Interfaces\\Namespace\\ReturnType'], $this->_object->getUses());
	}
	
	public function testRender() : void
	{
		$this->assertNotEmpty($this->_object->render());
	}
	
	public function testGetPublicCount() : void
	{
		$this->_object->addField(new Php74FieldMetadata(true, 'title', 'description', 'name', new PhpSingleType('int', 'int')));
		
		$this->assertEquals(2, $this->_object->getPublicCount());
	}
	
	public function testHasShortFieldNames() : void
	{
		$this->assertFalse($this->_object->hasShortFieldNames());
		
		$this->_object->addField(new Php74FieldMetadata(false, 'title', 'description', 'n', new PhpSingleType('int', 'int')));
		
		$this->assertTrue($this->_object->hasShortFieldNames());
	}
	
	public function testHasLongFieldNames() : void
	{
		$this->assertFalse($this->_object->hasLongFieldNames());
		
		$this->_object->addField(new Php74FieldMetadata(false, 'title', 'description', 'azertyuiopqsdfghjklmwxcvbnazertyuiopqsdfghjklmwxcvbn', new PhpSingleType('int', 'int')));
		
		$this->assertTrue($this->_object->hasLongFieldNames());
	}
	
	public function testGetLineCount() : void
	{
		$this->_object->addField(new Php74FieldMetadata(false, 'title', 'description', 'name', new PhpSingleType('int', 'int')));
		$this->_object->addMethod(new Php74MethodMetadata('title', 'description', 'name', new PhpSingleType('int', 'int')));
		$this->assertEquals(44, $this->_object->getLineCount());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$namespaces = new NamespaceHolder('Interfaces\\Namespace', 'Classes\\Namespace', 'Tests\\Namespace');
		$this->_object = new Php74ClassMetadata($namespaces, 'composerPackageName', 'className', 'interfaceName', 'title', 'description');
	}
	
}
