<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\JsonSchema\PhpSingleType;
use PHPUnit\Framework\TestCase;

/**
 * PhpSingleTypeTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\PhpSingleType
 * @internal
 * @small
 */
class PhpSingleTypeTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var PhpSingleType
	 */
	protected PhpSingleType $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('Namespace\\From\\Object<T>', $this->_object->__toString());
	}
	
	public function testGetDocType() : void
	{
		$this->assertEquals('Object<T>', $this->_object->getDocType());
	}
	
	public function testGetNamespace() : void
	{
		$this->assertEquals('Namespace\\From', $this->_object->getNamespace());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('Object', $this->_object->getName());
	}
	
	public function testGetNullable() : void
	{
		$this->assertTrue($this->_object->isNullable());
	}
	
	public function testCollectNamespacedClasses() : void
	{
		$this->assertEquals(['Namespace\\From\\Object'], $this->_object->collectNamespacedClasses());
	}
	
	public function testCollectNoNamespaceOnPrimitive() : void
	{
		$this->assertEquals([], (new PhpSingleType('int', 'int'))->collectNamespacedClasses());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new PhpSingleType('Object<T>', '\\Namespace\\From\\Object', true);
	}
	
}
