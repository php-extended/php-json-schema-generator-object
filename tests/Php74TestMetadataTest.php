<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\JsonSchema\NamespaceHolder;
use PhpExtended\JsonSchema\Php74CompoundMetadata;
use PhpExtended\JsonSchema\Php74FieldMetadata;
use PhpExtended\JsonSchema\Php74MethodMetadata;
use PhpExtended\JsonSchema\Php74TestMetadata;
use PhpExtended\JsonSchema\PhpSingleType;
use PHPUnit\Framework\TestCase;

/**
 * Php74TestMetadataTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\Php74TestMetadata
 * @internal
 * @small
 */
class Php74TestMetadataTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Php74TestMetadata
	 */
	protected Php74TestMetadata $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetComposerPackageName() : void
	{
		$this->assertEquals('composerPackageName', $this->_object->getComposerPackageName());
	}
	
	public function testGetUse() : void
	{
		$this->assertEquals(['PHPUnit\\Framework\\TestCase'], $this->_object->getUses());
		$this->_object->addUses(['Stringable']);
		$this->assertEquals(['PHPUnit\\Framework\\TestCase', 'Stringable'], $this->_object->getUses());
	}
	
	public function testDoesNotAcceptUse() : void
	{
		$this->assertEquals(['PHPUnit\\Framework\\TestCase'], $this->_object->getUses());
		$this->_object->addUse('Tests\\Namespace\\OtherTestClass');
		$this->assertEquals(['PHPUnit\\Framework\\TestCase'], $this->_object->getUses());
	}
	
	public function testGetTestNamespace() : void
	{
		$this->assertEquals('Tests\\Namespace', $this->_object->getTestNamespace());
	}
	
	public function testGetTestClassName() : void
	{
		$this->assertEquals('testClassName', $this->_object->getTestClassName());
	}
	
	public function testGetClassNamespace() : void
	{
		$this->assertEquals('Classes\\Namespace', $this->_object->getClassNamespace());
	}
	
	public function testGetClassName() : void
	{
		$this->assertEquals('className', $this->_object->getClassName());
	}
	
	public function testGetTitle() : void
	{
		$this->assertEquals('title', $this->_object->getTitle());
	}
	
	public function testGetDescription() : void
	{
		$this->assertEquals('description', $this->_object->getDescription());
	}
	
	public function testGetCompounds() : void
	{
		$this->assertEquals([], $this->_object->getCompounds());
		$compound = new Php74CompoundMetadata(
			new NamespaceHolder('Interfaces\\Namespace', 'Classes\\Namespace', 'Tests\\Namespace'),
			new Php74FieldMetadata(true, 'title', 'description', 'name', new PhpSingleType('int', 'int')),
			new Php74MethodMetadata('title', 'description', 'name', new PhpSingleType('int', 'int')),
			new Php74MethodMetadata('title', 'description', 'name', new PhpSingleType('int', 'int')),
			'defaultValue',
			'calculatedValue',
			['Additional\\Compound\\UsedClass'],
		);
		$this->_object->addCompound($compound);
		$this->assertEquals([$compound], $this->_object->getCompounds());
		$this->assertEquals(['PHPUnit\\Framework\\TestCase', 'Additional\\Compound\\UsedClass'], $this->_object->getUses());
	}
	
	public function testRender() : void
	{
		$this->assertNotEmpty($this->_object->render());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$namespaces = new NamespaceHolder('Interfaces\\Namespace', 'Classes\\Namespace', 'Tests\\Namespace');
		$this->_object = new Php74TestMetadata($namespaces, 'composerPackageName', 'testClassName', 'className', 'title', 'description');
	}
	
}
