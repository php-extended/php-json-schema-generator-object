<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\JsonSchema\JsonSchema;
use PhpExtended\JsonSchema\JsonSchemaAbstractVisitor;
use PhpExtended\JsonSchema\JsonSchemaResolver;
use PhpExtended\JsonSchema\NamespaceHolder;
use PHPUnit\Framework\TestCase;

/**
 * JsonSchemaAbstractVisitorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\JsonSchemaAbstractVisitor
 * @internal
 * @small
 */
class JsonSchemaAbstractVisitorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var JsonSchemaAbstractVisitor
	 */
	protected JsonSchemaAbstractVisitor $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testVisitRaw() : void
	{
		$this->assertNull($this->_object->handle('key', new JsonSchema(), new NamespaceHolder('I', 'C', 'T')));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = $this->getMockForAbstractClass(JsonSchemaAbstractVisitor::class, [$this->createMock(JsonSchemaResolver::class)]);
	}
	
}
