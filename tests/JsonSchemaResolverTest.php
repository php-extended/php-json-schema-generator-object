<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Domain\Domain;
use PhpExtended\HttpMessage\Uri;
use PhpExtended\JsonSchema\JsonSchema;
use PhpExtended\JsonSchema\JsonSchemaRepository;
use PhpExtended\JsonSchema\JsonSchemaResolver;
use PhpExtended\JsonSchema\NamespaceHolder;
use PhpExtended\JsonSchema\NamespaceRepository;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Log\NullLogger;
use Psr\SimpleCache\CacheInterface;

/**
 * JsonSchemaResolverTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\JsonSchemaResolver
 * @internal
 * @small
 */
class JsonSchemaResolverTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var JsonSchemaResolver
	 */
	protected JsonSchemaResolver $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testSetDefaults() : void
	{
		$schema = new JsonSchema();
		$schema->setId(new Uri());
		$namespace = new NamespaceHolder('I', 'C', 'T');
		$this->_object->setDefaults($schema, $namespace);
		$this->assertEquals($schema, $this->_object->getDefaultSchema());
		$this->assertEquals($namespace, $this->_object->getDefaultNamespace());
	}
	
	public function testSetDefaultsFailedToFindId() : void
	{
		$this->expectException(LogicException::class);
		
		$this->_object->setDefaults(new JsonSchema(), new NamespaceHolder('I', 'C', 'T'));
	}
	
	public function testGetDefaultSchemaFailed() : void
	{
		$this->expectException(LogicException::class);
		
		$this->_object->getDefaultSchema();
	}
	
	public function testGetDefaultNamespaceFailed() : void
	{
		$this->expectException(LogicException::class);
		
		$this->_object->getDefaultNamespace();
	}
	
	public function testResolveSchemaNoRef() : void
	{
		$schema = new JsonSchema();
		$schema->setId(new Uri());
		$namespace = new NamespaceHolder('I', 'C', 'T');
		
		$this->_object->setDefaults($schema, $namespace);
		$this->assertEquals($schema, $this->_object->resolveSchema($schema));
	}
	
	public function testResolveSchemaNoHost() : void
	{
		$schema = new JsonSchema();
		$schema->setId(new Uri());
		$schema->setRef(new Uri());
		$namespace = new NamespaceHolder('I', 'C', 'T');
		
		$this->_object->setDefaults($schema, $namespace);
		$this->assertEquals($schema, $this->_object->resolveSchema($schema));
	}
	
	public function testResolveSchemaInItself() : void
	{
		$schema = new JsonSchema();
		$schema->setId(new Uri());
		$namespace = new NamespaceHolder('I', 'C', 'T');
		$source = new JsonSchema();
		$source->setRef(new Uri('https', null, null, null, null, null, '#/$defs/target'));
		$target = new JsonSchema();
		$target->setId(new Uri('https', null, new Domain(['example', 'com'])));
		$schema->setDefs([
			'source' => $source,
			'target' => $target,
		]);
		
		$expected = $target->mergeWith($source);
		
		$this->_object->setDefaults($schema, $namespace);
		$this->assertEquals($expected, $this->_object->resolveSchema($source));
	}
	
	public function testResolveNamespaceNoRef() : void
	{
		$schema = new JsonSchema();
		$schema->setId(new Uri());
		$expected = new NamespaceHolder('I', 'C', 'T');
		$defaultSchema = new JsonSchema();
		$defaultSchema->setId(new Uri());
		$this->_object->setDefaults($defaultSchema, $expected);
		$this->assertEquals($expected, $this->_object->resolveNamespace($schema));
	}
	
	public function testResolveNamespaceNoHost() : void
	{
		$schema = new JsonSchema();
		$schema->setId(new Uri());
		$schema->setRef(new Uri());
		$expected = new NamespaceHolder('I', 'C', 'T');
		$defaultSchema = new JsonSchema();
		$defaultSchema->setId(new Uri());
		$this->_object->setDefaults($defaultSchema, $expected);
		$this->assertEquals($expected, $this->_object->resolveNamespace($schema));
	}
	
	public function testResolveNamespace() : void
	{
		$schema = new JsonSchema();
		$schema->setId(new Uri());
		$schema->setRef(new Uri('https', null, new Domain(['example', 'com'])));
		$expected = new NamespaceHolder('I', 'C', 'T');
		$defaultSchema = new JsonSchema();
		$defaultSchema->setId(new Uri('https', null, new Domain(['example', 'com'])));
		$this->_object->setDefaults($defaultSchema, $expected);
		$this->assertEquals($expected, $this->_object->resolveNamespace($schema));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$nsr = new NamespaceRepository(new NullLogger(), $this->createMock(CacheInterface::class), $this->createMock(ClientInterface::class));
		$this->_object = new JsonSchemaResolver($this->createMock(JsonSchemaRepository::class), $nsr);
	}
	
}
