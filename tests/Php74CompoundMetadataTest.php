<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\JsonSchema\NamespaceHolder;
use PhpExtended\JsonSchema\Php74CompoundMetadata;
use PhpExtended\JsonSchema\Php74FieldMetadata;
use PhpExtended\JsonSchema\Php74MethodMetadata;
use PhpExtended\JsonSchema\PhpSingleType;
use PHPUnit\Framework\TestCase;

/**
 * Php74CompoundMetadataTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\Php74CompoundMetadata
 * @internal
 * @small
 */
class Php74CompoundMetadataTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Php74CompoundMetadata
	 */
	protected Php74CompoundMetadata $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testIsPublic() : void
	{
		$this->assertTrue($this->_object->isPublic());
	}
	
	public function testIsNullable() : void
	{
		$this->assertFalse($this->_object->isNullable());
	}
	
	public function testIsArray() : void
	{
		$this->assertFalse($this->_object->isArray());
	}
	
	public function testGetFieldName() : void
	{
		$this->assertEquals('name', $this->_object->getFieldName());
	}
	
	public function testGetGetterName() : void
	{
		$this->assertEquals('getName', $this->_object->getGetterName());
	}
	
	public function testGetSetterName() : void
	{
		$this->assertEquals('setName', $this->_object->getSetterName());
	}
	
	public function testGetDefaultValue() : void
	{
		$this->assertEquals('defaultValue', $this->_object->getDefaultValue());
	}
	
	public function testGetCalculatedValue() : void
	{
		$this->assertEquals('calculatedValue', $this->_object->getCalculatedValue());
	}
	
	public function testGetUses() : void
	{
		$this->assertEquals([], $this->_object->getUses());
		$this->_object->addUses(['\\Stringable']);
		$this->assertEquals(['Stringable'], $this->_object->getUses());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Php74CompoundMetadata(
			new NamespaceHolder('I', 'C', 'T'),
			new Php74FieldMetadata(true, 'title', 'description', 'name', new PhpSingleType('int', 'int')),
			new Php74MethodMetadata('title', 'description', 'getName', new PhpSingleType('float', 'float')),
			new Php74MethodMetadata('title', 'description', 'setName', new PhpSingleType('bool', 'bool')),
			'defaultValue',
			'calculatedValue',
		);
	}
	
}
