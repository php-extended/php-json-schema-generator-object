<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\JsonSchema\NamespaceHolder;
use PhpExtended\JsonSchema\NamespaceHolderInterface;
use PhpExtended\JsonSchema\PhpNamingTrait;
use PHPUnit\Framework\TestCase;

/**
 * PhpNamingTraitTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\PhpNamingTrait
 * @internal
 * @small
 */
class PhpNamingTraitTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var PhpNamingTrait
	 */
	protected $_object;
	
	/**
	 * The namespace holder.
	 * 
	 * @var NamespaceHolderInterface
	 */
	protected NamespaceHolderInterface $_namespaceHolder;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetParameterNameFromKey() : void
	{
		$this->assertEquals('fieldName', $this->_object->getParameterNameFromKey('field-name'));
	}
	
	public function testGetClassNameForKey() : void
	{
		$this->assertEquals('ClassName', $this->_object->getClassNameFromKey('class-name'));
	}
	
	public function testGetNamespacedClassNameForKey() : void
	{
		$this->assertEquals('Classes\\Namespace\\ClassName', $this->_object->getNamespacedClassNameFromKey($this->_namespaceHolder, 'class-name'));
	}
	
	public function testGetInterfaceNameForKey() : void
	{
		$this->assertEquals('ClassNameInterface', $this->_object->getInterfaceNameFromKey('class-name'));
	}
	
	public function testGetNamespacedInterfaceNameForKey() : void
	{
		$this->assertEquals('Interfaces\\Namespace\\ClassNameInterface', $this->_object->getNamespacedInterfaceNameFromKey($this->_namespaceHolder, 'class-name'));
	}
	
	public function testGetTestNameForKey() : void
	{
		$this->assertEquals('ClassNameTest', $this->_object->getTestNameFromKey('class-name'));
	}
	
	public function testGetNamespacedTestNameForKey() : void
	{
		$this->assertEquals('Tests\\Namespace\\ClassNameTest', $this->_object->getNamespacedTestNameFromKey($this->_namespaceHolder, 'class-name'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_namespaceHolder = new NamespaceHolder('Interfaces\\Namespace', 'Classes\\Namespace', 'Tests\\Namespace');
		$this->_object = new class()
		{
			use PhpNamingTrait;
			
			public function __toString() : string
			{
				return static::class.'@'.\spl_object_hash($this);
			}
			
		};
	}
	
}
