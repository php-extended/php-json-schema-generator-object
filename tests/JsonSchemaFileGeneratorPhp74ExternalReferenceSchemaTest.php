<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\JsonSchema\JsonSchemaFileGeneratorPhp74;
use PhpExtended\JsonSchema\JsonSchemaProvider;
use PhpExtended\JsonSchema\NamespaceHolder;
use PHPUnit\Framework\TestCase;

/**
 * JsonSchemaFileGeneratorPhp74ExternalReferenceSchemaTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\JsonSchema\JsonSchemaFileGeneratorPhp74
 * @internal
 * @small
 * @internal
 */
class JsonSchemaFileGeneratorPhp74ExternalReferenceSchemaTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var JsonSchemaFileGeneratorPhp74
	 */
	protected JsonSchemaFileGeneratorPhp74 $_object;
	
	public function testFailedToGetReference() : void
	{
		$this->expectException(RuntimeException::class);
		
		$provider = new JsonSchemaProvider();
		$schema = $provider->provideFromString('{
	"$id" : "https://example.com/example",
	"type": "null",
	"$defs": {
		"ClassName": {
			"type": "object",
			"description": "This is a description",
			"properties": {
				"field_object": {
					"$ref": "https://example.com/prerequisites#/$defs/Reference",
					"description": "The description of the field"
				}
			}
		}
	}
}');
		$this->_object->generate($schema);
	}
	
	public function testFailedToGetTarget() : void
	{
		$this->expectException(RuntimeException::class);
		
		$provider = new JsonSchemaProvider();
		$schema1 = $provider->provideFromString('{
	"$id" : "https://example.com/prerequisites",
	"type": "null",
	"$defs": {}
}');
		
		$schema = $provider->provideFromString('{
	"$id" : "https://example.com/example",
	"type": "null",
	"$defs": {
		"ClassName": {
			"type": "object",
			"description": "This is a description",
			"properties": {
				"field_object": {
					"$ref": "https://example.com/prerequisites#/$defs/Reference",
					"description": "The description of the field"
				}
			}
		}
	}
}');
		
		$this->_object->generate($schema1);
		$this->_object->generate($schema);
	}
	
	public function testGenerateReference() : void
	{
		$provider = new JsonSchemaProvider();
		$schema1 = $provider->provideFromString('{
	"$id" : "https://example.com/prerequisites",
	"type": "null",
	"$defs": {
		"Reference": {
			"type": "object",
			"description": "This is the reference",
			"properties": {
				"ref_id": {
					"type": "integer",
					"description": "The id of the reference"
				}
			}
		}
	}
}');
		
		$schema = $provider->provideFromString('{
	"$id" : "https://example.com/example",
	"type": "null",
	"$defs": {
		"ClassName": {
			"type": "object",
			"description": "This is a description",
			"properties": {
				"field_object": {
					"$ref": "https://example.com/prerequisites#/$defs/Reference",
					"description": "The description of the field"
				}
			}
		}
	}
}');
		
		$this->_object->generate($schema1); // register schema1 to repositories
		$collection = $this->_object->generate($schema);
		
		$interface = '<?php declare(strict_types=1);

/*
 * This file is part of the interfaces/package-name library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Interfaces\\Namespace;

use Stringable;

/**
 * ClassNameInterface interface file.
 * 
 * This is a description
 * 
 * /!\\ This file was generated automatically from the json-schema.json file.
 * /!\\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\\JsonSchema\\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ClassNameInterface extends Stringable
{
	
	/**
	 * Gets the description of the field
	 * 
	 * @return ?ReferenceInterface
	 */
	public function getFieldObject() : ?ReferenceInterface;
	
}
';
		
		$class = '<?php declare(strict_types=1);

/*
 * This file is part of the classes/package-name library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Classes\\Namespace;

use Interfaces\\Namespace\\ClassNameInterface;
use Interfaces\\Namespace\\ReferenceInterface;

/**
 * ClassName class file.
 * 
 * This is a simple implementation of the ClassNameInterface.
 * 
 * /!\\ This file was generated automatically from the json-schema.json file.
 * /!\\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\\JsonSchema\\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ClassName implements ClassNameInterface
{
	
	/**
	 * The description of the field
	 * 
	 * @var ?ReferenceInterface
	 */
	protected ?ReferenceInterface $_fieldObject = null;
	
	/**
	 * {@inheritDoc}
	 * @see \\Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.\'@\'.\\spl_object_hash($this);
	}
	
	/**
	 * Sets the description of the field
	 * 
	 * @param ?ReferenceInterface $fieldObject
	 * @return ClassNameInterface
	 */
	public function setFieldObject(?ReferenceInterface $fieldObject) : ClassNameInterface
	{
		$this->_fieldObject = $fieldObject;
		
		return $this;
	}
	
	/**
	 * Gets the description of the field
	 * 
	 * @return ?ReferenceInterface
	 */
	public function getFieldObject() : ?ReferenceInterface
	{
		return $this->_fieldObject;
	}
	
}
';
		
		$test = '<?php declare(strict_types=1);

/*
 * This file is part of the tests/package-name library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Tests\\Namespace;

use Classes\\Namespace\\ClassName;
use Classes\\Namespace\\Reference;
use PHPUnit\\Framework\\TestCase;

/**
 * ClassNameTest test file.
 * 
 * /!\\ This file was generated automatically from the json-schema.json file.
 * /!\\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\\JsonSchema\\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \\Classes\\Namespace\\ClassName
 * @internal
 * @small
 */
class ClassNameTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ClassName
	 */
	protected ClassName $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\\get_class($this->_object).\'@\'.\\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetFieldObject() : void
	{
		$this->assertNull($this->_object->getFieldObject());
		$expected = $this->getMockBuilder(Reference::class)->disableOriginalConstructor()->getMock();
		$this->_object->setFieldObject($expected);
		$this->assertEquals($expected, $this->_object->getFieldObject());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \\PHPUnit\\Framework\\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ClassName();
	}
	
}
';
		
		$this->assertCount(1, $collection->getInterfaceFiles());
		$this->assertEquals($interface, \array_values($collection->getInterfaceFiles())[0], 'interface');
		$this->assertCount(1, $collection->getClassFiles());
		$this->assertEquals($class, \array_values($collection->getClassFiles())[0], 'class');
		$this->assertCount(1, $collection->getTestFiles());
		$this->assertEquals($test, \array_values($collection->getTestFiles())[0], 'test');
	}
	
	public function testGenerateReferenceMandatory() : void
	{
		$provider = new JsonSchemaProvider();
		$schema1 = $provider->provideFromString('{
	"$id" : "https://example.com/prerequisites",
	"type": "null",
	"$defs": {
		"Reference": {
			"type": "object",
			"description": "This is the reference",
			"properties": {
				"ref_id": {
					"type": "integer",
					"description": "The id of the reference"
				}
			}
		}
	}
}');
		
		$schema = $provider->provideFromString('{
	"$id" : "https://example.com/example",
	"type": "null",
	"$defs": {
		"ClassName": {
			"type": "object",
			"description": "This is a description",
			"properties": {
				"field_object": {
					"$ref": "https://example.com/prerequisites#/$defs/Reference",
					"description": "The description of the field"
				}
			},
			"required" : ["field_object"]
		}
	}
}');
		
		$this->_object->generate($schema1); // register schema1 to repositories
		$collection = $this->_object->generate($schema);
		
		$interface = '<?php declare(strict_types=1);

/*
 * This file is part of the interfaces/package-name library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Interfaces\\Namespace;

use Stringable;

/**
 * ClassNameInterface interface file.
 * 
 * This is a description
 * 
 * /!\\ This file was generated automatically from the json-schema.json file.
 * /!\\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\\JsonSchema\\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ClassNameInterface extends Stringable
{
	
	/**
	 * Gets the description of the field
	 * 
	 * @return ReferenceInterface
	 */
	public function getFieldObject() : ReferenceInterface;
	
}
';
		
		$class = '<?php declare(strict_types=1);

/*
 * This file is part of the classes/package-name library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Classes\\Namespace;

use Interfaces\\Namespace\\ClassNameInterface;
use Interfaces\\Namespace\\ReferenceInterface;

/**
 * ClassName class file.
 * 
 * This is a simple implementation of the ClassNameInterface.
 * 
 * /!\\ This file was generated automatically from the json-schema.json file.
 * /!\\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\\JsonSchema\\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ClassName implements ClassNameInterface
{
	
	/**
	 * The description of the field
	 * 
	 * @var ReferenceInterface
	 */
	protected ReferenceInterface $_fieldObject;
	
	/**
	 * Constructor for ClassName with private members.
	 * 
	 * @param ReferenceInterface $fieldObject
	 */
	public function __construct(ReferenceInterface $fieldObject)
	{
		$this->setFieldObject($fieldObject);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \\Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.\'@\'.\\spl_object_hash($this);
	}
	
	/**
	 * Sets the description of the field
	 * 
	 * @param ReferenceInterface $fieldObject
	 * @return ClassNameInterface
	 */
	public function setFieldObject(ReferenceInterface $fieldObject) : ClassNameInterface
	{
		$this->_fieldObject = $fieldObject;
		
		return $this;
	}
	
	/**
	 * Gets the description of the field
	 * 
	 * @return ReferenceInterface
	 */
	public function getFieldObject() : ReferenceInterface
	{
		return $this->_fieldObject;
	}
	
}
';
		
		$test = '<?php declare(strict_types=1);

/*
 * This file is part of the tests/package-name library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace Tests\\Namespace;

use Classes\\Namespace\\ClassName;
use Classes\\Namespace\\Reference;
use PHPUnit\\Framework\\TestCase;

/**
 * ClassNameTest test file.
 * 
 * /!\\ This file was generated automatically from the json-schema.json file.
 * /!\\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\\JsonSchema\\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \\Classes\\Namespace\\ClassName
 * @internal
 * @small
 */
class ClassNameTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ClassName
	 */
	protected ClassName $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\\get_class($this->_object).\'@\'.\\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetFieldObject() : void
	{
		$this->assertEquals($this->getMockBuilder(Reference::class)->disableOriginalConstructor()->getMock(), $this->_object->getFieldObject());
		$expected = $this->getMockBuilder(Reference::class)->disableOriginalConstructor()->getMock();
		$this->_object->setFieldObject($expected);
		$this->assertEquals($expected, $this->_object->getFieldObject());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \\PHPUnit\\Framework\\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ClassName($this->getMockBuilder(Reference::class)->disableOriginalConstructor()->getMock());
	}
	
}
';
		
		$this->assertCount(1, $collection->getInterfaceFiles());
		$this->assertEquals($interface, \array_values($collection->getInterfaceFiles())[0], 'interface');
		$this->assertCount(1, $collection->getClassFiles());
		$this->assertEquals($class, \array_values($collection->getClassFiles())[0], 'class');
		$this->assertCount(1, $collection->getTestFiles());
		$this->assertEquals($test, \array_values($collection->getTestFiles())[0], 'test');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$namespaces = new NamespaceHolder('Interfaces\\Namespace', 'Classes\\Namespace', 'Tests\\Namespace');
		$this->_object = new JsonSchemaFileGeneratorPhp74(
			$namespaces,
			'interfaces/package-name',
			'classes/package-name',
			'tests/package-name',
		);
	}
	
}
