<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

use Stringable;

/**
 * Php74ParameterMetadata class file.
 * 
 * This represents all the metadata needed to render a parameters for a method
 * with php7.4+ compatibility.
 * 
 * @author Anastaszor
 */
class Php74ParameterMetadata implements Stringable
{
	
	/**
	 * The title of this parameter.
	 * 
	 * @var ?string
	 */
	protected ?string $_title;
	
	/**
	 * The name of this parameter.
	 *
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * The type of this parameter.
	 * 
	 * @var PhpAbstractType
	 */
	protected PhpAbstractType $_type;
	
	/**
	 * Builds a new parameter metadata with its data.
	 * 
	 * @param string $name
	 * @param ?string $title
	 * @param PhpAbstractType $type
	 */
	public function __construct(string $name, ?string $title, PhpAbstractType $type)
	{
		$this->_name = $name;
		$this->_title = $title;
		$this->_type = $type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the name of the parameter.
	 * 
	 * @return string
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * Gets the title of the parameter.
	 * 
	 * @return ?string
	 */
	public function getTitle() : ?string
	{
		return $this->_title;
	}
	
	/**
	 * Gets the doc type of the parameter.
	 * 
	 * @return string
	 */
	public function getDocType() : string
	{
		return $this->_type->getDocType();
	}
	
	/**
	 * Gets the php type of the parameter.
	 *
	 * @return string
	 */
	public function getPhpType() : string
	{
		return $this->_type->getName();
	}
	
	/**
	 * Collects all the namespaced classes for this parameter.
	 * 
	 * @return array<integer, class-string>
	 */
	public function collectNamespacedClasses() : array
	{
		return $this->_type->collectNamespacedClasses();
	}
	
	/**
	 * Gets whether this parameter is nullable.
	 * 
	 * @return boolean
	 */
	public function isNullable() : bool
	{
		return $this->_type->isNullable();
	}
	
	/**
	 * Gets whether this parameter is an array.
	 * 
	 * @return boolean
	 */
	public function isArray() : bool
	{
		return $this->_type->isArray();
	}
	
}
