<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

/**
 * PhpNamingTrait class file.
 * 
 * This trait is to transform key values into parameters, method, classes,
 * interfaces and test names.
 * 
 * @author Anastaszor
 */
trait PhpNamingTrait
{
	
	/**
	 * Gets a suitable parameter name from a given key.
	 *
	 * @param string $key
	 * @return string
	 */
	public function getParameterNameFromKey(string $key) : string
	{
		return \lcfirst($this->getClassNameFromKey($key));
	}
	
	/**
	 * Gets a suitable class name from a given key.
	 *
	 * @param string $key
	 * @return class-string
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function getClassNameFromKey(string $key) : string
	{
		/** @phpstan-ignore-next-line */
		return \str_replace(' ', '', \ucwords(\str_replace(['-', '_'], ' ', $key)));
	}
	
	/**
	 * Gets a suitable class name with namespace from a given key.
	 *
	 * @param NamespaceHolderInterface $namespaceHolder
	 * @param string $key
	 * @return class-string
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function getNamespacedClassNameFromKey(NamespaceHolderInterface $namespaceHolder, string $key) : string
	{
		/** @phpstan-ignore-next-line */
		return $namespaceHolder->getClassNamespace().'\\'.$this->getClassNameFromKey($key);
	}
	
	/**
	 * Gets a suitable interface name from a given key.
	 *
	 * @param string $key
	 * @return class-string
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function getInterfaceNameFromKey(string $key) : string
	{
		/** @phpstan-ignore-next-line */
		return $this->getClassNameFromKey($key).'Interface';
	}
	
	/**
	 * Gets a suitable interface name with namespace from a given key.
	 *
	 * @param NamespaceHolderInterface $namespaceHolder
	 * @param string $key
	 * @return class-string
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function getNamespacedInterfaceNameFromKey(NamespaceHolderInterface $namespaceHolder, string $key) : string
	{
		/** @phpstan-ignore-next-line */
		return $namespaceHolder->getInterfaceNamespace().'\\'.$this->getInterfaceNameFromKey($key);
	}
	
	/**
	 * Gets a suitable test name from a given key.
	 *
	 * @param string $key
	 * @return class-string
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function getTestNameFromKey(string $key) : string
	{
		/** @phpstan-ignore-next-line */
		return $this->getClassNameFromKey($key).'Test';
	}
	
	/**
	 * Gets a suitable test name with namespace from a given key.
	 *
	 * @param NamespaceHolderInterface $namespaceHolder
	 * @param string $key
	 * @return class-string
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function getNamespacedTestNameFromKey(NamespaceHolderInterface $namespaceHolder, string $key) : string
	{
		/** @phpstan-ignore-next-line */
		return $namespaceHolder->getTestNamespace().'\\'.$this->getTestNameFromKey($key);
	}
	
}
