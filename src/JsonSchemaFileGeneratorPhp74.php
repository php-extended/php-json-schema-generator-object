<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

use InvalidArgumentException;
use LogicException;
use PhpExtended\File\FileSystem;
use PhpExtended\HttpClient\ClientFactory;
use PhpExtended\SimpleCache\SimpleCacheFilesystem;
use PhpExtended\SimpleCache\SimpleCacheLogger;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use RuntimeException;

/**
 * JsonSchemaFileGeneratorPhp74 class file.
 * 
 * This class is a generator that generates new classes with compatibility for
 * php7.4+.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class JsonSchemaFileGeneratorPhp74 implements JsonSchemaFileGeneratorInterface
{
	
	/**
	 * The json schema resolver.
	 * 
	 * @var JsonSchemaResolver
	 */
	protected JsonSchemaResolver $_resolver;
	
	/**
	 * The composer package name for the interfaces.
	 * 
	 * @var string
	 */
	protected string $_interfacesComposerPackageName;
	
	/**
	 * The composer package name for the classes.
	 * 
	 * @var string
	 */
	protected string $_classesComposerPackageName;
	
	/**
	 * The composer package name for the test classes.
	 * 
	 * @var string
	 */
	protected string $_testsComposerPackageName;
	
	/**
	 * The metadata provider.
	 * 
	 * @var Php74MetadataProvider
	 */
	protected Php74MetadataProvider $_metadataProvider;
	
	/**
	 * The namespace holder.
	 *
	 * @var NamespaceHolder
	 */
	private NamespaceHolder $_namespaceHolder;
	
	/**
	 * Builds a new JsonSchemaFileGeneratorPhp74 with its data.
	 * 
	 * @param NamespaceHolder $namespaceHolder
	 * @param string $interfacesComposerPackageName
	 * @param string $classesComposerPackageName
	 * @param string $testsComposerPackageName
	 * @param ?LoggerInterface $logger
	 * @throws InvalidArgumentException
	 */
	public function __construct(
		NamespaceHolder $namespaceHolder,
		string $interfacesComposerPackageName,
		string $classesComposerPackageName,
		string $testsComposerPackageName,
		?LoggerInterface $logger = null
	) {
		$this->_namespaceHolder = $namespaceHolder;
		$this->_interfacesComposerPackageName = $interfacesComposerPackageName;
		$this->_classesComposerPackageName = $classesComposerPackageName;
		$this->_testsComposerPackageName = $testsComposerPackageName;
		
		$logger ??= new NullLogger();
		
		$filesystem = new FileSystem(__DIR__.'/../cache');
		$cache = new SimpleCacheLogger(new SimpleCacheFilesystem($filesystem), $logger);
		
		$clientFactory = new ClientFactory();
		$clientFactory->setLogger($logger);
		$httpClient = $clientFactory->createClient();
		
		$namespaceRepository = new NamespaceRepository($logger, $cache, $httpClient);
		$jsonSchemaRepository = new JsonSchemaRepository($logger, $cache, $httpClient, new JsonSchemaProvider());
		$this->_resolver = new JsonSchemaResolver($jsonSchemaRepository, $namespaceRepository);
		
		$this->_metadataProvider = new Php74MetadataProvider($this->_resolver);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaFileGeneratorInterface::generate()
	 * @throws LogicException
	 * @throws RuntimeException
	 */
	public function generate(JsonSchemaInterface $schema) : JsonSchemaFileCollectionInterface
	{
		$this->_resolver->setDefaults($schema, $this->_namespaceHolder);
		
		return $this->generateRecursive('root', $schema);
	}
	
	/**
	 * Generates recursively all code classes.
	 * 
	 * @param string $key
	 * @param JsonSchemaInterface $schema
	 * @return JsonSchemaFileCollectionInterface
	 * @throws LogicException
	 * @throws RuntimeException
	 */
	public function generateRecursive(string $key, JsonSchemaInterface $schema) : JsonSchemaFileCollectionInterface
	{
		$collection = new JsonSchemaFileCollection();
		
		switch(true)
		{
			case $schema instanceof JsonSchemaObjectInterface:
				$type = $schema->getAdditionalProperties();
				if(!empty($schema->getProperties()))
				{
					$collection = $collection->mergeWith($this->generateForObject($key, $schema));
				}
				elseif(null !== $type)
				{
					$collection = $collection->mergeWith($this->generateRecursive($key.'[]', $type));
				}
				break;
				
			case $schema instanceof JsonSchemaArrayInterface:
				$type = $schema->getItems();
				if(null !== $type)
				{
					$collection = $collection->mergeWith($this->generateRecursive($key.'[]', $type));
					
					break;
				}
				
				throw new LogicException('NOT IMPLEMENTED : not a list');
				
			default:
				// ignore null schemas
				if($schema->getType() === 'null')
				{
					break;
				}
				
				throw new LogicException('NOT IMPLEMENTED : unknown type');
		}
		
		foreach($schema->getDefs() as $key => $definition)
		{
			$collection = $collection->mergeWith($this->generateRecursive($key, $definition));
		}
		
		return $collection;
	}
	
	
	/**
	 * Generates all the code (interface, class and test) for a given object.
	 * 
	 * @param string $key
	 * @param JsonSchemaObjectInterface $schema
	 * @return JsonSchemaFileCollectionInterface
	 * @throws LogicException
	 * @throws RuntimeException
	 */
	public function generateForObject(string $key, JsonSchemaObjectInterface $schema) : JsonSchemaFileCollectionInterface
	{
		$collection = new JsonSchemaFileCollection();
		
		$interfaceMetadata = new Php74InterfaceMetadata(
			$this->_resolver->getDefaultNamespace(),
			$this->_interfacesComposerPackageName,
			$this->_metadataProvider->getInterfaceNameFromKey($key),
			$schema->getTitle(),
			$schema->getDescription(),
		);
		
		$classMetadata = new Php74ClassMetadata(
			$this->_resolver->getDefaultNamespace(),
			$this->_classesComposerPackageName,
			$this->_metadataProvider->getClassNameFromKey($key),
			$interfaceMetadata->getClassName(),
			null,
			'This is a simple implementation of the '.$interfaceMetadata->getClassName().'.',
		);
		
		/** @phpstan-ignore-next-line */ /** @psalm-suppress ArgumentTypeCoercion */
		$classMetadata->addUse($this->_resolver->getDefaultNamespace()->getInterfaceNamespace().'\\'.$interfaceMetadata->getClassName());
		
		$testMetadata = new Php74TestMetadata(
			$this->_resolver->getDefaultNamespace(),
			$this->_testsComposerPackageName,
			$this->_metadataProvider->getTestNameFromKey($key),
			$classMetadata->getClassName(),
			null,
			null,
		);
		
		/** @phpstan-ignore-next-line */ /** @psalm-suppress ArgumentTypeCoercion */
		$testMetadata->addUse($this->_resolver->getDefaultNamespace()->getClassNamespace().'\\'.$classMetadata->getClassName());
		
		/** @var JsonSchemaInterface $rawProperty */
		foreach($schema->getProperties() as $rawKey => $rawProperty)
		{
			$property = $this->_resolver->resolveSchema($rawProperty);
			$namespace = $this->_resolver->resolveNamespace($rawProperty);
			
			$required = \in_array($rawKey, $schema->getRequired(), true);
			
			/** @var Php74FieldMetadata $field */
			$field = $this->_metadataProvider->getFieldFrom($rawKey, $property, $namespace, $required);
			$returnType = new PhpSingleType($interfaceMetadata->getNamespacedClassName(), $interfaceMetadata->getNamespacedClassName());
			/** @var Php74MethodMetadata $setter */
			$setter = $this->_metadataProvider->getSetterFrom($rawKey, $property, $namespace, $required, $returnType);
			$setter->setRelatedField($field);
			/** @var Php74MethodMetadata $getter */
			$getter = $this->_metadataProvider->getGetterFrom($rawKey, $property, $namespace, $required);
			$getter->setRelatedField($field);
			/** @var Php74CompoundMetadata $compound */
			$compound = $this->_metadataProvider->getCompoundFor($rawKey, $field, $getter, $setter, $property, $namespace);
			
			$interfaceMetadata->addMethod($getter);
			$classMetadata->addField($field);
			$classMetadata->addMethod($setter);
			$classMetadata->addMethod($getter);
			$testMetadata->addCompound($compound);
		}
		
		$ifqcn = $testMetadata->getClassNamespace().'\\'.$interfaceMetadata->getClassName();
		/** @phpstan-ignore-next-line */ /** @psalm-suppress ArgumentTypeCoercion */
		$collection->addInterfaceFile($ifqcn, $interfaceMetadata->render());
		
		$cfqcn = $testMetadata->getClassNamespace().'\\'.$classMetadata->getClassName();
		/** @phpstan-ignore-next-line */ /** @psalm-suppress ArgumentTypeCoercion */
		$collection->addClassFile($cfqcn, $classMetadata->render());
		
		$tfqcn = $testMetadata->getTestNamespace().'\\'.$testMetadata->getTestClassName();
		/** @phpstan-ignore-next-line */ /** @psalm-suppress ArgumentTypeCoercion */
		$collection->addTestFile($tfqcn, $testMetadata->render());
		
		return $collection;
	}
	
}
