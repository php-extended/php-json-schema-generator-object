<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

use InvalidArgumentException;
use JsonException;
use PhpExtended\HttpMessage\Request;
use PhpExtended\Reifier\ReificationThrowable;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\UriInterface;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\CacheInterface;
use RuntimeException;
use Throwable;

/**
 * JsonSchemaRepository class file.
 * 
 * This is a simple implementation of the JsonSchemaRepositoryInterface using
 * logging, local cache and http retrieval of json schema data from a schema id.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class JsonSchemaRepository implements JsonSchemaRepositoryInterface
{
	
	/**
	 * The logger.
	 *
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * The cache client.
	 *
	 * @var CacheInterface
	 */
	protected CacheInterface $_cache;
	
	/**
	 * The http client.
	 *
	 * @var ClientInterface
	 */
	protected ClientInterface $_httpClient;
	
	/**
	 * The json schema provider.
	 * 
	 * @var JsonSchemaProviderInterface
	 */
	protected JsonSchemaProviderInterface $_provider;
	
	/**
	 * The known schemas.
	 * 
	 * @var array<string, JsonSchemaInterface>
	 */
	private array $_schemas = [];
	
	/**
	 * Builds a new JsonSchemaRepository with its dependancies.
	 * 
	 * @param LoggerInterface $logger
	 * @param CacheInterface $cache
	 * @param ClientInterface $httpClient
	 * @param JsonSchemaProviderInterface $jsonSchemaProvider
	 */
	public function __construct(LoggerInterface $logger, CacheInterface $cache, ClientInterface $httpClient, JsonSchemaProviderInterface $jsonSchemaProvider)
	{
		$this->_logger = $logger;
		$this->_cache = $cache;
		$this->_httpClient = $httpClient;
		$this->_provider = $jsonSchemaProvider;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaRepositoryInterface::findJsonSchema()
	 * @SuppressWarnings("PHPMD.ElseExpression")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function findJsonSchema(UriInterface $id) : JsonSchemaInterface
	{
		$id = $id->withFragment('');
		$key = $id->__toString();
		
		// {{{ first check : get the local object cache
		$known = $this->_schemas[$key] ?? null;
		if(null !== $known)
		{
			return $known;
		}
		// }}}
		
		// {{{ Second check : get from the forward cache
		try
		{
			$schema = $this->_cache->get($key);
			if(null !== $schema && $schema instanceof JsonSchemaInterface)
			{
				return $this->_schemas[$key] = $schema;
			}
		}
		// @codeCoverageIgnoreStart
		catch(\Psr\SimpleCache\InvalidArgumentException $e)
		{
			// ignore, should not happen
		}
		// @codeCoverageIgnoreEnd
		// }}}
		
		// {{{ Third check : get from the uri
		try
		{
			$request = new Request();
			$request = $request->withUri($id);
			$response = $this->_httpClient->sendRequest($request);
			if(200 !== $response->getStatusCode())
			{
				if($id->getHost() === 'gitlab.com') // special handling to get raw data
				{
					$uri1 = $this->getModifiedUri($id, '/-/raw/master/');
					$request = new Request();
					$request = $request->withUri($uri1);
					$response = $this->_httpClient->sendRequest($request);
					if(200 !== $response->getStatusCode())
					{
						$uri2 = $this->getModifiedUri($id, '/-/raw/main/');
						$request = new Request();
						$request = $request->withUri($uri2);
						$response = $this->_httpClient->sendRequest($request);
						if(200 !== $response->getStatusCode())
						{
							$message = 'Failed to get namespace values from uris {uri}, {uri1}, {uri2}';
							$context = ['{uri}' => $id, '{uri1}' => $uri1, '{uri2}' => $uri2];
							
							throw new RuntimeException(\strtr($message, $context));
						}
					}
				}
				else
				{
					$message = 'Failed to get namespace values from uri {uri}';
					$context = ['{uri}' => $id];
					
					throw new RuntimeException(\strtr($message, $context));
				}
			}
		}
		catch(Throwable $exc)
		{
			$message = 'Failed to get schema values from uri {uri}';
			$context = ['{uri}' => $id];
			
			throw new RuntimeException(\strtr($message, $context), -1, $exc);
		}
		// }}}
		
		try
		{
			$jsonSchema = $this->_provider->provideFromString($response->getBody()->__toString());
			$this->save($id, $jsonSchema);
			
			return $jsonSchema;
		}
		catch(ReificationThrowable $exc)
		{
			// should not happen on getting the right files
			// @codeCoverageIgnoreStart
			$message = 'Failed to build the json schema from uri "{uri}"';
			$context = ['{uri}' => $id];
			
			throw new RuntimeException(\strtr($message, $context), -2, $exc);
			// @codeCoverageIgnoreEnd
		}
		catch(JsonException $exc)
		{
			$message = 'Failed to decode json from json schema from uri "{uri}"';
			$context = ['{uri}' => $id];
			
			throw new RuntimeException(\strtr($message, $context), -3, $exc);
		}
	}
	
	/**
	 * Saves the given json schema for the given id.
	 * 
	 * @param UriInterface $id
	 * @param JsonSchemaInterface $jsonSchema
	 * @return boolean whether the save is a success
	 */
	public function save(UriInterface $id, JsonSchemaInterface $jsonSchema) : bool
	{
		try
		{
			$key = $id->withFragment('')->__toString();
			$this->_schemas[$key] = $jsonSchema;
			
			/** @psalm-suppress MissingThrowsDocblock */
			return $this->_cache->set($key, $jsonSchema, 86400);
		}
		// @codeCoverageIgnoreStart
		catch(Throwable $exc)
		{
			// should not happen
			return false;
		}
		// @codeCoverageIgnoreEnd
	}
	
	/**
	 * Gets a modified uri with the insert (branch) between the repo name and
	 * the file name.
	 *
	 * @param UriInterface $base
	 * @param string $insert
	 * @return UriInterface
	 * @throws InvalidArgumentException
	 */
	public function getModifiedUri(UriInterface $base, string $insert) : UriInterface
	{
		$uri1 = clone $base;
		$uriPath = $base->getPath();
		$uriPathParts = \explode('/', \trim($uriPath, '/'));
		/** @psalm-suppress PossiblyUndefinedIntArrayOffset */
		$uriPathNewParts = \array_merge([
			$uriPathParts[0],
			$uriPathParts[1],
		], \explode('/', $insert), \array_slice($uriPathParts, 2));
		
		return $uri1->withPath('/'.\implode('/', $uriPathNewParts));
	}
	
}
