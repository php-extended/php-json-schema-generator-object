<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

/**
 * PhpArrayType class file.
 * 
 * This represents an array with its inner type.
 * 
 * @author Anastaszor
 */
class PhpArrayType extends PhpAbstractType
{
	public const KEY_TYPE_INT_ONLY = 'int';
	public const KEY_TYPE_STR_ONLY = 'string';
	public const KEY_TYPE_INT_STR = 'int|string';
	
	/**
	 * The key type for this array.
	 * 
	 * @var string
	 */
	protected string $_keyType;
	
	/**
	 * The inner type.
	 * 
	 * @var PhpAbstractType
	 */
	protected PhpAbstractType $_innerType;
	
	/**
	 * Builds a new PhpArrayType with the given inner type.
	 * 
	 * @param string $keyType (use this class constants)
	 * @param PhpAbstractType $innerType
	 * @param boolean $nullable
	 */
	public function __construct(string $keyType, PhpAbstractType $innerType, bool $nullable = false)
	{
		parent::__construct($nullable);
		$this->_keyType = $keyType;
		$this->_innerType = $innerType;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\PhpAbstractType::getDocType()
	 */
	public function getDocType() : string
	{
		return 'array<'.$this->_keyType.', '.$this->_innerType->getDocType().'>';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\PhpAbstractType::getNamespace()
	 */
	public function getNamespace() : string
	{
		return '';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\PhpAbstractType::getName()
	 */
	public function getName() : string
	{
		return 'array';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\PhpAbstractType::isArray()
	 */
	public function isArray() : bool
	{
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\PhpAbstractType::collectNamespacedClasses()
	 */
	public function collectNamespacedClasses() : array
	{
		return $this->_innerType->collectNamespacedClasses();
	}
	
}
