<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

/**
 * JsonSchemaFileCollection class file.
 * 
 * This is a simple implementation of the JsonSchemaFileCollectionInterface.
 * 
 * @author Anastaszor
 */
class JsonSchemaFileCollection implements JsonSchemaFileCollectionInterface
{
	
	/**
	 * The raw data for interfaces files.
	 * 
	 * @var array<class-string, string>
	 */
	protected array $_interfaces = [];
	
	/**
	 * The raw data for classes files.
	 * 
	 * @var array<class-string, string>
	 */
	protected array $_classes = [];
	
	/**
	 * The raw data for tests files.
	 * 
	 * @var array<class-string, string>
	 */
	protected array $_tests = [];
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Saves a given interface file by its class name.
	 * 
	 * @param class-string $fqcn
	 * @param string $classCode
	 * @return JsonSchemaFileCollectionInterface
	 */
	public function addInterfaceFile(string $fqcn, string $classCode) : JsonSchemaFileCollectionInterface
	{
		$this->_interfaces[$fqcn] = $classCode;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaFileCollectionInterface::getInterfaceFiles()
	 */
	public function getInterfaceFiles() : array
	{
		return $this->_interfaces;
	}
	
	/**
	 * Saves a given class file by its class name.
	 * 
	 * @param class-string $fqcn
	 * @param string $classCode
	 * @return JsonSchemaFileCollectionInterface
	 */
	public function addClassFile(string $fqcn, string $classCode) : JsonSchemaFileCollectionInterface
	{
		$this->_classes[$fqcn] = $classCode;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaFileCollectionInterface::getClassFiles()
	 */
	public function getClassFiles() : array
	{
		return $this->_classes;
	}
	
	/**
	 * Saves a given test file by its class name.
	 * 
	 * @param class-string $fqcn
	 * @param string $classCode
	 * @return JsonSchemaFileCollectionInterface
	 */
	public function addTestFile(string $fqcn, string $classCode) : JsonSchemaFileCollectionInterface
	{
		$this->_tests[$fqcn] = $classCode;
		
		return $this;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaFileCollectionInterface::getTestFiles()
	 */
	public function getTestFiles() : array
	{
		return $this->_tests;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaFileCollectionInterface::mergeWith()
	 */
	public function mergeWith(?JsonSchemaFileCollectionInterface $other = null) : JsonSchemaFileCollectionInterface
	{
		if(null === $other)
		{
			return $this;
		}
		
		$new = new JsonSchemaFileCollection();
		
		foreach($this->getInterfaceFiles() as $fqcn => $classCode)
		{
			$new->addInterfaceFile($fqcn, $classCode);
		}
		
		foreach($other->getInterfaceFiles() as $fqcn => $classCode)
		{
			$new->addInterfaceFile($fqcn, $classCode);
		}
		
		foreach($this->getClassFiles() as $fqcn => $classCode)
		{
			$new->addClassFile($fqcn, $classCode);
		}
		
		foreach($other->getClassFiles() as $fqcn => $classCode)
		{
			$new->addClassFile($fqcn, $classCode);
		}
		
		foreach($this->getTestFiles() as $fqcn => $classCode)
		{
			$new->addTestFile($fqcn, $classCode);
		}
		
		foreach($other->getTestFiles() as $fqcn => $classCode)
		{
			$new->addTestFile($fqcn, $classCode);
		}
		
		return $new;
	}
	
}
