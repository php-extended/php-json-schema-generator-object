<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

use LogicException;
use Stringable;

/**
 * Php74MethodMetadata class file.
 * 
 * This represents all the metadata needed to render a method with php7.4+
 * compatibility.
 * 
 * @author Anastaszor
 */
class Php74MethodMetadata implements Stringable
{
	
	/**
	 * The title of the method.
	 * 
	 * @var ?string
	 */
	protected ?string $_title;
	
	/**
	 * The description of the method.
	 * 
	 * @var string
	 */
	protected ?string $_description;
	
	/**
	 * The name of the method.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * The parameters of the method.
	 * 
	 * @var array<string, Php74ParameterMetadata>
	 */
	protected array $_parameters = [];
	
	/**
	 * The return type of the method.
	 * 
	 * @var PhpAbstractType
	 */
	protected PhpAbstractType $_returnType;
	
	/**
	 * The field related to this method (setter or getter.
	 * 
	 * @var ?Php74FieldMetadata
	 */
	protected ?Php74FieldMetadata $_relatedField = null;
	
	/**
	 * Builds a new method metadata with its data.
	 * 
	 * @param ?string $title
	 * @param ?string $description
	 * @param string $name
	 * @param PhpAbstractType $returnType
	 */
	public function __construct(
		?string $title,
		?string $description,
		string $name,
		PhpAbstractType $returnType
	) {
		$this->_title = $title;
		$this->_description = $description;
		$this->_name = $name;
		$this->_returnType = $returnType;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets whether this method is a setter.
	 *
	 * @return boolean
	 */
	public function isSetter() : bool
	{
		return \substr($this->getName(), 0, 3) === 'set';
	}
	
	/**
	 * Gets whether this method is a getter.
	 *
	 * @return boolean
	 */
	public function isGetter() : bool
	{
		return \substr($this->getName(), 0, 3) === 'get' 
			|| \substr($this->getName(), 0, 3) === 'has'; // for booleans
	}
	
	/**
	 * Gets the title of the method.
	 * 
	 * @return ?string
	 */
	public function getTitle() : ?string
	{
		return $this->_title;
	}
	
	/**
	 * The description of the method.
	 * 
	 * @return ?string
	 */
	public function getDescription() : ?string
	{
		return $this->_description;
	}
	
	/**
	 * The name of the method.
	 * 
	 * @return string
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * Adds the given parameter with the given name to this method.
	 * 
	 * @param Php74ParameterMetadata $parameter
	 * @return Php74MethodMetadata
	 */
	public function addParameter(Php74ParameterMetadata $parameter) : Php74MethodMetadata
	{
		$this->_parameters[$parameter->getName()] = $parameter;
		
		return $this;
	}
	
	/**
	 * The parameters of the method.
	 * 
	 * @return array<integer, Php74ParameterMetadata>
	 */
	public function getParameters() : array
	{
		return \array_values($this->_parameters);
	}
	
	/**
	 * The return doc type of the method.
	 * 
	 * @return string
	 */
	public function getReturnDocType() : string
	{
		return $this->_returnType->getDocType();
	}
	
	/**
	 * The return php type of the method.
	 * 
	 * @return string
	 */
	public function getReturnPhpType() : string
	{
		return $this->_returnType->getName();
	}
	
	/**
	 * Gets whether the return type of this method is nullable.
	 * 
	 * @return boolean
	 */
	public function isReturnNullable() : bool
	{
		return $this->_returnType->isNullable();
	}
	
	/**
	 * Gets whether the return type of this method is an array.
	 * 
	 * @return boolean
	 */
	public function isReturnArray() : bool
	{
		return $this->_returnType->isArray();
	}
	
	/**
	 * Sets the related field of this method.
	 * 
	 * @param ?Php74FieldMetadata $relatedField
	 * @return Php74MethodMetadata
	 */
	public function setRelatedField(?Php74FieldMetadata $relatedField) : Php74MethodMetadata
	{
		$this->_relatedField = $relatedField;
		
		return $this;
	}
	
	/**
	 * Gets the related field of this method.
	 * 
	 * @return Php74FieldMetadata
	 * @throws LogicException
	 */
	public function getRelatedField() : Php74FieldMetadata
	{
		if(null === $this->_relatedField)
		{
			throw new LogicException('The related field for method '.$this->_name.' is null');
		}
		
		return $this->_relatedField;
	}
	
	/**
	 * Collects all the namespaced classes for this method.
	 * 
	 * @return array<integer, class-string>
	 */
	public function collectNamespacedClasses() : array
	{
		$collected = $this->_returnType->collectNamespacedClasses();
		
		/** @var Php74ParameterMetadata $param */
		foreach($this->_parameters as $param)
		{
			$collected = \array_merge($collected, $param->collectNamespacedClasses());
		}
		
		return $collected;
	}
	
	/**
	 * Gets approximatively the count of lines that are to be rendered for this
	 * method.
	 * 
	 * @return integer
	 */
	public function getLineCount() : int
	{
		$descriptions = [];
		
		foreach(\explode("\n", (string) $this->_description) as $descriptionLine)
		{
			$descriptions = \array_merge($descriptions, \explode("\n", \wordwrap($descriptionLine, 76, "\n", false)));
		}
		
		return 1 // /*
			 + (null === $this->_title || '' === $this->_title ? 0 : 2)
			 + \count($descriptions)
			 + 1 // *
			 + (int) $this->isSetter() // @param
			 + 1 // @return
			 + 1 // */
			 + 1 // public function
			 + 1 // {
			 + 1 // $this
			 + ($this->isSetter() ? 2 : 0)
			 + 1 // }
			 + 1; // (blank)
	}
	
}
