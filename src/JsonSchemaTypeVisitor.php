<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

use DateTimeInterface;
use LogicException;
use PhpExtended\Email\EmailAddressInterface;
use PhpExtended\Email\MailboxListInterface;
use PhpExtended\HttpClient\AcceptLanguageChainInterface;
use PhpExtended\Uuid\UuidInterface;
use PhpExtended\Version\VersionInterface;
use Psr\Http\Message\UriInterface;
use RuntimeException;

/**
 * JsonSchemaTypeVisitor class file.
 * 
 * This is a visitor that transforms json schema values into php types.
 * 
 * @author Anastaszor
 * @extends JsonSchemaAbstractVisitor<PhpAbstractType>
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 * @todo make it a factory ?
 */
class JsonSchemaTypeVisitor extends JsonSchemaAbstractVisitor
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaVisitorNoop::visitSchemaRaw()
	 * @throws LogicException
	 */
	public function visitSchemaRaw(JsonSchemaInterface $schema) : PhpAbstractType
	{		
		throw new LogicException('NOT IMPLEMENTED TYPE SCHEMA '.((string) \json_encode($schema, \JSON_PRETTY_PRINT)));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaVisitorNoop::visitSchemaArray()
	 * @throws LogicException
	 * @throws RuntimeException
	 */
	public function visitSchemaArray(JsonSchemaArrayInterface $schema) : PhpAbstractType
	{
		$items = $schema->getItems();
		if(null === $items)
		{
			throw new LogicException('NOT IMPLEMENTED NOT A LIST');
		}
		
		if(null === $this->_key || null === $this->_namespace)
		{
			throw new LogicException('Failed to get a proper key or namespace for this schema, maybe you have used visit() instead of handle() ?');
		}
		
		$newKey = $this->_key.'[]';
		$newNamespace = $this->_resolver->resolveNamespace($items);
		$newSchema = $this->_resolver->resolveSchema($items);
		/** @var PhpAbstractType $recursive */
		$recursive = $this->handle($newKey, $newSchema, $newNamespace);
		
		return new PhpArrayType(PhpArrayType::KEY_TYPE_INT_ONLY, $recursive);
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaVisitorNoop::visitSchemaBoolean()
	 */
	public function visitSchemaBoolean(JsonSchemaBooleanInterface $schema) : PhpAbstractType
	{
		return new PhpSingleType('bool', 'bool');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaVisitorNoop::visitSchemaFloat()
	 */
	public function visitSchemaFloat(JsonSchemaFloatInterface $schema) : PhpAbstractType
	{
		return new PhpSingleType('float', 'float');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaVisitorNoop::visitSchemaInteger()
	 */
	public function visitSchemaInteger(JsonSchemaIntegerInterface $schema) : PhpAbstractType
	{
		return new PhpSingleType('int', 'int');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaVisitorNoop::visitSchemaObject()
	 * @throws LogicException
	 * @throws RuntimeException
	 */
	public function visitSchemaObject(JsonSchemaObjectInterface $schema) : PhpAbstractType
	{
		if(null === $this->_key || null === $this->_namespace)
		{
			throw new LogicException('Failed to get a proper key or namespace for this schema, maybe you have used visit() instead of handle() ?');
		}
		
		$items = $schema->getAdditionalProperties();
		if(empty($schema->getProperties()) && null !== $items)
		{
			$newKey = $this->_key.'[]';
			$newNamespace = $this->_resolver->resolveNamespace($items);
			$newSchema = $this->_resolver->resolveSchema($items);
			/** @var PhpAbstractType $recursive */
			$recursive = $this->handle($newKey, $newSchema, $newNamespace);
			
			return new PhpArrayType(PhpArrayType::KEY_TYPE_STR_ONLY, $recursive);
		}
		
		$ref = $schema->getRef();
		$classKey = null === $ref ? $this->_key : \basename(\rawurldecode($ref->__toString()));
		$className = $this->getNamespacedInterfaceNameFromKey($this->_namespace, $classKey);
		
		return new PhpSingleType($className, $className);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaVisitorNoop::visitSchemaString()
	 * @throws LogicException
	 */
	public function visitSchemaString(JsonSchemaStringInterface $schema) : PhpAbstractType
	{
		$stringType = $this->getStringTypeFromSchema($schema);
		
		return new PhpSingleType($stringType, $stringType);
	}
	
	/**
	 * Gets the php type for a string schema.
	 *
	 * @param JsonSchemaStringInterface $schema
	 * @return class-string|'string'
	 * @throws LogicException
	 */
	public function getStringTypeFromSchema(JsonSchemaStringInterface $schema) : string
	{
		switch($schema->getFormat())
		{
			case 'accept-language-chain':
				return AcceptLanguageChainInterface::class;
				
			case 'date':
			case 'date-time':
			case 'time':
				return DateTimeInterface::class;
				
			case 'email':
				return EmailAddressInterface::class;
				
			case 'mailbox-list':
				return MailboxListInterface::class;
				
			case 'uri':
				return UriInterface::class;
				
			case 'uuid':
				return UuidInterface::class;
				
			case 'version':
				return VersionInterface::class;
				
			case '':
			case null:
				return 'string';
				
			default:
				/** @phpstan-ignore-next-line */
				throw new LogicException('NOT IMPLEMENTED : '.\get_class($schema).' - '.($schema->getFormat() ?? '(no format)'));
		}
	}
	
}
