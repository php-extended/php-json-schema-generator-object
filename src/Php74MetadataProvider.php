<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

use LogicException;
use Stringable;

/**
 * Php74MetadataProvider class file.
 * 
 * This is a generic provider to be overridden by providers that are specific
 * to a type of schema.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class Php74MetadataProvider implements Stringable
{
	use PhpNamingTrait;
	
	/**
	 * The json schema type visitor.
	 * 
	 * @var JsonSchemaTypeVisitor
	 */
	protected JsonSchemaTypeVisitor $_typeVisitor;
	
	/**
	 * The json schema default value visitor for compounds.
	 * 
	 * @var JsonSchemaDefaultVisitor
	 */
	protected JsonSchemaDefaultVisitor $_defaultVisitor;
	
	/**
	 * The json schema calculated value visitor for compounds.
	 * 
	 * @var JsonSchemaCalculatedVisitor
	 */
	protected JsonSchemaCalculatedVisitor $_calculatedVisitor;
	
	/**
	 * The json schema test uses visitor for compounds.
	 * 
	 * @var JsonSchemaTestUsesVisitor
	 */
	protected JsonSchemaTestUsesVisitor $_testUsesVisitor;
	
	/**
	 * Builds a new Php74MetadataProvider with DI.
	 * 
	 * @param JsonSchemaResolver $resolver
	 */
	public function __construct(JsonSchemaResolver $resolver)
	{
		$this->_typeVisitor = new JsonSchemaTypeVisitor($resolver);
		$this->_defaultVisitor = new JsonSchemaDefaultVisitor($resolver);
		$this->_calculatedVisitor = new JsonSchemaCalculatedVisitor($resolver);
		$this->_testUsesVisitor = new JsonSchemaTestUsesVisitor($resolver);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the field metadata.
	 * 
	 * @param string $key
	 * @param JsonSchemaInterface $schema
	 * @param NamespaceHolderInterface $namespace
	 * @param bool $required
	 * @return Php74FieldMetadata
	 * @throws LogicException
	 */
	public function getFieldFrom(string $key, JsonSchemaInterface $schema, NamespaceHolderInterface $namespace, bool $required) : Php74FieldMetadata
	{
		return new Php74FieldMetadata(false, $schema->getTitle(), $schema->getDescription(), $this->getParameterNameFromKey($key), $this->getTypeFromSchema($key, $schema, $namespace, $required));
	}
	
	/**
	 * Gets the getter method from a schema.
	 * 
	 * @param string $key
	 * @param JsonSchemaInterface $schema
	 * @param NamespaceHolderInterface $namespace
	 * @param bool $required
	 * @return Php74MethodMetadata
	 * @throws LogicException
	 */
	public function getGetterFrom(string $key, JsonSchemaInterface $schema, NamespaceHolderInterface $namespace, bool $required) : Php74MethodMetadata
	{
		return new Php74MethodMetadata($schema->getTitle(), 'Gets '.\lcfirst((string) $schema->getDescription()), ($schema->getType() === 'boolean' ? 'has' : 'get').$this->getClassNameFromKey($key), $this->getTypeFromSchema($key, $schema, $namespace, $required));
	}
	
	/**
	 * Gets the setter method from a schema.
	 * 
	 * @param string $key
	 * @param JsonSchemaInterface $schema
	 * @param NamespaceHolderInterface $namespace
	 * @param bool $required
	 * @param PhpAbstractType $returnType
	 * @return Php74MethodMetadata
	 * @throws LogicException
	 */
	public function getSetterFrom(string $key, JsonSchemaInterface $schema, NamespaceHolderInterface $namespace, bool $required, PhpAbstractType $returnType) : Php74MethodMetadata
	{
		$method = new Php74MethodMetadata($schema->getTitle(), 'Sets '.\lcfirst((string) $schema->getDescription()), 'set'.$this->getClassNameFromKey($key), $returnType);
		$method->addParameter(new Php74ParameterMetadata($this->getParameterNameFromKey($key), null, $this->getTypeFromSchema($key, $schema, $namespace, $required)));
		
		return $method;
	}
	
	/**
	 * Gets the compound from it scomponents.
	 * 
	 * @param string $key
	 * @param Php74FieldMetadata $field
	 * @param Php74MethodMetadata $getter
	 * @param Php74MethodMetadata $setter
	 * @param JsonSchemaInterface $schema
	 * @param NamespaceHolderInterface $namespace
	 * @return Php74CompoundMetadata
	 * @throws LogicException
	 */
	public function getCompoundFor(string $key, Php74FieldMetadata $field, Php74MethodMetadata $getter, Php74MethodMetadata $setter, JsonSchemaInterface $schema, NamespaceHolderInterface $namespace) : Php74CompoundMetadata
	{
		return new Php74CompoundMetadata($namespace, $field, $getter, $setter, $this->_defaultVisitor->handle($key, $schema, $namespace), $this->_calculatedVisitor->handle($key, $schema, $namespace), $this->_testUsesVisitor->handle($key, $schema, $namespace));
	}
	
	/**
	 * Gets the type from the given schema.
	 * 
	 * @param string $key
	 * @param JsonSchemaInterface $schema
	 * @param NamespaceHolderInterface $namespace
	 * @param bool $required
	 * @return PhpAbstractType
	 * @throws LogicException
	 */
	public function getTypeFromSchema(string $key, JsonSchemaInterface $schema, NamespaceHolderInterface $namespace, bool $required) : PhpAbstractType
	{
		/** @var PhpAbstractType $type */
		$type = $this->_typeVisitor->handle($key, $schema, $namespace);
		
		return $type->setNullable(!$required);
	}
	
}
