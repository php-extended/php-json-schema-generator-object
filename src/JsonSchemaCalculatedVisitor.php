<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

use InvalidArgumentException;
use LogicException;
use RuntimeException;

/**
 * JsonSchemaCalculatedVisitor class file.
 * 
 * This visitor generates calculated values for a schema.
 * 
 * @author Anastaszor
 * @extends JsonSchemaAbstractVisitor<string>
 */
class JsonSchemaCalculatedVisitor extends JsonSchemaAbstractVisitor
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaVisitorNoop::visitSchemaRaw()
	 * @throws LogicException
	 */
	public function visitSchemaRaw(JsonSchemaInterface $schema) : string
	{
		throw new LogicException('NOT IMPLEMENTED TYPE OTHER THAN BOOLEAN');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaVisitorNoop::visitSchemaArray()
	 * @throws InvalidArgumentException
	 * @throws LogicException
	 * @throws RuntimeException
	 */
	public function visitSchemaArray(JsonSchemaArrayInterface $schema) : string
	{
		$items = $schema->getItems();
		if(null === $items)
		{
			throw new LogicException('NOT IMPLEMENTED NOT A LIST');
		}
		
		if(null === $this->_key || null === $this->_namespace)
		{
			throw new LogicException('Failed to get a proper key or namespace for this schema, maybe you have used visit() instead of handle() ?');
		}
		
		$newKey = $this->_key.'[]';
		$newNamespace = $this->_resolver->resolveNamespace($items);
		$newSchema = $this->_resolver->resolveSchema($items);
		$recursive = $this->handle($newKey, $newSchema, $newNamespace);
		
		return '['.$recursive.', '.$recursive.']';
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaVisitorNoop::visitSchemaBoolean()
	 */
	public function visitSchemaBoolean(JsonSchemaBooleanInterface $schema) : string
	{
		return 'true';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaVisitorNoop::visitSchemaFloat()
	 */
	public function visitSchemaFloat(JsonSchemaFloatInterface $schema) : string
	{
		return '15.2';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaVisitorNoop::visitSchemaInteger()
	 */
	public function visitSchemaInteger(JsonSchemaIntegerInterface $schema) : string
	{
		return '25';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaVisitorNoop::visitSchemaObject()
	 * @throws LogicException
	 * @throws RuntimeException
	 */
	public function visitSchemaObject(JsonSchemaObjectInterface $schema) : string
	{
		if(null === $this->_key || null === $this->_namespace)
		{
			throw new LogicException('Failed to get a proper key or namespace for this schema, maybe you have used visit() instead of handle() ?');
		}
		
		$items = $schema->getAdditionalProperties();
		if(empty($schema->getProperties()) && null !== $items)
		{
			$newKey = $this->_key.'[]';
			$newNamespace = $this->_resolver->resolveNamespace($items);
			$newSchema = $this->_resolver->resolveSchema($items);
			$recursive = $this->handle($newKey, $newSchema, $newNamespace);
			
			return '[\'key1\' => '.$recursive.', \'key2\' => '.$recursive.']';
		}
		
		$ref = $schema->getRef();
		$classKey = null === $ref ? $this->_key : \basename(\rawurldecode($ref->__toString()));
		
		return '$this->getMockBuilder('.$this->getClassNameFromKey($classKey).'::class)->disableOriginalConstructor()->getMock()';
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaVisitorNoop::visitSchemaString()
	 */
	public function visitSchemaString(JsonSchemaStringInterface $schema) : string
	{
		switch($schema->getFormat())
		{
			case 'accept-language-chain':
				return '(new AcceptLanguageChainParser())->parse(\'fr-FR,en-US;q=0.8,en;q=0.5\')';
			
			case 'date':
				return 'DateTimeImmutable::createFromFormat(\'!Y-m-d\', \'2001-01-01\')';
				
			case 'date-time':
				return 'DateTimeImmutable::createFromFormat(\'!Y-m-d H:i:s\', \'2001-01-01 00:00:01\')';
				
			case 'email':
				return '(new EmailAddressParser())->parse(\'admin@example.com\')';
				
			case 'mailbox-list':
				return '(new MailboxListParser())->parse(\'Admin <admin@example.com>, "Other" <other@example.com>\')';
				
			case 'time':
				return 'DateTimeImmutable::createFromFormat(\'!H:i:s\', \'01:01:01\')';
				
			case 'uri':
				return '(new UriParser())->parse(\'https://admin.example.com\')';
				
			case 'uuid':
				return '(new UuidParser())->parse(\'b592ccab-7e43-4de9-8460-34592395addb\')';
				
			case 'version':
				return '(new VersionParser())->parse(\'1.2.3-beta\')';
				
			case '':
			case null:
				return "'qsdfghjklm'";
				
			default:
				/** @phpstan-ignore-next-line */
				throw new LogicException('NOT IMPLEMENTED : '.\get_class($schema).' - '.($schema->getFormat() ?? '(no format)'));
		}
	}
	
}
