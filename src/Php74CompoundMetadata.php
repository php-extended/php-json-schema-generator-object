<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

use Stringable;

/**
 * Php74CompoundMetadata class file.
 * 
 * This represents all the metadata needed to render a test method with
 * php7.4+ compatibility.
 * 
 * @author Anastaszor
 */
class Php74CompoundMetadata implements Stringable
{
	
	/**
	 * The namespace holder for this compound.
	 * 
	 * @var NamespaceHolderInterface
	 */
	protected NamespaceHolderInterface $_namespaceHolder;
	
	/**
	 * The field to be tested by this compound.
	 * 
	 * @var Php74FieldMetadata
	 */
	protected Php74FieldMetadata $_field;
	
	/**
	 * The getter method related to the field.
	 * 
	 * @var Php74MethodMetadata
	 */
	protected Php74MethodMetadata $_getter;
	
	/**
	 * The setter method related to the field.
	 * 
	 * @var Php74MethodMetadata
	 */
	protected Php74MethodMetadata $_setter;
	
	/**
	 * The default value.
	 * 
	 * @var string
	 */
	protected string $_defaultValue;
	
	/**
	 * The calculated value.
	 * 
	 * @var string
	 */
	protected string $_calculatedValue;
	
	/**
	 * The additional uses this compound needs.
	 * 
	 * @var array<integer, class-string>
	 */
	protected array $_uses = [];
	
	/**
	 * Builds a new Compound.
	 * 
	 * @param NamespaceHolderInterface $namespaceHolder
	 * @param Php74FieldMetadata $field
	 * @param Php74MethodMetadata $getter
	 * @param Php74MethodMetadata $setter
	 * @param string $defaultValue
	 * @param string $calculatedValue
	 * @param array<integer, class-string> $additionalUses
	 */
	public function __construct(
		NamespaceHolderInterface $namespaceHolder,
		Php74FieldMetadata $field,
		Php74MethodMetadata $getter,
		Php74MethodMetadata $setter,
		string $defaultValue,
		string $calculatedValue,
		array $additionalUses = []
	) {
		$this->_namespaceHolder = $namespaceHolder;
		$this->_field = $field;
		$this->_getter = $getter;
		$this->_setter = $setter;
		$this->_defaultValue = $defaultValue;
		$this->_calculatedValue = $calculatedValue;
		$this->_uses = $additionalUses;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets whether this field is public.
	 * 
	 * @return boolean
	 */
	public function isPublic() : bool
	{
		return $this->_field->isPublic();
	}
	
	/**
	 * Gets whether this field is nullable.
	 * 
	 * @return boolean
	 */
	public function isNullable() : bool
	{
		return $this->_field->isNullable();
	}
	
	/**
	 * Gets whether this field is an array.
	 * 
	 * @return boolean
	 */
	public function isArray() : bool
	{
		return $this->_field->isArray();
	}
	
	/**
	 * Gets the name of the field.
	 * 
	 * @return string
	 */
	public function getFieldName() : string
	{
		return $this->_field->getName();
	}
	
	/**
	 * Gets the name of the getter method.
	 * 
	 * @return string
	 */
	public function getGetterName() : string
	{
		return $this->_getter->getName();
	}
	
	/**
	 * Gets the name of the setter method.
	 * 
	 * @return string
	 */
	public function getSetterName() : string
	{
		return $this->_setter->getName();
	}
	
	/**
	 * Gets the default value.
	 * 
	 * @return string
	 */
	public function getDefaultValue() : string
	{
		return $this->_defaultValue;
	}
	
	/**
	 * Gets the calculated value.
	 * 
	 * @return string
	 */
	public function getCalculatedValue() : string
	{
		return $this->_calculatedValue;
	}
	
	/**
	 * Adds all the new classes to uses statements of this interface metadata.
	 *
	 * @param array<integer, class-string> $usedFqcns
	 * @return Php74CompoundMetadata
	 */
	public function addUses(array $usedFqcns) : Php74CompoundMetadata
	{
		foreach($usedFqcns as $usedFqcn)
		{
			$this->addUse($usedFqcn);
		}
		
		return $this;
	}
	
	/**
	 * Adds a new class to the uses statements of this interface metadata.
	 *
	 * @param class-string $usedFqcn
	 * @return Php74CompoundMetadata
	 */
	public function addUse(string $usedFqcn) : Php74CompoundMetadata
	{
		$usedFqcn = \trim($usedFqcn, '\\/');
		/** @phpstan-ignore-next-line */ /** @psalm-suppress PropertyTypeCoercion */
		$this->_uses[] = $usedFqcn;
		
		return $this;
	}
	
	/**
	 * Gets the classes used in this interface.
	 *
	 * @return array<integer, class-string>
	 */
	public function getUses() : array
	{
		return $this->_uses;
	}
	
}
