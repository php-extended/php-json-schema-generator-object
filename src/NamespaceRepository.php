<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

use InvalidArgumentException;
use JsonException;
use PhpExtended\HttpMessage\Request;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\UriInterface;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\CacheInterface;
use RuntimeException;
use Throwable;

/**
 * NamespaceRepository class file.
 * 
 * This is a simple implementation of the NamespaceRepositoryInterface using
 * logging, local cache and http retrieval of namespace data from a schema id.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class NamespaceRepository implements NamespaceRepositoryInterface
{
	
	/**
	 * The logger.
	 * 
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * The cache client.
	 * 
	 * @var CacheInterface
	 */
	protected CacheInterface $_cache;
	
	/**
	 * The http client.
	 * 
	 * @var ClientInterface
	 */
	protected ClientInterface $_httpClient;
	
	/**
	 * The known namespaces.
	 * 
	 * @var array<string, NamespaceHolderInterface>
	 */
	private array $_namespaces = [];
	
	/**
	 * Builds a new NamespaceRepository with its dependancies.
	 * 
	 * @param LoggerInterface $logger
	 * @param CacheInterface $cache
	 * @param ClientInterface $httpClient
	 */
	public function __construct(LoggerInterface $logger, CacheInterface $cache, ClientInterface $httpClient)
	{
		$this->_logger = $logger;
		$this->_cache = $cache;
		$this->_httpClient = $httpClient;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\NamespaceRepositoryInterface::findNamespaces()
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.ElseExpression")
	 */
	public function findNamespaces(UriInterface $id) : NamespaceHolderInterface
	{
		try
		{
			$id = $id->withFragment('');
			$id = $id->withPath(\str_replace('json-schema.json', 'composer.json', $id->getPath()));
		}
		// @codeCoverageIgnoreStart
		catch(InvalidArgumentException $exc)
		{
			// should not happen
		}
		// @codeCoverageIgnoreEnd
		
		$key = $id->__toString();
		
		// {{{ First check : get the local object cache
		$known = $this->_namespaces[$key] ?? null;
		if(null !== $known)
		{
			return $known;
		}
		// }}}
		
		// {{{ Second check : get from the forward cache
		try
		{
			$namespaces = $this->_cache->get($key);
			if(null !== $namespaces && $namespaces instanceof NamespaceHolderInterface)
			{
				return $this->_namespaces[$key] = $namespaces;
			}
		}
		// @codeCoverageIgnoreStart
		catch(\Psr\SimpleCache\InvalidArgumentException $e)
		{
			// ignore, should not happen
		}
		// @codeCoverageIgnoreEnd
		// }}}
		
		// {{{ Third check : get from the uri
		try
		{
			$request = new Request();
			$request = $request->withUri($id);
			$response = $this->_httpClient->sendRequest($request);
			if(200 !== $response->getStatusCode())
			{
				if($id->getHost() === 'gitlab.com') // special handling to get raw data
				{
					$uri1 = $this->getModifiedUri($id, '/-/raw/master/');
					$request = new Request();
					$request = $request->withUri($uri1);
					$response = $this->_httpClient->sendRequest($request);
					if(200 !== $response->getStatusCode())
					{
						$uri2 = $this->getModifiedUri($id, '/-/raw/main/');
						$request = new Request();
						$request = $request->withUri($uri2);
						$response = $this->_httpClient->sendRequest($request);
						if(200 !== $response->getStatusCode())
						{
							$message = 'Failed to get namespace values from uris {uri}, {uri1}, {uri2}';
							$context = ['{uri}' => $id, '{uri1}' => $uri1, '{uri2}' => $uri2];
							
							throw new RuntimeException(\strtr($message, $context));
						}
					}
				}
				else
				{
					$message = 'Failed to get namespace values from uri {uri}';
					$context = ['{uri}' => $id];
					
					throw new RuntimeException(\strtr($message, $context));
				}
			}
		}
		catch(Throwable $exc)
		{
			$message = 'Failed to get namespace values from uri {uri}';
			$context = ['{uri}' => $id];
			
			throw new RuntimeException(\strtr($message, $context), -1, $exc);
		}
		// }}}
		
		$composerRawData = $response->getBody()->__toString();
		// TODO remplace all that follows by exploiting the composer parsed library
		
		try
		{
			$composerJsonData = \json_decode($composerRawData, true, 512, \JSON_THROW_ON_ERROR);
		}
		catch(JsonException $exc)
		{
			$message = 'Failed to decode json data from uri {uri}';
			$context = ['{uri}' => $id];
			
			throw new RuntimeException(\strtr($message, $context), -2, $exc);
		}
		
		if(!\is_array($composerJsonData))
		{
			// should not happen if we found a valid composer.json
			// @codeCoverageIgnoreStart
			$message = 'Failed to decode json data to array from uri {uri}';
			$context = ['{uri}' => $id];
			
			throw new RuntimeException(\strtr($message, $context));
			// @codeCoverageIgnoreEnd
		}
		
		/** @psalm-suppress PossiblyUndefinedStringArrayOffset,MixedArrayAccess */
		$composerPsr4 = $composerJsonData['autoload']['psr-4'];
		if(!\is_array($composerPsr4))
		{
			// should not happen if we found a valid composer.json
			// @codeCoverageIgnoreStart
			$message = 'Failed to find autoloading array in psr-4 key in composer.json at uri {uri}';
			$context = ['{uri}' => $id];
			
			throw new RuntimeException(\strtr($message, $context));
			// @codeCoverageIgnoreEnd
		}
		
		$namespace = \array_keys($composerPsr4)[0] ?? null;
		if(null === $namespace || !\is_string($namespace))
		{
			// should not happen if we found a valid composer.json
			// @codeCoverageIgnoreStart
			$message = 'Failed to find autoloading namespace in psr-4 key in composer.json at uri {uri}';
			$context = ['{uri}' => $id];
			
			throw new RuntimeException(\strtr($message, $context));
			// @codeCoverageIgnoreEnd
		}
		
		$namespace = \trim((string) $namespace, '\\/');
		$namespaces = new NamespaceHolder($namespace, $namespace, $namespace.'\\Test');
		$this->save($id, $namespaces);
		
		return $namespaces;
	}
	
	/**
	 * Saves the given namespaces for the given id.
	 * 
	 * @param UriInterface $id
	 * @param NamespaceHolderInterface $namespaces
	 * @return boolean whether the save is a success
	 */
	public function save(UriInterface $id, NamespaceHolderInterface $namespaces) : bool
	{
		try
		{
			$id = $id->withPath(\str_replace('json-schema.json', 'composer.json', $id->getPath()))
				->withFragment('')
			;
			$key = $id->__toString();
			$this->_namespaces[$key] = $namespaces;
			
			/** @psalm-suppress MissingThrowsDocblock */
			return $this->_cache->set($key, $namespaces, 86400);
		}
		// @codeCoverageIgnoreStart
		catch(Throwable $exc)
		{
			// should not happen
			return false;
		}
		// @codeCoverageIgnoreEnd
	}
	
	/**
	 * Gets a modified uri with the insert (branch) between the repo name and
	 * the file name.
	 * 
	 * @param UriInterface $base
	 * @param string $insert
	 * @return UriInterface
	 * @throws InvalidArgumentException
	 */
	public function getModifiedUri(UriInterface $base, string $insert) : UriInterface
	{
		$uri1 = clone $base;
		$uriPath = $base->getPath();
		$uriPathParts = \explode('/', \trim($uriPath, '/'));
		/** @psalm-suppress PossiblyUndefinedIntArrayOffset */
		$uriPathNewParts = \array_merge([
			$uriPathParts[0],
			$uriPathParts[1],
		], \explode('/', $insert), \array_slice($uriPathParts, 2));
		
		return $uri1->withPath('/'.\implode('/', $uriPathNewParts));
	}
	
}
