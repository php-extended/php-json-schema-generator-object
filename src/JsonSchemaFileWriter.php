<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use RuntimeException;

/**
 * JsonSchemaFileWriter class file.
 * 
 * This is an implementation of the JsonSchemaFileWriterInterface that is able
 * to write all interfaces, classes and tests into their respective folder on
 * the filesystem.
 * 
 * @author Anastaszor
 */
class JsonSchemaFileWriter implements JsonSchemaFileWriterInterface
{
	
	/**
	 * The logger.
	 * 
	 * @var LoggerInterface
	 */
	protected LoggerInterface $_logger;
	
	/**
	 * The path where to store interfaces files.
	 * 
	 * @var string
	 */
	protected string $_interfacesFolderPath;
	
	/**
	 * The path where to store classes files.
	 * 
	 * @var string
	 */
	protected string $_classesFolderPath;
	
	/**
	 * The path where to store tests files.
	 * 
	 * @var string
	 */
	protected string $_testsFolderPath;
	
	/**
	 * Builds a new JsonSchemaFileWriter with its needed data.
	 * 
	 * @param LoggerInterface $logger,
	 * @param string $interfacesFolderPath
	 * @param string $classesFolderPath
	 * @param string $testsFolderPath
	 * @throws InvalidArgumentException
	 */
	public function __construct(LoggerInterface $logger, string $interfacesFolderPath, string $classesFolderPath, string $testsFolderPath)
	{
		$this->_logger = $logger;
		$this->_interfacesFolderPath = $this->checkWriteable('interfaces', $interfacesFolderPath);
		$this->_classesFolderPath = $this->checkWriteable('classes', $classesFolderPath);
		$this->_testsFolderPath = $this->checkWriteable('tests', $testsFolderPath);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaFileWriterInterface::write()
	 */
	public function write(JsonSchemaFileCollectionInterface $collection) : int
	{
		$count = 0;
		
		foreach($collection->getInterfaceFiles() as $ifqcn => $contents)
		{
			$className = \basename(\trim(\str_replace('\\', '/', $ifqcn), '/'));
			$classPath = $this->_interfacesFolderPath.\DIRECTORY_SEPARATOR.$className.'.php';
			$count += $this->writeAt($className, $classPath, $contents);
		}
		
		foreach($collection->getClassFiles() as $cfqcn => $contents)
		{
			$className = \basename(\trim(\str_replace('\\', '/', $cfqcn), '/'));
			$classPath = $this->_classesFolderPath.\DIRECTORY_SEPARATOR.$className.'.php';
			$count += $this->writeAt($className, $classPath, $contents);
		}
		
		foreach($collection->getTestFiles() as $tfqcn => $contents)
		{
			$className = \basename(\trim(\str_replace('\\', '/', $tfqcn), '/'));
			$classPath = $this->_testsFolderPath.\DIRECTORY_SEPARATOR.$className.'.php';
			$count += $this->writeAt($className, $classPath, $contents);
		}
		
		return $count;
	}
	
	/**
	 * Checks whether the given path is a writeable directory.
	 * 
	 * @param string $type
	 * @param string $path
	 * @return string the real writeable directory path
	 * @throws InvalidArgumentException if the folder is not writeable
	 */
	public function checkWriteable(string $type, string $path) : string
	{
		$realpath = \realpath($path);
		if(false === $realpath)
		{
			$message = 'The given path for {type} does not point to an existing folder at {path}';
			$context = ['{type}' => $type, '{path}' => $path];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
		
		if(!\is_dir($realpath))
		{
			$message = 'The {type} folder does not exists or is not a folder at {path}';
			$context = ['{type}' => $type, '{path}' => $realpath];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
		
		if(!\is_writable($realpath))
		{
			// code not testable
			// @codeCoverageIgnoreStart
			if(!\chmod($realpath, 0755))
			{
				$message = 'The {type} folder is not writeable at {path}';
				$context = ['{type}' => $type, '{path}' => $realpath];
				
				throw new InvalidArgumentException(\strtr($message, $context));
			}
			// @codeCoverageIgnoreEnd
		}
		
		return $realpath;
	}
	
	/**
	 * Writes the given contents for the file at the given path.
	 * 
	 * @param string $className
	 * @param string $classPath
	 * @param string $contents
	 * @return integer the number of files written
	 * @throws RuntimeException
	 */
	public function writeAt(string $className, string $classPath, string $contents) : int
	{
		$this->_logger->info('Writing {className} at {classPath}', ['className' => $className, 'classPath' => $classPath]);
		$res = @\file_put_contents($classPath, $contents);
		if(false === $res)
		{
			$message = 'Failed to write {className} at {classPath}';
			$context = ['{className}' => $className, '{classPath}' => $classPath];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		return 1;
	}
	
}
