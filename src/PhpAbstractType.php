<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

use Stringable;

/**
 * PhpAbstractType class file.
 * 
 * This represents a node in the type tree that can be represented in php.
 * 
 * @author Anastaszor
 */
abstract class PhpAbstractType implements Stringable
{
	
	/**
	 * Whether this type is nullable.
	 *
	 * @var boolean
	 */
	protected bool $_nullable;
	
	/**
	 * Builds a new PhpAbstractType with DI.
	 * 
	 * @param boolean $nullable
	 */
	public function __construct(bool $nullable = false)
	{
		$this->_nullable = $nullable;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->getNamespacedDocType();
	}
	
	/**
	 * Gets the full doc type without namespace of this type.
	 *
	 * @return string
	 */
	abstract public function getDocType() : string;
	
	/**
	 * Gets the namespace of this type.
	 *
	 * @return string
	 */
	abstract public function getNamespace() : string;
	
	/**
	 * Gets the name of this type.
	 *
	 * @return string
	 */
	abstract public function getName() : string;
	
	/**
	 * Gets all the namespaced classes to be put in use X; statements.
	 * 
	 * @return array<integer, class-string>
	 */
	abstract public function collectNamespacedClasses() : array;
	
	/**
	 * Gets the namespaced doc type class.
	 * 
	 * @return class-string
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function getNamespacedDocType() : string
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress LessSpecificReturnStatement */
		return \trim($this->getNamespace().'\\'.$this->getDocType(), '\\');
	}
	
	/**
	 * Gets the namespaced class name.
	 * 
	 * @return class-string
	 * @psalm-suppress MoreSpecificReturnType
	 */
	public function getNamespacedClassName() : string
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress LessSpecificReturnStatement */
		return \trim($this->getNamespace().'\\'.$this->getName(), '\\');
	}
	
	/**
	 * Sets the nullable value of this type.
	 *
	 * @param boolean $nullable
	 * @return PhpAbstractType
	 */
	public function setNullable(bool $nullable) : PhpAbstractType
	{
		$this->_nullable = $nullable;
		
		return $this;
	}
	
	/**
	 * Gets whether this type is nullable.
	 *
	 * @return boolean
	 */
	public function isNullable() : bool
	{
		return $this->_nullable;
	}
	
	/**
	 * Gets whether this type is an array type.
	 * 
	 * @return boolean
	 */
	public function isArray() : bool
	{
		return false;
	}
	
}
