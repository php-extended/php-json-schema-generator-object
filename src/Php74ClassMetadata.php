<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

use Stringable;

/**
 * Php74ClassMetadata class file.
 * 
 * This represents all the metadata needed to render a class field with
 * php7.4+ compatibility.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 */
class Php74ClassMetadata implements Stringable
{
	
	/**
	 * The namespaces.
	 * 
	 * @var NamespaceHolderInterface
	 */
	protected NamespaceHolderInterface $_namespaceHolder;
	
	/**
	 * The composer package name of this interface.
	 *
	 * @var string
	 */
	protected string $_composerPackageName;
	
	/**
	 * The use statements of this interface.
	 *
	 * @var array<integer, class-string>
	 */
	protected array $_uses = [];
	
	/**
	 * The class name of this class.
	 *
	 * @var string
	 */
	protected string $_className;
	
	/**
	 * The name of the interface related to this class.
	 * 
	 * @var string
	 */
	protected string $_interfaceName;
	
	/**
	 * The title of this interface.
	 *
	 * @var ?string
	 */
	protected ?string $_title = null;
	
	/**
	 * The description of this interface.
	 *
	 * @var ?string
	 */
	protected ?string $_description = null;
	
	/**
	 * The fields of this class.
	 * 
	 * @var array<string, Php74FieldMetadata>
	 */
	protected array $_fields = [];
	
	/**
	 * The methods of this interface. [name => metadata].
	 *
	 * @var array<string, Php74MethodMetadata>
	 */
	protected array $_methods = [];
	
	/**
	 * Builds a new Interface Metadata with its data.
	 * 
	 * @param NamespaceHolderInterface $namespaceHolder
	 * @param string $composerPackageName
	 * @param string $className
	 * @param string $interfaceName
	 * @param ?string $title
	 * @param ?string $description
	 */
	public function __construct(
		NamespaceHolderInterface $namespaceHolder,
		string $composerPackageName,
		string $className,
		string $interfaceName,
		?string $title,
		?string $description
	) {
		$this->_namespaceHolder = $namespaceHolder;
		$this->_composerPackageName = $composerPackageName;
		$this->_className = $className;
		$this->_interfaceName = $interfaceName;
		$this->_title = $title;
		$this->_description = $description;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the composer package name this interface lives in.
	 *
	 * @return string
	 */
	public function getComposerPackageName() : string
	{
		return $this->_composerPackageName;
	}
	
	/**
	 * Gets the namespace of this interface.
	 *
	 * @return string
	 */
	public function getNamespace() : string
	{
		return $this->_namespaceHolder->getClassNamespace();
	}
	
	/**
	 * Adds all the new classes to uses statements of this interface metadata.
	 *
	 * @param array<integer, class-string> $usedFqcns
	 * @return Php74ClassMetadata
	 */
	public function addUses(array $usedFqcns) : Php74ClassMetadata
	{
		foreach($usedFqcns as $usedFqcn)
		{
			$this->addUse($usedFqcn);
		}
		
		return $this;
	}
	
	/**
	 * Adds a new class to the uses statements of this interface metadata.
	 *
	 * @param class-string $usedFqcn
	 * @return Php74ClassMetadata
	 */
	public function addUse(string $usedFqcn) : Php74ClassMetadata
	{
		$usedFqcn = \trim($usedFqcn, '\\/');
		$usedNamespace = \str_replace('/', '\\', \dirname(\str_replace('\\', '/', $usedFqcn)));
		
		if($this->_namespaceHolder->getClassNamespace() !== $usedNamespace)
		{
			/** @phpstan-ignore-next-line */ /** @psalm-suppress PropertyTypeCoercion */
			$this->_uses[] = $usedFqcn;
		}
		
		return $this;
	}
	
	/**
	 * Gets the classes used in this interface.
	 *
	 * @return array<integer, class-string>
	 */
	public function getUses() : array
	{
		$uses = $this->_uses;
		
		/** @var Php74FieldMetadata $field */
		foreach($this->_fields as $field)
		{
			foreach($field->collectNamespacedClasses() as $className)
			{
				$usedNamespace = \str_replace('/', '\\', \dirname(\str_replace('\\', '/', $className)));
				
				if($this->_namespaceHolder->getClassNamespace() !== $usedNamespace)
				{
					$uses[] = $className;
				}
			}
		}
		
		/** @var Php74MethodMetadata $method */
		foreach($this->_methods as $method)
		{
			foreach($method->collectNamespacedClasses() as $className)
			{
				$usedNamespace = \str_replace('/', '\\', \dirname(\str_replace('\\', '/', $className)));
				
				if($this->_namespaceHolder->getClassNamespace() !== $usedNamespace)
				{
					$uses[] = $className;
				}
			}
		}
		
		$uses = \array_unique($uses);
		$uses = \array_values($uses);
		\natcasesort($uses);
		
		return $uses;
	}
	
	/**
	 * Gets the class name of this interface.
	 *
	 * @return string
	 */
	public function getClassName() : string
	{
		return $this->_className;
	}
	
	/**
	 * Gets the interface name of this interface.
	 * 
	 * @return string
	 */
	public function getInterfaceName() : string
	{
		return $this->_interfaceName;
	}
	
	/**
	 * Gets the title of this interface.
	 *
	 * @return ?string
	 */
	public function getTitle() : ?string
	{
		return $this->_title;
	}
	
	/**
	 * Gets the description of this interface.
	 *
	 * @return ?string
	 */
	public function getDescription() : ?string
	{
		return $this->_description;
	}
	
	/**
	 * Adds a field with the given name to this class.
	 * 
	 * @param ?Php74FieldMetadata $field
	 * @return Php74ClassMetadata
	 */
	public function addField(?Php74FieldMetadata $field) : Php74ClassMetadata
	{
		if(null !== $field)
		{
			$this->_fields[$field->getName()] = $field;
		}
		
		return $this;
	}
	
	/**
	 * Gets all the non public fields.
	 * 
	 * @return array<integer, Php74FieldMetadata>
	 */
	public function getNonPublicFields() : array
	{
		$result = [];
		
		/** @var Php74FieldMetadata $field */
		foreach($this->_fields as $field)
		{
			if(!$field->isPublic())
			{
				$result[] = $field;
			}
		}
		
		return $result;
	}
	
	/**
	 * Counts the non public fields that exists in this class.
	 * 
	 * @return integer
	 */
	public function countNonPublicFields() : int
	{
		$count = 0;
		
		/** @var Php74FieldMetadata $field */
		foreach($this->_fields as $field)
		{
			if(!$field->isPublic())
			{
				$count++;
			}
		}
		
		return $count;
	}
	
	/**
	 * Gets whether this class has non public fields.
	 * 
	 * @return boolean
	 */
	public function hasNonPublicFields() : bool
	{
		/** @var Php74FieldMetadata $field */
		foreach($this->_fields as $field)
		{
			if(!$field->isPublic())
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Gets the fields that are or are not public.
	 * 
	 * @return array<integer, Php74FieldMetadata>
	 */
	public function getFields() : array
	{
		$fields = \array_values($this->_fields);
		
		\usort($fields, function(Php74FieldMetadata $fm1, Php74FieldMetadata $fm2)
		{
			if($fm1->isPublic() !== $fm2->isPublic())
			{
				if($fm1->isPublic())
				{
					return -1;
				}
				if($fm2->isPublic())
				{
					return 1;
				}
			}
			
			return 0;
		});
		
		return $fields;
	}
	
	/**
	 * Gets the total count of fields.
	 * 
	 * @return integer the number of fields
	 */
	public function getFieldCount() : int
	{
		return \count($this->_fields);
	}
	
	/**
	 * Gets whether this class needs a constructor. A Class needs a constructor
	 * if there exists a field that is not public nor nullable (i.e. must be
	 * set before usage).
	 * 
	 * @return bool
	 */
	public function needsConstructor() : bool
	{
		/** @var Php74FieldMetadata $field */
		foreach($this->_fields as $field)
		{
			if(!$field->isPublic() && !$field->isNullable())
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Adds a method with the given name to this class.
	 *
	 * @param ?Php74MethodMetadata $method
	 * @return Php74ClassMetadata
	 */
	public function addMethod(?Php74MethodMetadata $method) : Php74ClassMetadata
	{
		if(null !== $method)
		{
			$this->_methods[$method->getName()] = $method;
		}
		
		return $this;
	}
	
	/**
	 * Gets the methods for this class.
	 *
	 * @return array<integer, Php74MethodMetadata>
	 */
	public function getMethods() : array
	{
		return \array_values($this->_methods);
	}
	
	/**
	 * Gets the count of methods for this class.
	 *
	 * @return integer
	 */
	public function getMethodCount() : int
	{
		return \count($this->_methods) + 1 /* toString */ + (int) $this->needsConstructor() /* __construct */;
	}
	
	/**
	 * Gets the public item count.
	 * 
	 * @return integer
	 */
	public function getPublicCount() : int
	{
		$publicCount = 0;
		
		/** @var Php74FieldMetadata $field */
		foreach($this->_fields as $field)
		{
			$publicCount += (int) $field->isPublic();
		}
		
		return $publicCount + $this->getMethodCount();
	}
	
	/**
	 * Gets whether this class has short variable names.
	 * 
	 * @return boolean
	 */
	public function hasShortFieldNames() : bool
	{
		/** @var Php74FieldMetadata $field */
		foreach($this->_fields as $field)
		{
			if(\strlen($field->getName()) < 3)
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Gets whether this class has long variable names.
	 * 
	 * @return bool
	 */
	public function hasLongFieldNames() : bool
	{
		/** @var Php74FieldMetadata $field */
		foreach($this->_fields as $field)
		{
			if(\strlen($field->getName()) > 40)
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Gets the complexity of the generated class.
	 * 1 (__construct) + 1 (__toString) + 1/setter + 1/getter.
	 * 
	 * @return integer
	 */
	public function getComplexity() : int
	{
		return 1 + 1 + 2 * $this->getFieldCount();
	}
	
	/**
	 * Gets an estimation of the line count to render the interior of the class.
	 * 
	 * @return integer
	 */
	public function getLineCount() : int
	{
		$fieldsLineCount = 0;
		
		/** @var Php74FieldMetadata $field */
		foreach($this->_fields as $field)
		{
			$fieldsLineCount += 1 + $field->getLineCount();
		}
		
		$ctorLineCount = 0;
		
		if($this->needsConstructor())
		{
			$ctorLineCount += 1 // /*
							+ 2 // title
							+ $this->countNonPublicFields()
							+ ((int) ($this->countNonPublicFields() >= 10)) // @Suppress
							+ 1 // */
							+ 1 // public function
							+ 1 // {
							+ $this->countNonPublicFields()
							+ 1 // }
							+ 1; // (blank)
		}
		
		$methodsLineCount = 0;
		
		/** @var Php74MethodMetadata $method */
		foreach($this->_methods as $method)
		{
			$methodsLineCount += 1 + $method->getLineCount();
		}
		
		return 1 // initial space
			 + $fieldsLineCount
			 + $ctorLineCount
			 + 10 // __toString
			 + $methodsLineCount;
	}
	
	/**
	 * Renders the interface into a php code file.
	 *
	 * @return string
	 */
	public function render() : string
	{
		\ob_start();
		
		require __DIR__.'/../stub/class.74.stub.php';
		
		return (string) \ob_get_clean();
	}
	
}
