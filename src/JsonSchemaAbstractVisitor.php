<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

use LogicException;

/**
 * JsonschemaAbstractVisitor class file.
 * 
 * @author Anastaszor
 * @template T of null|integer|float|string|array|object
 * @extends JsonSchemaVisitorNoop<T>
 */
abstract class JsonSchemaAbstractVisitor extends JsonSchemaVisitorNoop
{
	use PhpNamingTrait;
	
	/**
	 * The current key value.
	 *
	 * @var ?string
	 */
	protected ?string $_key = null;
	
	/**
	 * The current namespace holder value.
	 *
	 * @var ?NamespaceHolderInterface
	 */
	protected ?NamespaceHolderInterface $_namespace = null;
	
	/**
	 * The provider to resolve recursively.
	 *
	 * @var JsonSchemaResolver
	 */
	protected JsonSchemaResolver $_resolver;
	
	/**
	 * Builds a new JsonSchemaTypeVisitor with DI.
	 *
	 * @param JsonSchemaResolver $resolver
	 */
	public function __construct(JsonSchemaResolver $resolver)
	{
		$this->_resolver = $resolver;
	}
	
	/**
	 * Handles all of the components for generating a new type.
	 *
	 * @param string $key
	 * @param JsonSchemaInterface $schema
	 * @param NamespaceHolderInterface $namespace
	 * @return T
	 * @throws LogicException
	 */
	public function handle(string $key, JsonSchemaInterface $schema, NamespaceHolderInterface $namespace)
	{
		$previousKey = $this->_key;
		$previousNamespace = $this->_namespace;
		$this->_key = $key;
		$this->_namespace = $namespace;
		
		$result = $this->visit($schema);
		
		$this->_key = $previousKey;
		$this->_namespace = $previousNamespace;
		
		return $result;
	}
	
}
