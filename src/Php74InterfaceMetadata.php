<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

use Stringable;

/**
 * Php74InterfaceMetadata class file.
 * 
 * This represents all the metadata needed to render an interface file with
 * php7.4+ compatibility.
 * 
 * @author Anastaszor
 */
class Php74InterfaceMetadata implements Stringable
{
	
	/**
	 * The namespace holder.
	 * 
	 * @var NamespaceHolderInterface
	 */
	protected NamespaceHolderInterface $_namespaceHolder;
	
	/**
	 * The composer package name of this interface.
	 * 
	 * @var string
	 */
	protected string $_composerPackageName;
	
	/**
	 * The use statements of this interface.
	 * 
	 * @var array<integer, class-string>
	 * @psalm-suppress PropertyTypeCoercion
	 */
	protected array $_uses = ['Stringable'];
	
	/**
	 * The class name of this interface.
	 * 
	 * @var string
	 */
	protected string $_className;
	
	/**
	 * The title of this interface.
	 * 
	 * @var ?string
	 */
	protected ?string $_title;
	
	/**
	 * The description of this interface.
	 * 
	 * @var ?string
	 */
	protected ?string $_description = null;
	
	/**
	 * The methods of this interface. [name => metadata].
	 * 
	 * @var array<string, Php74MethodMetadata>
	 */
	protected array $_methods = [];
	
	/**
	 * Builds a new Interface Metadata with its data.
	 * 
	 * @param NamespaceHolderInterface $namespaceHolder
	 * @param string $composerPackageName
	 * @param string $className
	 * @param ?string $title
	 * @param ?string $description
	 */
	public function __construct(
		NamespaceHolderInterface $namespaceHolder,
		string $composerPackageName,
		string $className,
		?string $title,
		?string $description
	) {
		$this->_namespaceHolder = $namespaceHolder;
		$this->_composerPackageName = $composerPackageName;
		$this->_className = $className;
		$this->_title = $title;
		$this->_description = $description;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the composer package name this interface lives in.
	 * 
	 * @return string
	 */
	public function getComposerPackageName() : string
	{
		return $this->_composerPackageName;
	}
	
	/**
	 * Gets the namespace of this interface.
	 * 
	 * @return string
	 */
	public function getNamespace() : string
	{
		return $this->_namespaceHolder->getInterfaceNamespace();
	}
	
	/**
	 * Adds all the new classes to uses statements of this interface metadata.
	 * 
	 * @param array<integer, class-string> $usedFqcns
	 * @return Php74InterfaceMetadata
	 */
	public function addUses(array $usedFqcns) : Php74InterfaceMetadata
	{
		foreach($usedFqcns as $usedFqcn)
		{
			$this->addUse($usedFqcn);
		}
		
		return $this;
	}
	
	/**
	 * Adds a new class to the uses statements of this interface metadata.
	 * 
	 * @param class-string $usedFqcn
	 * @return Php74InterfaceMetadata
	 */
	public function addUse(string $usedFqcn) : Php74InterfaceMetadata
	{
		$usedFqcn = \trim($usedFqcn, '\\/');
		$usedNamespace = \str_replace('/', '\\', \dirname(\str_replace('\\', '/', $usedFqcn)));
		
		if($this->_namespaceHolder->getInterfaceNamespace() !== $usedNamespace)
		{
			/** @phpstan-ignore-next-line */ /** @psalm-suppress PropertyTypeCoercion */
			$this->_uses[] = $usedFqcn;
		}
		
		return $this;
	}
	
	/**
	 * Gets the classes used in this interface.
	 * 
	 * @return array<integer, class-string>
	 */
	public function getUses() : array
	{
		$uses = $this->_uses;
		
		/** @var Php74MethodMetadata $method */
		foreach($this->_methods as $method)
		{
			foreach($method->collectNamespacedClasses() as $className)
			{
				$usedNamespace = \str_replace('/', '\\', \dirname(\str_replace('\\', '/', $className)));
				
				if($this->_namespaceHolder->getInterfaceNamespace() !== $usedNamespace)
				{
					$uses[] = $className;
				}
			}
		}
		
		$uses = \array_unique($uses);
		$uses = \array_values($uses);
		\natcasesort($uses);
		
		return $uses;
	}
	
	/**
	 * Gets the class name of this interface.
	 * 
	 * @return string
	 */
	public function getClassName() : string
	{
		return $this->_className;
	}
	
	/**
	 * Gets the namespaced class name for this interface.
	 * 
	 * @return string
	 */
	public function getNamespacedClassName() : string
	{
		return \trim($this->getNamespace().'\\'.$this->getClassName(), '\\');
	}
	
	/**
	 * Gets the title of this interface.
	 * 
	 * @return ?string
	 */
	public function getTitle() : ?string
	{
		return $this->_title;
	}
	
	/**
	 * Gets the description of this interface.
	 * 
	 * @return ?string
	 */
	public function getDescription() : ?string
	{
		return $this->_description;
	}
	
	/**
	 * Adds a method with the given name to this interface.
	 * 
	 * @param ?Php74MethodMetadata $method
	 * @return Php74InterfaceMetadata
	 */
	public function addMethod(?Php74MethodMetadata $method) : Php74InterfaceMetadata
	{
		if(null !== $method)
		{
			$this->_methods[$method->getName()] = $method;
		}
		
		return $this;
	}
	
	/**
	 * Gets the methods for this interface.
	 * 
	 * @return array<integer, Php74MethodMetadata>
	 */
	public function getMethods() : array
	{
		return \array_values($this->_methods);
	}
	
	/**
	 * Gets the count of methods for this interface.
	 * 
	 * @return integer
	 */
	public function getMethodCount() : int
	{
		return \count($this->_methods);
	}
	
	/**
	 * Renders the interface into a php code file.
	 * 
	 * @return string
	 */
	public function render() : string
	{
		\ob_start();
		
		require __DIR__.'/../stub/interface.74.stub.php';
		
		return (string) \ob_get_clean();
	}
	
}
