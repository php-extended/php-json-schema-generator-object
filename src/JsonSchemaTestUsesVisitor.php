<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

use DateTimeImmutable;
use InvalidArgumentException;
use LogicException;
use PhpExtended\Email\EmailAddressParser;
use PhpExtended\Email\MailboxListParser;
use PhpExtended\HttpClient\AcceptLanguageChainParser;
use PhpExtended\Uri\UriParser;
use PhpExtended\Uuid\UuidParser;
use PhpExtended\Version\VersionParser;
use RuntimeException;

/**
 * JsonSchemaTestUsesVisitor class file.
 * 
 * This visitor generates the array of use statements to be incorporated into
 * a test file.
 * 
 * @author Anastaszor
 * @extends JsonSchemaAbstractVisitor<array<integer, class-string>>
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class JsonSchemaTestUsesVisitor extends JsonSchemaAbstractVisitor
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaVisitorNoop::visitSchemaRaw()
	 */
	public function visitSchemaRaw(JsonSchemaInterface $schema) : array
	{
		return [];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaVisitorNoop::visitSchemaArray()
	 * @throws InvalidArgumentException
	 * @throws LogicException
	 * @throws RuntimeException
	 */
	public function visitSchemaArray(JsonSchemaArrayInterface $schema) : array
	{
		$items = $schema->getItems();
		if(null === $items)
		{
			throw new LogicException('NOT IMPLEMENTED NOT A LIST');
		}
		
		if(null === $this->_key || null === $this->_namespace)
		{
			throw new LogicException('Failed to get a proper key or namespace for this schema, maybe you have used visit() instead of handle() ?');
		}
		
		$newKey = $this->_key;
		$newNamespace = $this->_resolver->resolveNamespace($items);
		$newSchema = $this->_resolver->resolveSchema($items);
		
		return $this->handle($newKey, $newSchema, $newNamespace);
	}

	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaVisitorNoop::visitSchemaBoolean()
	 */
	public function visitSchemaBoolean(JsonSchemaBooleanInterface $schema) : array
	{
		return [];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaVisitorNoop::visitSchemaFloat()
	 */
	public function visitSchemaFloat(JsonSchemaFloatInterface $schema) : array
	{
		return [];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaVisitorNoop::visitSchemaInteger()
	 */
	public function visitSchemaInteger(JsonSchemaIntegerInterface $schema) : array
	{
		return [];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaVisitorNoop::visitSchemaObject()
	 * @throws LogicException
	 * @throws RuntimeException
	 */
	public function visitSchemaObject(JsonSchemaObjectInterface $schema) : array
	{
		if(null === $this->_key || null === $this->_namespace)
		{
			throw new LogicException('Failed to get a proper key or namespace for this schema, maybe you have used visit() instead of handle() ?');
		}
		
		$items = $schema->getAdditionalProperties();
		if(empty($schema->getProperties()) && null !== $items)
		{
			$newKey = $this->_key;
			$newNamespace = $this->_resolver->resolveNamespace($items);
			$newSchema = $this->_resolver->resolveSchema($items);
			
			return $this->handle($newKey, $newSchema, $newNamespace);
		}
		
		$ref = $schema->getRef();
		$classKey = null === $ref ? $this->_key : \basename(\rawurldecode($ref->__toString()));
		
		return [$this->getNamespacedClassNameFromKey($this->_namespace, $classKey)];
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\JsonSchemaVisitorNoop::visitSchemaString()
	 */
	public function visitSchemaString(JsonSchemaStringInterface $schema) : array
	{
		switch($schema->getFormat())
		{
			case 'accept-language-chain':
				return [AcceptLanguageChainParser::class];
				
			case 'date':
			case 'date-time':
			case 'time':
				return [DateTimeImmutable::class];
				
			case 'email':
				return [EmailAddressParser::class];
				
			case 'mailbox-list':
				return [MailboxListParser::class];
				
			case 'uri':
				return [UriParser::class];
				
			case 'uuid':
				return [UuidParser::class];
				
			case 'version':
				return [VersionParser::class];
				
			case '':
			case null:
				return [];
				
			default:
				/** @phpstan-ignore-next-line */
				throw new LogicException('NOT IMPLEMENTED : '.\get_class($schema).' - '.($schema->getFormat() ?? '(no format)'));
		}
	}
	
}
