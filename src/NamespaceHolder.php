<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

/**
 * NamespaceHolder class file.
 * 
 * This class holds all of the needed namespace values.
 * 
 * @author Anastaszor
 */
class NamespaceHolder implements NamespaceHolderInterface
{
	
	/**
	 * The interface namespace.
	 * 
	 * @var string
	 */
	protected string $_interfaceNamespace;
	
	/**
	 * The class namespace.
	 * 
	 * @var string
	 */
	protected string $_classNamespace;
	
	/**
	 * The test namespace.
	 * 
	 * @var string
	 */
	protected string $_testNamespace;
	
	/**
	 * Builds a new namespace holder with its namespace values.
	 * 
	 * @param string $interfaceNamespace
	 * @param string $classNamespace
	 * @param string $testNamespace
	 */
	public function __construct(string $interfaceNamespace, string $classNamespace, string $testNamespace)
	{
		$this->_interfaceNamespace = $interfaceNamespace;
		$this->_classNamespace = $classNamespace;
		$this->_testNamespace = $testNamespace;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\NamespaceHolderInterface::getInterfaceNamespace()
	 */
	public function getInterfaceNamespace() : string
	{
		return $this->_interfaceNamespace;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\NamespaceHolderInterface::getClassNamespace()
	 */
	public function getClassNamespace() : string
	{
		return $this->_classNamespace;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\NamespaceHolderInterface::getTestNamespace()
	 */
	public function getTestNamespace() : string
	{
		return $this->_testNamespace;
	}
	
}
