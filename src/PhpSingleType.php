<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

/**
 * PhpSingleType class file.
 * 
 * This represents a type representing a single entity type, neither union,
 * neither intersection type, neither composite type like array, but a scalar
 * primitive type, or a single class type.
 * 
 * @author Anastaszor
 */
class PhpSingleType extends PhpAbstractType
{
	
	/**
	 * The doc type of this type.
	 * 
	 * @var string
	 */
	protected string $_docType;
	
	/**
	 * The namespace of this type.
	 * 
	 * @var string
	 */
	protected string $_namespace;
	
	/**
	 * The name of this type.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * Builds a new PhpSingleType with its doc and php type value.
	 * 
	 * @param string $docType
	 * @param string $phpFqcn
	 * @param boolean $nullable
	 */
	public function __construct(string $docType, string $phpFqcn, bool $nullable = false)
	{
		parent::__construct($nullable);
		$pos = (int) \strrpos($phpFqcn, '\\');
		$this->_namespace = \trim((string) \substr($phpFqcn, 0, $pos), '\\');
		$this->_name = \trim((string) \substr($phpFqcn, $pos), '\\');
		$pos = (int) \strrpos($docType, '\\');
		$this->_docType = \trim((string) \substr($docType, $pos), '\\');
		$this->_nullable = $nullable;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\PhpAbstractType::getDocType()
	 */
	public function getDocType() : string
	{
		return $this->_docType;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\PhpAbstractType::getNamespace()
	 */
	public function getNamespace() : string
	{
		return $this->_namespace;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\PhpAbstractType::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\JsonSchema\PhpAbstractType::collectNamespacedClasses()
	 */
	public function collectNamespacedClasses() : array
	{
		// if primitive, no need to import it
		if(\in_array($this->getName(), [
			'null',
			'false', 'true', 'bool', 'boolean',
			'int', 'integer',
			'float', 'double',
			'string',
			'array', 'iterable',
			'object',
			'resource',
			'never',
			'void',
			'self', 'parent', 'static',
			'callable',
		], true))
		{
			return [];
		}
		
		return [$this->getNamespacedClassName()];
	}
	
}
