<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

use Stringable;

/**
 * Php74FieldMetadata class file.
 * 
 * This represents all the metadata needed to render a field with php7.4+
 * compatibility.
 * 
 * @author Anastaszor
 */
class Php74FieldMetadata implements Stringable
{
	
	/**
	 * Whether the field should be public.
	 * 
	 * @var boolean
	 */
	protected bool $_public = false;
	
	/**
	 * The title of the field.
	 *
	 * @var ?string
	 */
	protected ?string $_title;
	
	/**
	 * The description of the field.
	 *
	 * @var string
	 */
	protected ?string $_description;
	
	/**
	 * The name of the field.
	 *
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * The type of the field.
	 * 
	 * @var PhpAbstractType
	 */
	protected PhpAbstractType $_type;
	
	/**
	 * Builds a new Php74FieldMetadata.
	 * 
	 * @param bool $public
	 * @param ?string $title
	 * @param ?string $description
	 * @param string $name
	 * @param PhpAbstractType $type
	 */
	public function __construct(
		bool $public,
		?string $title,
		?string $description,
		string $name,
		PhpAbstractType $type
	) {
		$this->_public = $public;
		$this->_title = $title;
		$this->_description = $description;
		$this->_name = $name;
		$this->_type = $type;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets whether this field is public.
	 * 
	 * @return bool
	 */
	public function isPublic() : bool
	{
		return $this->_public;
	}
	
	/**
	 * Gets the title of this field.
	 * 
	 * @return ?string
	 */
	public function getTitle() : ?string
	{
		return $this->_title;
	}
	
	/**
	 * Gets the description of this field.
	 * 
	 * @return ?string
	 */
	public function getDescription() : ?string
	{
		return $this->_description;
	}
	
	/**
	 * Gets the name of this field.
	 * 
	 * @return string
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * Gets the doc type of this field.
	 * 
	 * @return string
	 */
	public function getDocType() : string
	{
		return $this->_type->getDocType();
	}
	
	/**
	 * Gets the php type of this field.
	 * 
	 * @return string
	 */
	public function getPhpType() : string
	{
		return $this->_type->getName();
	}
	
	/**
	 * Gets whether this field is nullable.
	 * 
	 * @return boolean
	 */
	public function isNullable() : bool
	{
		return $this->_type->isNullable();
	}
	
	/**
	 * Gets wshether this field is an array.
	 * 
	 * @return boolean
	 */
	public function isArray() : bool
	{
		return $this->_type->isArray();
	}
	
	/**
	 * Collects all the namespaced classes from this field.
	 * 
	 * @return array<integer, class-string>
	 */
	public function collectNamespacedClasses() : array
	{
		return $this->_type->collectNamespacedClasses();
	}
	
	/**
	 * Gets an estimation of the number of lines of this field, once rendered.
	 * 
	 * @return integer
	 */
	public function getLineCount() : int
	{
		$descriptions = [];
		
		foreach(\explode("\n", (string) $this->_description) as $descriptionLine)
		{
			$descriptions = \array_merge($descriptions, \explode("\n", \wordwrap($descriptionLine, 76, "\n", false)));
		}
		
		return 1 // /*
			 + (null === $this->_title || '' === $this->_title ? 0 : 2)
			 + \count($descriptions)
			 + 1 // *
			 + 1 // @var
			 + 1 // */
			 + 1 // public $_var
			 + 1; // (blank)
	}
	
}
