<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

use Stringable;

/**
 * Php74TestMetadata class file.
 * 
 * This represents all the metadata needed to render a test class file with
 * php7.4+ compatibility.
 * 
 * @author Anastaszor
 */
class Php74TestMetadata implements Stringable
{
	
	/**
	 * The namespaces.
	 * 
	 * @var NamespaceHolderInterface
	 */
	protected NamespaceHolderInterface $_namespaceHolder;
	
	/**
	 * The composer package name of this interface.
	 *
	 * @var string
	 */
	protected string $_composerPackageName;
	
	/**
	 * The use statements of this interface.
	 *
	 * @var array<integer, class-string>
	 * @psalm-suppress PropertyTypeCoercion
	 */
	protected array $_uses = ['PHPUnit\\Framework\\TestCase'];
	
	/**
	 * The class name of this test.
	 * 
	 * @var string
	 */
	protected string $_testClassName;
	
	/**
	 * The class name of this class.
	 *
	 * @var string
	 */
	protected string $_className;
	
	/**
	 * The title of this interface.
	 *
	 * @var ?string
	 */
	protected ?string $_title = null;
	
	/**
	 * The description of this interface.
	 *
	 * @var ?string
	 */
	protected ?string $_description = null;
	
	/**
	 * The compounds of this test.
	 * 
	 * @var array<string, Php74CompoundMetadata>
	 */
	protected array $_compounds = [];
	
	/**
	 * Builds a new Test Metadata with its data.
	 * 
	 * @param NamespaceHolderInterface $namespaceHolder
	 * @param string $composerPackageName
	 * @param string $testClassName
	 * @param string $className
	 * @param ?string $title
	 * @param ?string $description
	 */
	public function __construct(
		NamespaceHolderInterface $namespaceHolder,
		string $composerPackageName,
		string $testClassName,
		string $className,
		?string $title,
		?string $description
	) {
		$this->_namespaceHolder = $namespaceHolder;
		$this->_composerPackageName = $composerPackageName;
		$this->_testClassName = $testClassName;
		$this->_className = $className;
		$this->_title = $title;
		$this->_description = $description;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the composer package name this interface lives in.
	 *
	 * @return string
	 */
	public function getComposerPackageName() : string
	{
		return $this->_composerPackageName;
	}
	
	/**
	 * Adds all the new classes to uses statements of this interface metadata.
	 *
	 * @param array<integer, class-string> $usedFqcns
	 * @return Php74TestMetadata
	 */
	public function addUses(array $usedFqcns) : Php74TestMetadata
	{
		foreach($usedFqcns as $usedFqcn)
		{
			$this->addUse($usedFqcn);
		}
		
		return $this;
	}
	
	/**
	 * Adds a new class to the uses statements of this interface metadata.
	 *
	 * @param class-string $usedFqcn
	 * @return Php74TestMetadata
	 */
	public function addUse(string $usedFqcn) : Php74TestMetadata
	{
		$usedFqcn = \trim($usedFqcn, '\\/');
		$usedNamespace = \str_replace('/', '\\', \dirname(\str_replace('\\', '/', $usedFqcn)));
		
		if($this->_namespaceHolder->getTestNamespace() !== $usedNamespace)
		{
			/** @phpstan-ignore-next-line */ /** @psalm-suppress PropertyTypeCoercion */
			$this->_uses[] = $usedFqcn;
		}
		
		return $this;
	}
	
	/**
	 * Gets the classes used in this interface.
	 *
	 * @return array<integer, class-string>
	 */
	public function getUses() : array
	{
		$uses = $this->_uses;
		
		/** @var Php74CompoundMetadata $compound */
		foreach($this->_compounds as $compound)
		{
			foreach($compound->getUses() as $className)
			{
				$usedNamespace = \str_replace('/', '\\', \dirname(\str_replace('\\', '/', $className)));
				
				if($this->_namespaceHolder->getTestNamespace() !== $usedNamespace)
				{
					$uses[] = $className;
				}
			}
		}
		
		$uses = \array_unique($uses);
		$uses = \array_values($uses);
		\natcasesort($uses);
		
		return $uses;
	}
	
	/**
	 * Gets the namespace of the test.
	 * 
	 * @return string
	 */
	public function getTestNamespace() : string
	{
		return $this->_namespaceHolder->getTestNamespace();
	}
	
	/**
	 * Gets the class name of the test.
	 * 
	 * @return string
	 */
	public function getTestClassName() : string
	{
		return $this->_testClassName;
	}
	
	/**
	 * Gets the class namespace of the tested class by this test.
	 * 
	 * @return string
	 */
	public function getClassNamespace() : string
	{
		return $this->_namespaceHolder->getClassNamespace();
	}
	
	/**
	 * Gets the class name of the tested class by this test.
	 *
	 * @return string
	 */
	public function getClassName() : string
	{
		return $this->_className;
	}
	
	/**
	 * Gets the title of this interface.
	 *
	 * @return ?string
	 */
	public function getTitle() : ?string
	{
		return $this->_title;
	}
	
	/**
	 * Gets the description of this interface.
	 *
	 * @return ?string
	 */
	public function getDescription() : ?string
	{
		return $this->_description;
	}
	
	/**
	 * Adds a new compound to this test.
	 * 
	 * @param ?Php74CompoundMetadata $compound
	 * @return Php74TestMetadata
	 */
	public function addCompound(?Php74CompoundMetadata $compound) : Php74TestMetadata
	{
		if(null !== $compound)
		{
			$this->_compounds[$compound->getFieldName()] = $compound;
		}
		
		return $this;
	}
	
	/**
	 * Gets the compounds.
	 * 
	 * @return array<integer, Php74CompoundMetadata>
	 */
	public function getCompounds() : array
	{
		return \array_values($this->_compounds);
	}
	
	/**
	 * Renders the interface into a php code file.
	 *
	 * @return string
	 */
	public function render() : string
	{
		\ob_start();
		
		require __DIR__.'/../stub/test.74.stub.php';
		
		return (string) \ob_get_clean();
	}
	
}
