<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-json-schema-generator-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\JsonSchema;

use InvalidArgumentException;
use LogicException;
use RuntimeException;
use Stringable;

/**
 * JsonSchemaResolver class file.
 * 
 * This class is a resolver based on repositories to handle missing data.
 * 
 * @author Anastaszor
 * @todo make it a factory ?
 */
class JsonSchemaResolver implements Stringable
{
	
	/**
	 * The json schema repository.
	 *
	 * @var JsonSchemaRepository
	 */
	protected JsonSchemaRepository $_jsonSchemaRepository;
	
	/**
	 * The namespace repository.
	 *
	 * @var NamespaceRepository
	 */
	protected NamespaceRepository $_namespaceRepository;
	
	/**
	 * The default schema of the structure.
	 *
	 * @var ?JsonSchemaInterface
	 */
	protected ?JsonSchemaInterface $_defaultSchema = null;
	
	/**
	 * The default namespace holder.
	 *
	 * @var ?NamespaceHolderInterface
	 */
	protected ?NamespaceHolderInterface $_defaultNamespace = null;
	
	/**
	 * Builds a new JsonSchemaResolver with DI.
	 * 
	 * @param JsonSchemaRepository $jsonSchemaRepository
	 * @param NamespaceRepository $namespaceRepository
	 */
	public function __construct(JsonSchemaRepository $jsonSchemaRepository, NamespaceRepository $namespaceRepository)
	{
		$this->_jsonSchemaRepository = $jsonSchemaRepository;
		$this->_namespaceRepository = $namespaceRepository;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the default values for a given schema and namespace.
	 * 
	 * @param JsonSchemaInterface $defaultSchema
	 * @param NamespaceHolderInterface $defaultNamespace
	 * @return JsonSchemaResolver
	 * @throws LogicException
	 */
	public function setDefaults(JsonSchemaInterface $defaultSchema, NamespaceHolderInterface $defaultNamespace) : JsonSchemaResolver
	{
		$id = $defaultSchema->getId();
		if(null === $id)
		{
			throw new LogicException('The given root schema must have an uri id, none given.');
		}
		$id = $id->withFragment('');
		
		$this->_defaultSchema = $defaultSchema;
		$this->_defaultNamespace = $defaultNamespace;
		
		$this->_jsonSchemaRepository->save($id, $defaultSchema);
		$this->_namespaceRepository->save($id, $defaultNamespace);
		
		return $this;
	}
	
	/**
	 * Gets the default schema, throws an exception if defaults are not set.
	 * 
	 * @return JsonSchemaInterface
	 * @throws LogicException
	 */
	public function getDefaultSchema() : JsonSchemaInterface
	{
		if(null === $this->_defaultSchema)
		{
			throw new LogicException('Impossible to get the default schema as it was not set as default.');
		}
		
		return $this->_defaultSchema;
	}
	
	/**
	 * Gets the default namespace, throws an exception if defaults are not set.
	 * 
	 * @return NamespaceHolderInterface
	 * @throws LogicException
	 */
	public function getDefaultNamespace() : NamespaceHolderInterface
	{
		if(null === $this->_defaultNamespace)
		{
			throw new LogicException('Impossible to get the default namespace as it was not set as default.');
		}
		
		return $this->_defaultNamespace;
	}
	
	/**
	 * Gets the resolved schema from the ref of the given schema, taken from
	 * the root node of the schema. If the schema tree cannot resolve the path,
	 * then the given schema is returned as auto resolved.
	 *
	 * @param JsonSchemaInterface $schema
	 * @return JsonSchemaInterface
	 * @throws LogicException
	 * @throws RuntimeException
	 */
	public function resolveSchema(JsonSchemaInterface $schema) : JsonSchemaInterface
	{
		$ref = $schema->getRef();
		if(null === $ref)
		{
			return $schema;
		}
		
		$path = \rawurldecode($ref->getFragment());
		$tree = $this->getDefaultSchema();
		if(!empty($ref->getHost()))
		{
			$tree = $this->_jsonSchemaRepository->findJsonSchema($ref);
		}
		
		if(empty($path))
		{
			return $tree;
		}
		
		$resolved = $tree->getFromPath($path);
		if(null === $resolved)
		{
			$message = 'Failed to resolve the schema for $ref = {ref} in schema $id = {id}';
			$context = ['{ref}' => '#'.$path, '{id}' => $tree->getId()];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		return $resolved->mergeWith($schema);
	}
	
	/**
	 * Gets the resolved namespace holder from the ref of the given schema.
	 * If no ref is given, the root namespace holder is used, otherwise the
	 * namespace holder is retrieved from the namespace repository.
	 *
	 * @param JsonSchemaInterface $schema
	 * @return NamespaceHolderInterface
	 * @throws InvalidArgumentException
	 * @throws LogicException
	 * @throws RuntimeException
	 */
	public function resolveNamespace(JsonSchemaInterface $schema) : NamespaceHolderInterface
	{
		$ref = $schema->getRef();
		if(null === $ref)
		{
			return $this->getDefaultNamespace();
		}
		
		if(empty($ref->getHost()))
		{
			return $this->getDefaultNamespace();
		}
		
		return $this->_namespaceRepository->findNamespaces($ref);
	}
	
}
