<?php declare(strict_types=1);

/*
 * Test script for JsonSchemaFileGeneratorPhp74
 * 
 * Usage :
 * php generate.php "/path/to/json-file.php"
 * 
 * @author Anastaszor
 */

use PhpExtended\JsonSchema\JsonSchemaProvider;
use PhpExtended\JsonSchema\JsonSchemaFileGeneratorPhp74;
use PhpExtended\JsonSchema\JsonSchemaFileWriter;
use PhpExtended\Logger\BasicConsoleLogger;
use PhpExtended\JsonSchema\NamespaceHolder;

global $argv;

if(!isset($argv[1]))
{
	throw new InvalidArgumentException('The first argument should be the path to the json-schema file to generate data from');
}

$filePath = $argv[1];
$filePath = realpath($filePath);
if($filePath === false)
{
	throw new InvalidArgumentException('The provided path to the json-schema file is not valid');
}

$composer = __DIR__.'/vendor/autoload.php';
if(!is_file($composer))
{
	throw new RuntimeException('You should run composer first');
}
require $composer;

$logger = new BasicConsoleLogger(2);
$logger->notice('Input : '.$filePath);

$provider = new JsonSchemaProvider();
$jsonSchema = $provider->provideFromFile($filePath);

$interfacesRepositoryPath = dirname($filePath);
$classesRepositoryPath = str_replace('interface', 'object', $interfacesRepositoryPath);
$testsRepositoryPath = $classesRepositoryPath;
$interfacesComposerPath = $interfacesRepositoryPath.'/composer.json';
$classesComposerPath = $classesRepositoryPath.'/composer.json';
$testsComposerPath = $testsRepositoryPath.'/composer.json';
$interfacesComposerData = json_decode(file_get_contents($interfacesComposerPath), true, 512, JSON_THROW_ON_ERROR);
$interfacesPackageName = $interfacesComposerData['name'];
$interfacesNamespace = trim(array_keys($interfacesComposerData['autoload']['psr-4'])[0], '\\/');
$classesComposerData = json_decode(file_get_contents($classesComposerPath), true, 512, JSON_THROW_ON_ERROR);
$classesPackageName = $classesComposerData['name'];
$classesNamespace = trim(array_keys($classesComposerData['autoload']['psr-4'])[0], '\\/');
$testsComposerData = json_decode(file_get_contents($testsComposerPath), true, 512, JSON_THROW_ON_ERROR);
$testsPackageName = $testsComposerData['name'];
$testsNamespace = trim(array_keys($testsComposerData['autoload']['psr-4'])[0], '\\/').'\\Test';

$namespaces = new NamespaceHolder($interfacesNamespace, $classesNamespace, $testsNamespace);
$generator = new JsonSchemaFileGeneratorPhp74($namespaces, $interfacesPackageName, $classesPackageName, $testsPackageName, $logger);
$fileCollection = $generator->generate($jsonSchema);

$interfacesFolderPath = $interfacesRepositoryPath.'/src';
$classesFolderPath = $classesRepositoryPath.'/src';
$testsFolderPath = $testsRepositoryPath.'/tests';

$writer = new JsonSchemaFileWriter($logger, $interfacesFolderPath, $classesFolderPath, $testsFolderPath);
$writer->write($fileCollection);

