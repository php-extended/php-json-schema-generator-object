# php-extended/php-json-schema-generator-object

A library that implements the php-extended/php-json-schema-generator-interface interface library.

![coverage](https://gitlab.com/php-extended/php-json-schema-generator-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-json-schema-generator-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-json-schema-generator-object ^8`


## Basic Usage

This library may be used the following way :

```php

use PhpExtended\JsonSchema\JsonSchemaFileGeneratorPhp74;
use PhpExtended\JsonSchema\JsonSchemaFileWriter;
use PhpExtended\JsonSchema\JsonSchemaProvider;
use PhpExtended\JsonSchema\NamespaceHolder;

/** @var $logger Psr\Logger\LoggerInterface */

$jsonSchemaProvider = new JsonSchemaProvider();
$jsonSchema = $jsonSchemaProvider->provideFromFile('</path/to/file/json-schema.json>');

$namespaces = new NamespaceHolder('NS\\Interfaces', 'NS\\Classes', 'NS\\Tests');
$generator = new JsonSchemaFileGeneratorPhp74($namespaces, 'interface/composer-package-name', 'classes/composer-package-name', 'tests/composer-package-name', $logger);
$fileCollection = $generator->generate($jsonSchema);

$fileWriter = new JsonSchemaFileWriter($logger, '/path/to/interfaces/src', '/path/to/classes/src', '/path/to/tests/src');
$fileWrite->write($fileCollection);

```


## License

MIT (See [license file](LICENSE)).
